if [ $# -gt 0 ]; then
	echo The $0 received $# parameters: $@ #$*
else
	echo $0 usage: clear tar \| archive
	exit 1
fi

while [ "$1" != "" ]; do
	case $1 in
		clear)
			echo make.sh is clearing:
			rm -v -d -f -r **/compiled/*
			rm -v -d -f -r **/compiled
			rm -v -f -r */*~
			rm -v -f -r */**/*~
			rm -v -f -r #*.rkt#*#
			;;
		tar | archive)
			NOW=$(date +"%Y%m%d_%H%M%S")
			FILE="archive/geometric-constraints-archive-v$NOW.taz"
			echo make.sh is archiving: to file $FILE
			git archive -v --format=tar HEAD -o $FILE
			;;
		* )			echo $0 usage: clear tar \| archive
					exit 1;;
	esac
	shift
done

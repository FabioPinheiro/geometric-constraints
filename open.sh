#!/bin/sh
#open Racket with project's files
rdir=/home/fabio/developer/racket
if [ ! -d $rdir ]; then 
	rdir=~
fi
#fdir=/media/fabio/Data/Tese/Racket/Rosetta-constraints
fdir=$(pwd)
mdir_c=$fdir/core
mdir_gbf=$fdir/geometry-base-facts
mdir_e=$fdir/geometry-base-facts/entities
mdir_fm=$fdir/function-module
mdir_mm=$fdir/mathematical-module
mdir_uc=$fdir/use-cases
mdir_b=$fdir/benchmarks

echo Bom dia Fabio
echo Opening Dr.Racket

if [ $# -gt 0 ]; then
	echo The $0 received $# parameters: $@ #$*
else
	echo $0 no agrs?  =\(
	exit 1
fi

#loaded
C=false; G=false; E=false; F=false; M=false; D=false; T=false; B=false;

while [ "$1" != "" ]; do
	case $1 in
		-c | -core)
			if ! $C; then
				C=true;
				files="$files $(ls -d $mdir_c/*.rkt)";
			fi;;
		-g | -geometry)
			if ! $G; then
				G=true;
				files="$files $(ls -d $mdir_gbf/*.rkt)";
			fi;;
		-e | -entity | -entities)
			if ! $E; then
				E=true;	
				files="$files $(ls -d $mdir_e/*.rkt)";
			fi;;
		-f | -func)
			if ! $F; then
				F=true;
				files="$files $(ls -d $mdir_fm/*.rkt)";
			fi;;
		-m | -math)
			if ! $M; then
				M=true;
				files="$files $(ls -d $mdir_mm/*.rkt)";
			fi;;
		-d | -demo)
			if ! $D; then
				D=true;
				files="$files $(ls -d $mdir_uc/*.rkt)";
			fi;;
		-t | -test)
			if ! $T; then
				T=true;
				#files="$files $(ls -d 1 $mdir_test/*)";
			fi;;
		-b | -benchmarks)
			if ! $B; then
				C=true;
				files="$files $(ls -d 1 $mdir_b/*)";
			fi;;
		# -all ) 		C=true; B=true; F=true; M=true; T=true;;
		-i | -info | -h | -help | * )
			echo $0 usage: -core \ -geometry \ -entities \ -func \ -math \ -demo \ -benchmarks #\ -test
			exit 1;;
	esac
	shift
done

echo Opening Dr.Racket with:
for i in $files
do
	echo \\t $i
done

$rdir/bin/drracket $files

#lang racket

(require macro-debugger/expand)
(require macro-debugger/syntax-browser)
;(error-print-source-location)
(error-print-source-location #t)
;(error-print-source-location)

(exn:srclocs? exn:fail:contract)

(define (ola) (error 'olaerror))
#'1
(match (ola) ['sss 'yes][_ 'noo])
(define s1 #'(match (ola) ['sss 'yes][_ 'noo]))
#'2
(match/derived (ola) #'2 ['sss 'yes][_ 'noo])
(define s2 #'(match/derived (ola) #'2 ['sss 'yes][_ 'noo]))
#'3
(match/derived (ola) #'2 ['sss 'yes][_ 'noo])
(define s3 #'(match/derived (ola) #'2 ['sss 'yes][_ 'noo]))
#'4

;(browse-syntax (expand-only s1 (list #'match/derived)))
;(browse-syntax (expand-only s2 (list #'match/derived)))
;(browse-syntax (expand-only s3 (list #'match/derived)))

;(exn:srclocs-accessor 'v)

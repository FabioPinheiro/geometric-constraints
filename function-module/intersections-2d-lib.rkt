#lang racket
#|INTERSECTIONS-2d|#
(require "utilities-2d-lib.rkt")
(provide intersection-line-line*
         $intersection-line-line*
         intersects-line-line?
         intersection-line-line)

(provide intersection-circle-line*
         $intersection-circle-line*
         intersects-circle-line?
         tangent-circle-line?
         intersections-circle-line
         tangent-circle-line)

(provide intersection-circle-circle*
         $intersection-circle-circle*
         intersects-circle-circle?
         tangent-circle-circle?
         intersects-circle-circle
         tangent-circle-circle)

;######################################################################################################
(define  #;(Line * Line -> (or Location 'parallel)) ;TODO FIXME same line...
  (intersection-line-line* ;http://en.wikipedia.org/wiki/Line-line_intersection
   Ax Ay Bx By Cx Cy Dx Dy)
  (let* ((auxd (- (* (- Ax Bx) (- Cy Dy)) (* (- Ay By) (- Cx Dx)))))
    (cond [(= 0 auxd) 'parallel]
          [else (let* ((aux1 (- (* Ax By) (* Ay Bx)))
                       (aux2 (- (* Cx Dy) (* Cy Dx)))
                       (px (/ (- (* aux1 (- Cx Dx)) (* (- Ax Bx) aux2)) auxd))
                       (py (/ (- (* aux1 (- Cy Dy)) (* (- Ay By) aux2)) auxd)))
                  (list px py))])))

(define ($intersection-line-line* f<-parallel f<-Location . agrs)
  (let ([return (apply intersection-line-line* agrs)])
    (cond
      [(equal? return 'parallel) (f<-parallel return)]
      [((list/c number? number?) return) (f<-Location return)]
      [else (error '$intersection-line-line* "unexpected result (~a)" return)])))

(define #;(Line * Line -> boolean)
  (intersects-line-line? Ax Ay Bx By Cx Cy Dx Dy)
  ($intersection-line-line*
   (lambda (e) #f) ;f<-parallel
   (lambda (e) #t) ;f<-Location
   Ax Ay Bx By Cx Cy Dx Dy))

(define #;(Line * Line -> Location ^ throw ERROR)
  (intersection-line-line Ax Ay Bx By Cx Cy Dx Dy)
  ($intersection-line-line*
   (lambda (e) (error 'intersection-line-line "lines are parallel")) ;f<-parallel
   (lambda (e) e) ;f<-Location
   Ax Ay Bx By Cx Cy Dx Dy))



;######################################################################################################
(define #;(Line * Circle -> (or (cons 'no_intersection null)
                                (cons 'tangent Location)
                                (cons 'intersections (cons Location Location))))
  (intersection-circle-line* centerX centerY radius Ax Ay Bx By)
  ;http://mathworld.wolfram.com/Circle-LineIntersection.html
  (let* ((dx (- Bx Ax))
         (dy (- By Ay))
         (dr (sqrt (+ (sqr dx) (sqr dy))))
         (D (- (* Ax By) (* Bx Ay)))
         (delta (- (* (sqr radius) (sqr dr))
                   (sqr D))))
    (cond [(< delta 0) (cons 'no_intersection null)]
          ;[(= delta 0) (cons 'tangent (cons 0 0))] ;TODO FIXME BUGS
          [(>= delta 0) (let* ((SGN* (if (= 0 (sgn dy)) 1 (sgn dy))); For horizontal lines
                               (auxX+ (+ centerX (/ (+ (* D dy) (* SGN* dx (sqrt delta))) (sqr dr))))
                               (auxX- (+ centerX (/ (- (* D dy) (* SGN* dx (sqrt delta))) (sqr dr))))
                               (auxY+ (+ centerY (/ (+ (- (* D dx)) (* (abs dy) (sqrt delta))) (sqr dr))))
                               (auxY- (+ centerY (/ (- (- (* D dx)) (* (abs dy) (sqrt delta))) (sqr dr))))
                               (location1 (cons auxX+ auxY+))
                               (location2 (cons auxX- auxY-)))
                          (cond [(= delta 0) (if (equal? location1 location2) (cons 'tangent location1)
                                                 (error 'intersection-circle-line* "(= delta 0) location1 != location2"))]
                                [(> delta 0) (cons 'intersections (cons location1 location2))]))])))

(define ($intersection-circle-line* f<-no_intersection f<-tangent f<-intersections . agrs)
  (let ([return (apply intersection-circle-line* agrs)])
    (cond
      [((cons/c 'no_intersection any/c) return) (f<-no_intersection return)]
      [((cons/c 'tangent (cons/c number? number?)) return) (f<-tangent return)]
      [((cons/c 'intersections (cons/c (cons/c number? number?) (cons/c number? number?))) return) (f<-intersections return)]
      [else (error '$intersection-circle-line* "unexpected result (~a)" return)])))

(define #;(Line * Circle -> boolean)
  (intersects-circle-line? centerX centerY radius Ax Ay Bx By)
  ($intersection-circle-line*
   (lambda (e) #f) ;f<-no_intersection
   (lambda (e) #t) ;f<-tangent  ;FIXME ? #t or #f
   (lambda (e) #t) ;f<-intersections
   centerX centerY radius Ax Ay Bx By))

(define #;(Line * Circle -> boolean)
  (tangent-circle-line? centerX centerY radius Ax Ay Bx By)
  ($intersection-circle-line*
   (lambda (e) #f) ;f<-no_intersection
   (lambda (e) #t) ;f<-tangent
   (lambda (e) #f) ;f<-intersections
   centerX centerY radius Ax Ay Bx By))

(define #;(Line * Circle -> (cons Location Location) ^ throw ERROR)
  (intersections-circle-line centerX centerY radius Ax Ay Bx By)
  ($intersection-circle-line*
   (lambda (e) (error 'intersections-line-circle "no intersection")) ;f<-no_intersection
   (lambda (e) (error 'intersections-line-circle "the intersection is a tangent")) ;f<-tangent
   (lambda (e) (cdr e)) ;f<-intersections
   centerX centerY radius Ax Ay Bx By))

(define #;(Line * Circle -> Location ^ throw ERROR)
  (tangent-circle-line centerX centerY radius Ax Ay Bx By)
  ($intersection-circle-line*
   (lambda (e) (error 'intersections-line-circle "no intersection")) ;f<-no_intersection
   (lambda (e) (cdr e)) ;f<-tangent
   (lambda (e) (error 'intersections-line-circle "have two intersections (is not tangent)")) ;f<-intersections
   centerX centerY radius Ax Ay Bx By))


;######################################################################################################
(define #;(Line * Circle -> (or (cons 'no_intersection (or 'coincide 'contains 'too_far_apart)) ;//FIXME '(no_intersection . coincide) ...
                                (cons 'tangent Location)
                                (cons 'intersections (cons Location Location))))
  (intersection-circle-circle*  c1x c1y r1 c2x c2y r2)
  ;"Geometry and Algorithms for COMPUTER AIDED DESIGN" by Erich Hartmann
  (let ((a (* 2 (- c2x c1x)))
        (b (* 2 (- c2y c1y)))
        (c (+ (- (sqr c1x)) (- (sqr c1y)) (- (sqr r2))
              (sqr c2x) (sqr c2y) (sqr r1)))
        (d (location-location-distance c1x c1y c2x c2y)))
    ;tests "https://gsamaras.wordpress.com/code/determine-where-two-circles-intersect-c/"
    (cond [(> d (+ r1 r2)) ;circles are too far apart, no solution(s)
           (cons 'no_intersection 'too_far_apart)] 
          [(and (equal? d 0) (equal? r1 r2)) ;circles coincide
           (cons 'no_intersection 'coincide)]
          [(< (+ d (min r1 r2)) (max r1 r2)) ;One circle contains the other
           (cons 'no_intersection 'contains)]
          [else
           (let* ((r3 (+ r1 r2));FIXME; (r3 10); FIXME;(+ r1 r2))
                  (p1hack (if (= 0 b) ;correção de bug b=0
                              (list (/ (+ c (* r3 b)) a) (- r3))
                              (list (- 0 r3) (/ (+ c (* r3 a)) b)))) ;ax+by=c  a=-10 => y= (c + 10a) /b
                  (p2hack (if (= 0 b) ;correção de bug b=0
                              (list (/ (+ c (* (- r3) b)) a) r3)
                              (list r3 (/ (+ c (* (- r3) a)) b)))) ;ax+by=c  a=+10 => y= (c - 10a) /b
                  (ret1 (intersection-circle-line* c1x c1y r1 (car p1hack) (cadr p1hack) (car p2hack) (cadr p2hack))))
             #;(unless (equal? ret1 (circle-line-intersection* c2x c2y r2 (car p1hack) (cadr p1hack) (car p2hack) (cadr p2hack)))
                 (error (format "intersection-circle-circle* error:(ret1 ~a)!=(ret2 ~a)" ret1 ret2))) ;FAIL
             #;(display (combine
                         (with-color (rgba "red" 0.5)   (ring (pos c1x c1y 0) r1))
                         (with-color (rgba "green" 0.7) (ring (pos c2x c2y 0) r2))
                         (with-color (rgba "white" 1)   (arrow (pos (caar ret1) (cadar ret1) 0) (pos (caadr ret1) (cadadr ret1) 0)))))
             ret1)])))

(define ($intersection-circle-circle* f<-no_intersection f<-tangent f<-intersections . agrs)
  (let ([return (apply intersection-circle-circle* agrs)])
    (cond
      [((cons/c 'no_intersection any/c) return) (f<-no_intersection return)] ;(cons 'no_intersection (or 'coincide 'contains 'too_far_apart))
      [((cons/c 'tangent (cons/c number? number?)) return) (f<-tangent return)]
      [((cons/c 'intersections (cons/c (cons/c number? number?) (cons/c number? number?))) return) (f<-intersections return)]
      [else (error $intersection-circle-circle* "unexpected result (~a)" return)])))


(define  #;(Circle * Circle -> boolean)
  (intersects-circle-circle? c1x c1y r1 c2x c2y r2)
  ($intersection-circle-circle*
   (lambda (e) #f) ;f<-no_intersection
   (lambda (e) #f) ;f<-tangent ;FIXME tangent is a intersection?!
   (lambda (e) #t) ;f<-intersections
   c1x c1y r1 c2x c2y r2))

(define #;(Circle * Circle -> boolean)
  (tangent-circle-circle? c1x c1y r1 c2x c2y r2)
  ($intersection-circle-circle*
   (lambda (e) #f) ;f<-no_intersection
   (lambda (e) #t) ;f<-tangent
   (lambda (e) #f) ;f<-intersections
   c1x c1y r1 c2x c2y r2))

(define #;(Circle * Circle -> Location ^ throw ERROR)
  (intersects-circle-circle c1x c1y r1 c2x c2y r2)
  ($intersection-circle-circle*
   (lambda (e) (error 'tangent-circle-circle "no intersection or coincide")) ;f<-no_intersection
   (lambda (e) (error 'tangent-circle-circle "the intersection is a tangent")) ;f<-tangent
   (lambda (e) (cdr e)) ;f<-intersections
   c1x c1y r1 c2x c2y r2))

(define #;(Circle * Circle -> Location ^ throw ERROR)
  (tangent-circle-circle c1x c1y r1 c2x c2y r2)
  ($intersection-circle-circle*
   (lambda (e) (error 'tangent-circle-circle "no intersection or coincide")) ;f<-no_intersection
   (lambda (e) (cdr e)) ;f<-tangent
   (lambda (e) (error 'tangent-circle-circle "have two intersections (is not tangent)")) ;f<-intersections
   c1x c1y r1 c2x c2y r2))



(module+ test
    ;(require pict3d);FIXME REMOVE
    (displayln "++++++++++++++++++++++++++ module test start")
    
    (displayln ">>line-line")
    (displayln intersection-line-line*)
    (equal? (intersection-line-line* 7 10 7 -10 0 5 15 5) '(7 5))
    (equal? (intersection-line-line* 1 0 3 0 1 1 3 1) 'parallel)
    (equal? (intersection-line-line* 0 1 0 3 0 1 0 3) 'parallel)
    (displayln intersects-line-line?)
    (equal? (intersects-line-line? 7 10 7 -10 0 5 15 5) #t)
    (equal? (intersects-line-line? 1 0 3 0 1 1 3 1) #f)
    (displayln intersection-line-line)
    (equal? (intersection-line-line 7 10 7 -10 0 5 15 5) '(7 5))
    #;(equal? (intersection-line-line 1 0 3 0 1 1 3 1) error)
    
    (displayln ">>circle-line")
    (displayln intersection-circle-line*)
    (equal? (intersection-circle-line* 0 0 1 -5 0 5 0) '((1 0) (-1 0)))
    (equal? (intersection-circle-line* 0 0 1 -5 1 5 1) '((0 1)))
    (displayln intersects-circle-line?)
    (equal? (intersects-circle-line? 0 0 1 -5 0 5 0) #t)
    (equal? (intersects-circle-line? 0 0 1 -5 1 5 1) #t)
    (equal? (intersects-circle-line? 0 0 0.5 -5 1 5 1) #f)
    (displayln tangent-circle-line?)
    (equal? (tangent-circle-line? 0 0 1 -5 0 5 0) #f)
    (equal? (tangent-circle-line? 0 0 1 -5 1 5 1) #t)
    (equal? (tangent-circle-line? 0 0 0.5 -5 1 5 1) #f)
    (displayln intersections-circle-line)
    (equal? (intersections-circle-line 0 0 1 -5 0 5 0) '((1 . 0) -1 . 0))
    #;(equal? (intersections-circle-line 0 0 1 -5 1 5 1) error)
    #;(equal? (intersections-circle-line 0 0 0.5 -5 1 5 1) error)
    (displayln tangent-circle-line)
    #;(equal? (tangent-circle-line 0 0 1 -5 0 5 0) error)
    (equal? (tangent-circle-line 0 0 1 -5 1 5 1) '(0 . 1))
    #;(equal? (tangent-circle-line 0 0 0.5 -5 1 5 1) error)
    
    (displayln ">>circle-circle")
    (displayln intersection-circle-circle*)
    (intersection-circle-circle*  0 0 1  0 0 2)
    (intersection-circle-circle*  0 0 10  1 0 1.5)
    (intersection-circle-circle*  100 0 10  1 0 1)
    (intersection-circle-circle*  1 2 3  1 2 3)
    (equal? (intersection-circle-circle*  0 0 1  0 2 1) '(tangent 0 . 1)) ;tagente
    (equal? (intersection-circle-circle*  0 0 1.5  1 0 1.5) '(intersections (0.5 . 1.414213562373095) 0.5 . -1.414213562373095))
    (equal? (intersection-circle-circle*  0 0 1.5  1 0 1) '(intersections (1.125 . 0.9921567416492215) 1.125 . -0.9921567416492215))
    (equal? (intersection-circle-circle*  0 0 1.5  1 1 1) '(intersections (0.13070549283526772 . 1.4942945071647322) 1.4942945071647322 . 0.13070549283526772))
    (displayln intersects-circle-circle?)
    (equal? (intersects-circle-circle?  100 0 10  1 0 1) #f) ;no_intersection
    (equal? (intersects-circle-circle?  0 0 1  0 2 1) #f) ;tagente
    (equal? (intersects-circle-circle?  0 0 1.5  1 0 1.5) #t) ;intersections
    (displayln tangent-circle-circle?)
    (equal? (tangent-circle-circle?  100 0 10  1 0 1) #f) ;no_intersection
    (equal? (tangent-circle-circle?  0 0 1  0 2 1) #t) ;tagente
    (equal? (tangent-circle-circle?  0 0 1.5  1 0 1.5) #f) ;intersections
    (displayln intersects-circle-circle)
    #;(equal? (intersects-circle-circle  100 0 10  1 0 1) error) ;no_intersection
    #;(equal? (intersects-circle-circle  0 0 1  0 2 1) error) ;tagente
    (equal? (intersects-circle-circle  0 0 1.5  1 0 1.5) '((0.5 . 1.414213562373095) 0.5 . -1.414213562373095)) ;intersections
    (displayln tangent-circle-circle)
    #;(equal? (tangent-circle-circle  100 0 10  1 0 1) error) ;no_intersection
    (equal? (tangent-circle-circle  0 0 1  0 2 1) '(0 . 1)) ;tagente
    #;(equal? (tangent-circle-circle  0 0 1.5  1 0 1.5) error) ;intersections
    (displayln "++++++++++++++++++++++++++ module test end"))
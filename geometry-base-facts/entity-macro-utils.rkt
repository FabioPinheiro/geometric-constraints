#lang racket
(require
  racket/syntax
  syntax/id-table)

(provide $format-id
         ;OOO
         ;OOO-displayln
         ;OOO-ref
         ;IDtable
         (struct-out $ID)
         (struct-out $FIELD)
         (struct-out $FUNC)
         $ID-new
         $ID-ref
         $ID-map
         $ID-debug
         ;;;FIELD
         $ID-add-field
         ;$ID-ref-field
         ;$ID-map-fields
         $ID-mapSorted-fields
         ;;;FUNCTIONS
         $ID-add-fieldsRef
         $ID-ref-fieldsRef
         $ID-ref-fieldsRef-λname
         $ID-ref-fieldsRef-λinfo
         $ID-ref-fieldsRef-λ
         $ID-map-fieldsRef
         $ID-map-fieldsRef-debug
         $ID-description
         $ID-add-provide
         $ID-map-provide)


(define ($format-id id txt . agrs) (apply format-id  id txt agrs)) ;#:source id #:props
;(define IDtable (make-parameter (make-free-id-table)))
(define IDtable2 (make-free-id-table))
(struct $ID (id description fields fieldsRef fieldsSet oldConstructor newConstructor copyConstructor guard λ? provide) #:transparent #:mutable)
(struct $FIELD (name type order default λref) #:transparent #:mutable)
(struct $FUNC (name call defined?) #:transparent #:mutable)

;#######################################     OOO     ##################################################
(define OOO (make-free-id-table) #;(make-free-id-table))
(define (OOO-ref id)
  (free-id-table-ref OOO (datum->syntax #f (syntax->datum id))))
(define (OOO-displayln table)
  (displayln ">")
  (free-id-table-map
   table
   (λ (k v)
     (displayln k)
     ))
  (displayln "_"))

;#######################################     ID_TABLE     ############################################
(define ($ID-new id description oldConstructor)
  (free-id-table-set! OOO id (syntax->datum id))
  (free-id-table-set! IDtable2 id ($ID id                                    ;$ID-id
                                       description                           ;$ID-description
                                       (make-free-id-table)                  ;$ID-fields
                                       ;(mutable-free-id-set)                ;$ID-SetOfFields
                                       (make-free-id-table)                  ;$ID-fieldsRef
                                       (make-free-id-table)                  ;$ID-fieldsSet
                                       oldConstructor                        ;$ID-oldConstructor
                                       ($format-id id "new-~A" id)           ;$ID-newConstructor
                                       ($format-id id "copy-~A" id)          ;$ID-copyConstructor
                                       #f                                    ;$ID-guard
                                       ($format-id id "~A?" id)              ;$ID-λ?
                                       (make-free-id-table)                  ;$ID-provide
                                       )))

(define ($ID-ref id #:fail [fail #f])
  (free-id-table-ref IDtable2 id (if fail fail (lambda () (raise (error '$ID-ref (~a "IDtable no mapping for " id)))))))

(define ($ID-map [func (λ (k v) v)])
  (free-id-table-map IDtable2 func))

(define ($ID-debug)
  (displayln ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
  (free-id-table-map
   IDtable2
   (λ (k v)
     (displayln k)
     ))
  (displayln "______________________________________"))


(define ($ID-add-field id field type #:default [default '$unset])
  (let ([str ($ID-ref id)]
        [fieldRefname ($format-id id "~A-~A" id field)])
    ;add fields
    (let* ([fields-table ($ID-fields str)]
           [order (add1 (free-id-table-count fields-table))]) ;$FIELD-name $FIELD-type $FIELD-order
      (free-id-table-set! fields-table field ($FIELD field type order default fieldRefname)))
    ;add refs funtions
    ($ID-add-fieldsRef id field fieldRefname (list fieldRefname) #:defined? #t) ;func-name func-info
    #;(let* ([key-name-s ($format-id field "~A-s" fieldRefname)])
        (when (eq? 'value (syntax->datum type))
          ($ID-add-fieldsRef id key-name-s key-name-s (cons ($format-id field "~A" 'value-get-str) (cons fieldRefname '())))))
    
    (free-id-table-set! ($ID-fieldsSet str) field ($format-id id "set-~A-~A!" id field))
    
    ;add refs funtions extended
    ($ID-map-fieldsRef type
                       (λ (k v);key f-name f-info
                         (let* ([key-name ($format-id field "~A-~A" field k)]
                                [new-getter ($format-id field "~A-~A" id key-name)])
                           ($ID-add-fieldsRef id key-name new-getter (cons fieldRefname ($FUNC-call v)))
                           ))
                       #:fail (λ () #f))
    (void) #;"$ID-add-field-DONE"))

#;(define ($ID-ref-field id field)
    (free-id-table-ref ($ID-fields ($ID-ref id)) field
                       (lambda () (raise (error '$ID-ref-field (~a (syntax->datum id) " no mapping for " (syntax->datum field)))))))

(define ($ID-map-fields id [func (λ (k v) v)])
  (free-id-table-map ($ID-fields ($ID-ref id)) func))

(define ($ID-mapSorted-fields id [func (λ (k v) v)])
  (map (λ (pair) (func (car pair) (cdr pair)))
       (sort
        (free-id-table-map ($ID-fields ($ID-ref id)) (λ (k v) (cons k v)))
        (λ (e1 e2) (< ($FIELD-order (cdr e1)) ($FIELD-order (cdr e2)))))))


(define ($ID-add-fieldsRef id key func-name func-info #:defined? [defined? #f])
  (free-id-table-set! ($ID-fieldsRef ($ID-ref id)) key ($FUNC func-name func-info defined?)))

(define ($ID-ref-fieldsRef id field #:$FUNC [λλ (λ (o) o)])
  (λλ (free-id-table-ref ($ID-fieldsRef ($ID-ref id)) field
                         (lambda () (raise (error '$ID-ref-fieldsRef (~a (syntax->datum id) " no mapping for " (syntax->datum field))))))))

(define ($ID-ref-fieldsRef-λname id field)
  ($ID-ref-fieldsRef id field #:$FUNC $FUNC-name))

(define ($ID-ref-fieldsRef-λinfo id field)
  ($ID-ref-fieldsRef id field #:$FUNC $FUNC-call))

(define ($ID-ref-fieldsRef-λ id field obj)
  (define ($ID-ref-fieldsRef-λ-aux lst obj)
    (if (null? lst) obj ($ID-ref-fieldsRef-λ-aux (cdr lst) #`(#,(car lst) #,obj))))
  ($ID-ref-fieldsRef-λ-aux ($ID-ref-fieldsRef id field #:$FUNC $FUNC-call) obj))

(define ($ID-map-fieldsRef id [func (λ (k v) v)] #:fail [fail #f])
  (unless (procedure-arity-includes? func 2) (error '$ID-map-fieldsRef "procedure-arity of func doesn't include 2..."))
  (let ([aux ($ID-ref id #:fail fail)])
    (if aux
        (free-id-table-map ($ID-fieldsRef aux) func)
        #f)))

(define ($ID-map-fieldsRef-debug id)
  ($ID-map-fieldsRef id ;(λ (k v) (displayln (syntax->datum ($FUNC-name v)))))
                     (λ (k v) (displayln (cons (syntax->datum ($FUNC-name v))
                                               (~a ($FUNC-defined? v)"--->>" (map (λ (o) (syntax->datum o)) ($FUNC-call v))))))))

(define ($ID-add-provide id name)
  (free-id-table-set! ($ID-provide ($ID-ref id)) name (void)))

(define ($ID-map-provide id [func (λ (k v) k)])
  (unless (procedure-arity-includes? func 2) (error '$ID-map-provide "procedure-arity of func doesn't include 2..."))
  (free-id-table-map ($ID-provide ($ID-ref id)) func))


(module+ test
  (displayln "++++++++++++++++++++++++++ entity-macro-utils module test start")
  #|(define stx1 #'TTT)
  ($ID-new stx1 'description1 #'oldConstrut1)
  ($ID-ref stx1)
  ($ID-add-field stx1 #'x #'value)
  ($ID-add-field stx1 #'y #'type1)
  (displayln "$$ID-ref-fieldsRef")
  ($ID-ref-fieldsRef stx1 #'y #:$FUNC $FUNC-name)
  ($ID-ref-fieldsRef-λname stx1 #'y)
  ($ID-ref-fieldsRef stx1 #'y #:$FUNC $FUNC-call)
  
  ;(define stx2 #'AAA)
  ($ID-new #'AAA 'description1 #'oldConstrut2)
  ($ID-add-field #'AAA #'a #'TTT)
  
  (displayln "$ID-map-fieldsRef stx2")
  ($ID-map-fieldsRef #'AAA (λ (k v) (cons (syntax->datum ($FUNC-name v))
                                          (~a "--->>" (map (λ (o) (syntax->datum o)) ($FUNC-call v))))))
  
  ($ID-ref-fieldsRef #'AAA #'a-x #:$FUNC $FUNC-call)
  ($ID-ref-fieldsRef-λ #'AAA #'a-x #'obj)
  
  (displayln "OOOOOOOOOOOOOOOOOOOOOO")
  ($ID-map-fieldsRef-debug #'AAA)
  ($ID-map-fields #'AAA (λ (k v) (displayln k)))

  ($ID-map-fieldsRef-debug #'TTT)|#
  
  ($ID-new #'ABC 'description1 #'oldConstrut3)
  ($ID-add-field #'ABC #'a #'value)
  ($ID-add-field #'ABC #'c #'value)
  ($ID-add-field #'ABC #'b #'value)
  ($ID-mapSorted-fields #'ABC (λ (k v) v (syntax->datum ($FIELD-name v))))
  (displayln "++++++++++++++++++++++++++ entity-macro-utils module test end"))
#lang racket

(require "../entity-macro.rkt")
(require "var.rkt")

(entity-type constraints
  ([math var]
   [entity any]))

(entity-build constraints)

(module+ test
  (displayln "++++++++++++++++++++++++++ module+ test start")
  (define constraints1 (new-constraints '"{r}>1" `(r ,(new-var))))
  constraints1
  (displayln "++++++++++++++++++++++++++ module+ test end"))



#lang racket
(require "../entity-macro.rkt")
(require "value.rkt")
(require "point.rkt")

(entity-type line
  ([start point (new-point 0 0)]
   [end point (new-point 1 0)]
   [direction point 'unset]
   [length value 'unset]
   [subtype symbol 'straight]  ; #;TODO limit the symbol types (symbol 'straight 'segment 'semistraight)
   ))

(entity-build line)


(module+ test
  (require "value.rkt")
  (displayln "++++++++++++++++++++++++++ module+ test start")
  (define line1 (new-line))
  
  (define line2 (new-line (new-point "%pi" 10) (new-point (new-value) 0)))
  line1
  (line-start-x-s line1)
  (line-end-x-s line2)
  
;(line-start-x (new-line (new-point 1 2) (new-point 3 4)))
  (displayln "++++++++++++++++++++++++++ module+ test end"))
;





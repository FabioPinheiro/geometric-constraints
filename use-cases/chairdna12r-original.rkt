#lang racket/gui
(require (planet aml/rosetta:1:53))
;(backend rhino)
;(backend autocad)
(backend rhino5)   ;;only for 64-bit Operating System

;ChairDNA version 1.2 (September 2015)


;----------------------------------------------------------------------------------------------
;------GENERAL FUNCTIONS------
;----------------------------------------------------------------------------------------------

;DEGREES > RADIANS
(define (deg>rad deg) 
  (/ (* deg pi) 180))

;POINT-IN-LINE
;f = between 0 and 100
(define (point-in-line P0 P1 f)
  (+c P0 (*c (-c P1 P0) (/ f 100))))

;INTERSECTION LINE-LINE
;https://en.wikipedia.org/wiki/Line-line_intersection
;intersection of 2 lines in a horizontal 2D plane
(define (intersection-lines p1 p2 p3 p4)
  (let ((x1 (cx p1))
        (x2 (cx p2))
        (x3 (cx p3))
        (x4 (cx p4))
        (y1 (cy p1))
        (y2 (cy p2))
        (y3 (cy p3))
        (y4 (cy p4)))
    (xyz
     (/ (- (* (- (* x1 y2) (* y1 x2)) (- x3 x4)) (* (- x1 x2) (- (* x3 y4) (* y3 x4))))
        (- (* (- x1 x2) (- y3 y4)) (* (- y1 y2) (- x3 x4))))
     (/ (- (* (- (* x1 y2) (* y1 x2)) (- y3 y4)) (* (- y1 y2) (- (* x3 y4) (* y3 x4))))
        (- (* (- x1 x2) (- y3 y4)) (* (- y1 y2) (- x3 x4))))
     0
     )))

;TANGENT TO 2 CIRCLES
(define (δ-tan-2circles r0 r1 C0 C1) ;give 2 radius and 2 centres
  (cond 
    ((>= r0 r1) (acos (/ (- r0 r1) (distance C0 C1))))
    ((> r1 r0) (- pi (acos (/ (- r1 r0) (distance C0 C1)))))))

;LINE SLOPE
(define (θ-slope P0 P1) ;give 2 points of the line, P0 is higher that P1
  (cond
    ((= (cx P0) (cx P1)) pi/2) ;if vertical line > angle = pi/2
    ((> (cx P0) (cx P1)) (atan (/ (- (cy P0) (cy P1)) (- (cx P0) (cx P1))))) ;positive slope
    ((< (cx P0) (cx P1)) (- pi (atan (/ (- (cy P0) (cy P1)) (- (cx P1) (cx P0)))))))) ;negative slope

;NEW LINE, ARC, JOIN-CURVES (THAT ACCEPT POINTS)
(define (line-new p0 p1) (if (=c? p0 p1) (point p0) (line p0 p1)))     ;line between 2 coincident points > point
(define (arc-new p0 r Δi Δf) (if (= r 0) (point p0) (arc p0 r Δi Δf))) ; arc with radius=0 > point

(define (join-curves-new shapes) 
  (begin0 ;the result is the 1st line (the begin result is the last one)
    (join-curves (filter (lambda (s) (not (point? s))) shapes))   ;join-curves don't accept points
    (delete-shapes (filter point? shapes))))


;----------------------------------------------------------------------------------------------
;------GLOBAL VARIABLES------
;----------------------------------------------------------------------------------------------

;---GUIDES---
(define guides #t)                         ;[yes/no]

;mode
(define mode-wireframe #t)                 ;[yes/no]
(define mode-solid #f)                     ;[yes/no]

;template-chair
(define template-chair-none #t)            ;[yes/no]
(define template-chair-thonet14 #f)        ;[yes/no]
(define template-chair-thonet15 #f)        ;[yes/no]
(define template-chair-thonet18 #f)        ;[yes/no]
(define template-chair-random #f)          ;[yes/no]


;---LEGS---
;leg-front
(define leg-front #f)                      ;[yes/no] [s: sf, ss(+), ss-d(+), sl(+), sr(+), lsf, lss(+), lsr(+), lbf, lbs(+), lbs-d(+), lbr(+), asf]
(define leg-front-Δwidth 0)                ;[-100 100 mm] distance from the front corner of the seat, in the seat plane
(define leg-front-Δdepth 0)                ;[-100 100 mm]
(define leg-front-angle-splay 0)           ;[-45 45 º] between leg and vertical
(define leg-front-angle-rake 0)            ;[-45 45 º]

(define leg-front-sround #f)               ;[yes/no]
(define leg-front-sround-diameter 20)      ;[1 260 mm]
(define leg-front-ssquare #f)              ;[yes/no]
(define leg-front-ssquare-width 20)        ;[1 260 mm]
(define leg-front-ssquare-depth 20)        ;[1 260 mm]
(define leg-front-angle-taper 0)           ;[0 100 %]
         
;leg-back  
(define leg-back #f)                       ;[yes/no] [s: sb, ss(+), ss-d(+), sl(+), sr(+), bu, lsb, lss(+), lsr(+), lbb, lbs(+), lbs-d(+), lbr(+), asb]
(define leg-back-Δwidth 0)                 ;[-100 100 mm]
(define leg-back-Δdepth 0)                 ;[-100 100 mm]
(define leg-back-angle-splay 0)            ;[-45 45 º]
(define leg-back-angle-rake 0)             ;[-45 45 º]

(define leg-back-sround #f)                ;[yes/no]
(define leg-back-sround-diameter 20)       ;[1 260 mm]
(define leg-back-ssquare #f)               ;[yes/no]
(define leg-back-ssquare-width 20)         ;[1 260 mm]
(define leg-back-ssquare-depth 20)         ;[1 260 mm]
(define leg-back-angle-taper 0)            ;[0 100 %]
         
;leg-centre
(define leg-centre #f)                     ;[yes/no] [s: sr(+), sr-n, lbr(+), lbr-n]

(define leg-centre-sround #f)                ;[yes/no]
(define leg-centre-sround-diameter 20)       ;[1 260 mm]
(define leg-centre-ssquare #f)               ;[yes/no]
(define leg-centre-ssquare-width 20)         ;[1 260 mm]
(define leg-centre-ssquare-depth 20)         ;[1 260 mm]
(define leg-centre-angle-taper 0)            ;[0 100 %]
         
;---SEAT---
;seat-area
(define seat-area #t)                           ;[yes/no]
(define seat-area-width 483)                    ;[457 508 mm] 
(define seat-area-depth 427)                    ;[397 457 mm]
(define seat-area-height 419)                   ;[406 432 mm]
(define seat-area-angle-rake  0)                ;[0 5 º]
(define seat-area-radius-front 0)               ;[0 100 %] of max radius
(define seat-area-radius-rear 0)                ;[0 100 %] of max radius
(define seat-area-Δwidth-taper 0)               ;[0 100%] of the rear edge of the seat

;seat-panel
(define seat-panel #f)                          ;[yes/no]
(define seat-panel-thickness 10)                 ;[1 100 mm]

;-SEAT-OUTER-
;seat-front
(define seat-front #f)                     ;[yes/no] [p: lf, s: sl(+), sl-Δwf, so-sr (+), so-ss (+)]
         
;seat-back
(define seat-back #f)                      ;[yes/no] [p: lb, s: sl(+), sl-Δwr, bs(+), so-sr (+), so-ss (+)]
         
;seat-side
(define seat-side #f)                      ;[yes/no] [p: lf or lb, s: sc, bu, ass]
(define seat-side-depth 427)               ;[397 457 mm] [p: (not lf) or (not lb)]

;section
(define seat-outer-sround #f)                ;[yes/no] [p: sf or sb or ss]
(define seat-outer-sround-diameter 20)       ;[1 260]
(define seat-outer-ssquare #f)               ;[yes/no] [p: sf or sb or ss]
(define seat-outer-ssquare-height 20)        ;[1 260]
(define seat-outer-ssquare-width 20)         ;[1 260]

;-SEAT-INNER-
;seat-cross
(define seat-cross #f)                       ;[yes/no] [p: ss, s: si-sr (+), si-ss (+)]
(define seat-cross-Δdepth 50)                ;[y 0 100 %] of ss

;seat-long
(define seat-long #f)                        ;[yes/no] [p: lb+sf or lf+sb or sf+sb, s: si-sr (+), si-ss (+)]
(define seat-long-Δwidth-front 50)           ;[x 0 100 %] of sf [p: sf]
(define seat-long-Δwidth-rear 50)            ;[x 0 100 %] of sb [p: sb]

;seat-radial
(define seat-radial #f)                      ;[yes/no] [p: lf+lb or lc, s: si-sr (+), si-ss (+)]
(define seat-radial-number 4)                ;[3 5] [p: lc]

;section
(define seat-inner-sround #f)                ;[yes/no] [p: sc or sl or sr]
(define seat-inner-sround-diameter 20)       ;[1 260]
(define seat-inner-ssquare #f)               ;[yes/no] [p: sc or sl or sr]
(define seat-inner-ssquare-height 20)        ;[1 260]
(define seat-inner-ssquare-width 20)         ;[1 260]


;---BACK---
;back-area
(define back-area #t)                        ;[yes/no]
(define back-area-height 521)                ;[432 610 mm]
(define back-area-Δheight 76)                ;[0 152 mm]
(define back-area-angle-rake 90)             ;[90 105 º]
(define back-area-radius-top 0)              ;[0 100 %]
(define back-area-radius-bottom 0)           ;[0 100 %]

;back-panel
(define back-panel #f)                       ;[yes/no]
(define back-panel-thickness 10)             ;[1 100 mm]

;-BACK-OUTER
;back-upright
(define back-upright #f)                     ;[yes/no] [p: lb or ss or sl or ar, s: bt, bc, br, bo-ss, bo-sr]
(define back-upright-height 50)              ;[0 100 %] of back height
(define back-upright-angle-splay 0)          ;[-45 45 º]

;back-top
(define back-top #f)                       ;[yes/no] [p: bu, s: bs(+)]

;section
(define back-outer-sround #f)                ;[yes/no] [p: bu]
(define back-outer-sround-diameter 20)       ;[1 260]
(define back-outer-ssquare #f)               ;[yes/no] [p: bu]
(define back-outer-ssquare-depth 20)        ;[1 260]
(define back-outer-ssquare-width 20)         ;[1 260]

;-BACK-INNER-
;back-cross-top
(define back-cross-top #f)                   ;[yes/no] [p: bu, s: bs(+), bi-sr(+), bi-ss(+)]
(define back-cross-top-height 75)            ;[0 100%] of upper mid of bu

;back-cross-bottom
(define back-cross-bottom #f)                ;[yes/no] [p: bu, s: bs(+), bi-sr(+), bi-ss(+)]
(define back-cross-bottom-height 25)         ;[0 100%] of lower mid of bu

;back-cross-arm
(define back-cross-arm #f)                  ;[yes/no] [p: ar, s: bs(+)]

;back-splat
(define back-splat #f)                      ;[yes/no] [p: bt+sb or bt+bct, s: bi-sr(+), bi-ss(+)]
(define back-splat-Δwidth-top 50)           ;[0 100%] of bt
(define back-splat-Δwidth-bottom 50)        ;[0 100%] of sb or bct

;back-radial
(define back-radial #f)                         ;[yes/no] [p: bu, bi-sr(+), bi-ss(+)]

;section
(define back-inner-sround #f)                ;[yes/no] [p: bct or bcb or bc or br]
(define back-inner-sround-diameter 20)       ;[1 260]
(define back-inner-ssquare #f)               ;[yes/no] [p: bct or bcb or bc or br]
(define back-inner-ssquare-depth 20)        ;[1 260]
(define back-inner-ssquare-width 20)         ;[1 260]


;---LEG-STRETCHER---
;leg-stretcher-area
(define leg-stretcher-area #f)                ;[yes/no]
(define leg-stretcher-area-height 50)         ;[1 99 %] of lf
(define leg-stretcher-area-angle-rake 0)      ;[-100 100 %] of seat-height
(define leg-stretcher-area-radius-front 0)    ;[0 100 %]
(define leg-stretcher-area-radius-rear 0)     ;[0 100 %]

;LEG-STRETCHER-OUTER
;leg-stretchers-front
(define leg-stretcher-front #f)             ;[yes/no] [p: lf, s: lso-sr(+), lso-ss(+)]
(define leg-stretcher-front-height 50)      ;[1 99 %] of lf
         
;leg-stretcher-back
(define leg-stretcher-back #f)            ;[yes/no] [p: lb, s: lso-sr(+), lso-ss(+)]
(define leg-stretcher-back-height 50)     ;[z 1 99 %] of lb
         
;leg-stretcher-side
(define leg-stretcher-side #f)            ;[yes/no] [p: lf+lb, s: lsc, lso-sr(+), lso-ss(+)]
(define leg-stretcher-side-height 50)     ;[z 1 99 %] of lf

;section
(define leg-stretcher-outer-sround #f)                ;[yes/no] [p: lsf, lsb, lss]
(define leg-stretcher-outer-sround-diameter 20)       ;[1 260]
(define leg-stretcher-outer-ssquare #f)               ;[yes/no] [p: lsf, lsb, lss]
(define leg-stretcher-outer-ssquare-height 20)        ;[1 260]
(define leg-stretcher-outer-ssquare-width 20)         ;[1 260]

;LEG-STRETCHER-INNER
;leg-stretcher-cross
(define leg-stretcher-cross #f)               ;[yes/no] [p: lss, s: lsi-sr(+), lsi-ss(+)]
(define leg-stretcher-cross-Δdepth 50)        ;[y 0 100 %] of lss

;leg-stretcher-radial
(define leg-stretcher-radial #f)               ;[yes/no] [p: lf+lb, s: lsi-sr(+), lsi-ss(+)]
(define leg-stretcher-radial-height 50)        ;[1 99 %] of lf

;section
(define leg-stretcher-inner-sround #f)                ;[yes/no] [p: lsc or lsr]
(define leg-stretcher-inner-sround-diameter 20)       ;[1 260]
(define leg-stretcher-inner-ssquare #f)               ;[yes/no] [p: lsc or lsr]
(define leg-stretcher-inner-ssquare-height 20)        ;[1 260]
(define leg-stretcher-inner-ssquare-width 20)         ;[1 260]


;---LEG-BASE---
;leg-base-area
(define leg-base-area #f)                 ;[yes/no]
(define leg-base-area-width 483)          ;[457 508 mm] 
(define leg-base-area-depth 427)          ;[397 457 mm]
(define leg-base-area-radius-front 0)     ;[0 100 %]
(define leg-base-area-radius-rear 0)      ;[0 100 %]

;leg-base-panel
(define leg-base-panel #f)                   ;[yes/no]
(define leg-base-panel-thickness 10)         ;[1 100 mm]

;LEG-BASE-OUTER
;leg-base-front
(define leg-base-front #f)           ;[yes/no] [p: lf, s: lbo-sr(+), lbo-ss(+)]
         
;leg-base-back
(define leg-base-back #f)            ;[yes/no] [p: lb, s: lbo-sr(+), lbo-ss(+)]
         
;leg-base-side
(define leg-base-side #f)            ;[yes/no] [p: lf or lb, s: lbc, lbo-sr(+), lbo-ss(+)]
(define leg-base-side-depth 427)     ;[1 500 mm] [p: (not lf) or (not lb)]

;section
(define leg-base-outer-sround #f)                ;[yes/no] [p: lbf or lbb or lbs]
(define leg-base-outer-sround-diameter 20)       ;[1 260]
(define leg-base-outer-ssquare #f)               ;[yes/no] [p: lbf or lbb or lbs]
(define leg-base-outer-ssquare-height 20)        ;[1 260]
(define leg-base-outer-ssquare-width 20)         ;[1 260]

;LEG-BASE-INNER
;leg-base-cross
(define leg-base-cross #f)               ;[yes/no] [p: lbs, s: lbi-sr(+), lbi-ss(+)]
(define leg-base-cross-Δdepth 50)        ;[0 100 %] of lbs

;leg-base-radial
(define leg-base-radial #f)               ;[yes/no] [p: lf+lb or lc, s: lbi-sr(+), lbi-ss(+)]
(define leg-base-radial-number 4)         ;[3 5] [p: lc]  

;section
(define leg-base-inner-sround #f)                ;[yes/no] [p: lbc or lbr]
(define leg-base-inner-sround-diameter 20)       ;[1 260]
(define leg-base-inner-ssquare #f)               ;[yes/no] [p: lbc or lbr]
(define leg-base-inner-ssquare-height 20)        ;[1 260]
(define leg-base-inner-ssquare-width 20)         ;[1 260]


;---ARMS---
;arm-area
(define arm-area #f)                       ;[yes/no]
(define arm-area-height 229)               ;[203-254 mm]
(define arm-area-angle-splay 0)            ;[-45 45 º]
(define arm-area-radius-front 0)           ;[0 100 %]
(define arm-area-radius-rear 0)             ;[0 100 %]

;arm-support-front
(define arm-support-front #f)                 ;[yes/no] [p: lf, s: ar(+)]
(define arm-support-front-angle-rake 0)       ;[-45 45 º]

;arm-support-back
(define arm-support-back #f)                 ;[yes/no] [p: lb, s: ar(+), not bu]
(define arm-support-back-angle-rake 0)       ;[-45 45 º]

;arm-support-side
(define arm-support-side #f)                 ;[yes/no] [p: ss, s: ar(+)]
(define arm-support-side-Δdepth 50)          ;[0 100 %] of ss

(define arm-support-side-sround #f)                ;[yes/no]
(define arm-support-side-sround-diameter 20)       ;[1 260]
(define arm-support-side-ssquare #f)               ;[yes/no]
(define arm-support-side-ssquare-width 20)        ;[1 260]
(define arm-support-side-ssquare-depth 20)         ;[1 260]

;armrest
(define armrest #f)                       ;[yes/no] [p: asf or asb or ass, s: bua, bca]
(define armrest-depth 280)                ;[254 305 mm]
(define armrest-Δdepth-rear 50)           ;[0 100 %] of previous

(define armrest-sround #f)                ;[yes/no]
(define armrest-sround-diameter 20)       ;[1 260]
(define armrest-ssquare #f)               ;[yes/no]
(define armrest-ssquare-width 20)        ;[1 260]
(define armrest-ssquare-height 20)         ;[1 260]



;----------------------------------------------------------------------------------------------
;------CHAIR FUNCTION------
;----------------------------------------------------------------------------------------------

(define (chair
         
         ;----------VARIABLES----------  
         ;---GUIDES---
         #:guides [g guides]
         
         ;mode
         #:mode-wireframe [mw mode-wireframe]
         #:mode-solid [ms mode-solid]
         
         ;template-chair
         #:template-chair-none [tc-0 template-chair-none]
         #:template-chair-thonet14 [tc-t14 template-chair-thonet14]
         #:template-chair-thonet15 [tc-15 template-chair-thonet15]
         #:template-chair-thonet18 [tc-18 template-chair-thonet18]
         #:template-chair-random [tc-r template-chair-random]
         
         
         ;---LEGS---
         ;leg-front 
         #:leg-front [lf leg-front]
         #:leg-front-Δwidth [lf-Δw leg-front-Δwidth]
         #:leg-front-Δdepth [lf-Δd leg-front-Δdepth]
         #:leg-front-angle-splay [lf-αs leg-front-angle-splay]
         #:leg-front-angle-rake [lf-αr leg-front-angle-rake]
         
         #:leg-front-sround [lf-sr leg-front-sround]
         #:leg-front-sround-diameter [lf-srd leg-front-sround-diameter]
         #:leg-front-ssquare [lf-ss leg-front-ssquare]
         #:leg-front-ssquare-width [lf-ssw leg-front-ssquare-width]
         #:leg-front-ssquare-depth [lf-ssd leg-front-ssquare-depth]
         #:leg-front-angle-taper [lf-αt leg-front-angle-taper]
         
         ;leg-back  
         #:leg-back [lb leg-back]
         #:leg-back-Δwidth [lb-Δw leg-back-Δwidth]
         #:leg-back-Δdepth [lb-Δd leg-back-Δdepth]
         #:leg-back-angle-splay [lb-αs leg-back-angle-splay]
         #:leg-back-angle-rake [lb-αr leg-back-angle-rake]
         
         #:leg-back-sround [lb-sr leg-back-sround]
         #:leg-back-sround-diameter [lb-srd leg-back-sround-diameter]
         #:leg-back-ssquare [lb-ss leg-back-ssquare]
         #:leg-back-ssquare-width [lb-ssw leg-back-ssquare-width]
         #:leg-back-ssquare-depth [lb-ssd leg-back-ssquare-depth]
         #:leg-back-angle-taper [lb-αt leg-back-angle-taper]
  
         ;leg-centre
         #:leg-centre [lc leg-centre]
         
         #:leg-centre-sround [lc-sr leg-centre-sround]
         #:leg-centre-sround-diameter [lc-srd leg-centre-sround-diameter]
         #:leg-centre-ssquare [lc-ss leg-centre-ssquare]
         #:leg-centre-ssquare-width [lc-ssw leg-centre-ssquare-width]
         #:leg-centre-ssquare-depth [lc-ssd leg-centre-ssquare-depth]
         #:leg-centre-angle-taper [lc-αt leg-centre-angle-taper]
         
         
         ;---SEAT---
         ;seat-area
         #:seat-area [sa seat-area]
         #:seat-area-width [sa-w seat-area-width]
         #:seat-area-depth [sa-d seat-area-depth]
         #:seat-area-height [sa-h seat-area-height]
         #:seat-area-angle-rake [sa-αr seat-area-angle-rake]
         #:seat-area-radius-front [sa-rf seat-area-radius-front]
         #:seat-area-radius-rear [sa-rr seat-area-radius-rear]
         #:seat-area-Δwidth-taper [sa-Δwt seat-area-Δwidth-taper]
         
         ;seat-panel
         #:seat-panel [sp seat-panel]
         #:seat-panel-thickness [sp-t seat-panel-thickness]
         
         ;-SEAT-OUTER-
         ;seat-front
         #:seat-front [sf seat-front]
         
         ;seat-back
         #:seat-back [sb seat-back]
         
         ;seat-side
         #:seat-side [ss seat-side]
         #:seat-side-depth [ss-d seat-side-depth]
         
         ;section
         #:seat-outer-sround [so-sr seat-outer-sround]
         #:seat-outer-sround-diameter [so-srd seat-outer-sround-diameter]
         #:seat-outer-ssquare [so-ss seat-outer-ssquare]
         #:seat-outer-ssquare-height [so-ssh seat-outer-ssquare-height]
         #:seat-outer-ssquare-width [so-ssw seat-outer-ssquare-width]
         
         ;-SEAT-INNER-
         ;seat-cross
         #:seat-cross [sc seat-cross]
         #:seat-cross-Δdepth [sc-Δd seat-cross-Δdepth]
         
         ;seat-long
         #:seat-long [sl seat-long]
         #:seat-long-Δwidth-front [sl-Δwf seat-long-Δwidth-front]
         #:seat-long-Δwidth-rear [sl-Δwr seat-long-Δwidth-rear]
         
         ;seat-radial
         #:seat-radial [sr seat-radial]
         #:seat-radial-number [sr-n seat-radial-number]
         
         ;section
         #:seat-inner-sround [si-sr seat-inner-sround]
         #:seat-inner-sround-diameter [si-srd seat-inner-sround-diameter]
         #:seat-inner-ssquare [si-ss seat-inner-ssquare ]
         #:seat-inner-ssquare-height [si-ssh seat-inner-ssquare-height]
         #:seat-inner-ssquare-width [si-ssw seat-inner-ssquare-width]
         
         
         ;---BACK---
         ;back-area
         #:back-area [ba back-area]
         #:back-area-height [ba-h back-area-height]
         #:back-area-Δheight [ba-Δh back-area-Δheight]
         #:back-area-angle-rake [ba-αr back-area-angle-rake]
         #:back-area-radius-top [ba-rt back-area-radius-top]
         #:back-area-radius-bottom [ba-rb back-area-radius-bottom]
         
         ;back-panel
         #:back-panel [bp back-panel]
         #:back-panel-thickness [bp-t back-panel-thickness]
         
         ;-BACK-OUTER
         ;back-upright
         #:back-upright [bu back-upright]
         #:back-upright-height [bu-h back-upright-height]
         #:back-upright-angle-splay [bu-αs back-upright-angle-splay]
         
         ;back-top
         #:back-top [bt back-top]
         
         ;section
         #:back-outer-sround [bo-sr back-outer-sround]
         #:back-outer-sround-diameter [bo-srd back-outer-sround-diameter]
         #:back-outer-ssquare [bo-ss back-outer-ssquare]
         #:back-outer-ssquare-depth [bo-ssd back-outer-ssquare-depth]
         #:back-outer-ssquare-width [bo-ssw back-outer-ssquare-width]
         
         ;-BACK-INNER-
         ;back-cross-top
         #:back-cross-top [bct back-cross-top]
         #:back-cross-top-height [bct-h back-cross-top-height]
         
         ;back-cross-bottom
         #:back-cross-bottom [bcb back-cross-bottom]
         #:back-cross-bottom-height [bcb-h back-cross-bottom-height]
         
         ;back-cross-arm
         #:back-cross-arm [bca back-cross-arm]
         
         ;back-splat
         #:back-splat [bs back-splat]
         #:back-splat-Δwidth-top [bs-Δwt back-splat-Δwidth-top]
         #:back-splat-Δwidth-bottom [bs-Δwb back-splat-Δwidth-bottom]
         
         ;back-radial
         #:back-radial [br back-radial]
         
         ;section
         #:back-inner-sround [bi-sr back-inner-sround]
         #:back-inner-sround-diameter [bi-srd back-inner-sround-diameter]
         #:back-inner-ssquare [bi-ss back-inner-ssquare]
         #:back-inner-ssquare-depth [bi-ssd back-inner-ssquare-depth]
         #:back-inner-ssquare-width [bi-ssw back-inner-ssquare-width]

         
         ;---LEG-STRETCHER---
         ;leg-stretcher-area
         #:leg-stretcher-area [lsa leg-stretcher-area]
         #:leg-stretcher-area-height [lsa-h leg-stretcher-area-height]
         #:leg-stretcher-area-angle-rake [lsa-αr leg-stretcher-area-angle-rake]
         #:leg-stretcher-area-radius-front [lsa-rf leg-stretcher-area-radius-front]
         #:leg-stretcher-area-radius-rear [lsa-rr leg-stretcher-area-radius-rear]
         
         ;LEG-STRETCHER-OUTER
         ;leg-stretchers-front
         #:leg-stretcher-front [lsf leg-stretcher-front]
         #:leg-stretcher-front-height [lsf-h leg-stretcher-front-height]
         
         ;leg-stretcher-back
         #:leg-stretcher-back [lsb leg-stretcher-back]
         #:leg-stretcher-back-height [lsb-h leg-stretcher-back-height]
         
         ;leg-stretcher-side
         #:leg-stretcher-side [lss leg-stretcher-side]
         #:leg-stretcher-side-height [lss-h leg-stretcher-side-height]
         
         ;section
         #:leg-stretcher-outer-sround [lso-sr leg-stretcher-outer-sround]
         #:leg-stretcher-outer-sround-diameter [lso-srd leg-stretcher-outer-sround-diameter]
         #:leg-stretcher-outer-ssquare [lso-ss leg-stretcher-outer-ssquare]
         #:leg-stretcher-outer-ssquare-height [lso-ssh leg-stretcher-outer-ssquare-height]
         #:leg-stretcher-outer-ssquare-width [lso-ssw leg-stretcher-outer-ssquare-width]
         
         ;LEG-STRETCHER-INNER
         ;leg-stretcher-cross
         #:leg-stretcher-cross [lsc leg-stretcher-cross]
         #:leg-stretcher-cross-Δdepth [lsc-Δd leg-stretcher-cross-Δdepth]
         
         ;leg-stretcher-radial
         #:leg-stretcher-radial [lsr leg-stretcher-radial]
         #:leg-stretcher-radial-height [lsr-h leg-stretcher-radial-height]
         
         ;section
         #:leg-stretcher-inner-sround [lsi-sr leg-stretcher-inner-sround]
         #:leg-stretcher-inner-sround-diameter [lsi-srd leg-stretcher-inner-sround-diameter]
         #:leg-stretcher-inner-ssquare [lsi-ss leg-stretcher-inner-ssquare]
         #:leg-stretcher-inner-ssquare-height [lsi-ssh leg-stretcher-inner-ssquare-height]
         #:leg-stretcher-inner-ssquare-width [lsi-ssw leg-stretcher-inner-ssquare-width]
         
         
         ;---LEG-BASE---
         ;leg-base-area
         #:leg-base-area [lba leg-base-area]
         #:leg-base-area-width [lba-w leg-base-area-width]
         #:leg-base-area-depth [lba-d leg-base-area-depth]
         #:leg-base-area-radius-front [lba-rf leg-base-area-radius-front]
         #:leg-base-area-radius-rear [lba-rr leg-base-area-radius-rear]
         
         ;leg-base-panel
         #:leg-base-panel [lbp leg-base-panel]
         #:leg-base-thickness [lbp-t leg-base-panel-thickness]
         
         ;LEG-BASE-OUTER
         ;leg-base-front
         #:leg-base-front [lbf leg-base-front]
         
         ;leg-base-back
         #:leg-base-back [lbb leg-base-back]
         
         ;leg-base-side
         #:leg-base-side [lbs leg-base-side]
         #:leg-base-side-depth [lbs-d leg-base-side-depth]
         
         ;section
         #:leg-base-outer-sround [lbo-sr leg-base-outer-sround]
         #:leg-base-outer-sround-diameter [lbo-srd leg-base-outer-sround-diameter]
         #:leg-base-outer-ssquare [lbo-ss leg-base-outer-ssquare]
         #:leg-base-outer-ssquare-height [lbo-ssh leg-base-outer-ssquare-height]
         #:leg-base-outer-ssquare-width [lbo-ssw leg-base-outer-ssquare-width]
         
         ;LEG-BASE-INNER
         ;leg-base-cross
         #:leg-base-cross [lbc leg-base-cross]
         #:leg-base-cross-Δdepth [lbc-Δd leg-base-cross-Δdepth]
         
         ;leg-base-radial
         #:leg-base-radial [lbr leg-base-radial]
         #:leg-base-radial-number [lbr-n leg-base-radial-number]
         
         ;section
         #:leg-base-inner-sround [lbi-sr leg-base-inner-sround]
         #:leg-base-inner-sround-diameter [lbi-srd leg-base-inner-sround-diameter]
         #:leg-base-inner-ssquare [lbi-ss leg-base-inner-ssquare]
         #:leg-base-inner-ssquare-height [lbi-ssh leg-base-inner-ssquare-height]
         #:leg-base-inner-ssquare-width [lbi-ssw leg-base-inner-ssquare-width]
         
         
         ;---ARMS---
         ;arm-area
         #:arm-area [aa arm-area]
         #:arm-area-height [aa-h arm-area-height]
         #:arm-area-angle-splay [aa-αs arm-area-angle-splay]
         #:arm-area-radius-front [aa-rf arm-area-radius-front]
         #:arm-area-radius-rear [aa-rr arm-area-radius-rear]
         
         ;arm-support-front
         #:arm-support-front [asf arm-support-front]
         #:arm-support-front-angle-rake [asf-αr arm-support-front-angle-rake]
         
         ;arm-support-back
         #:arm-support-back [asb arm-support-back]
         #:arm-support-back-angle-rake [asb-αr arm-support-back-angle-rake]
         
         ;arm-support-side
         #:arm-support-side [ass arm-support-side]
         #:arm-support-side-Δdepth [ass-Δd arm-support-side-Δdepth]
         
         #:arm-support-side-sround [ass-sr arm-support-side-sround]
         #:arm-support-side-sround-diameter [ass-srd arm-support-side-sround-diameter]
         #:arm-support-side-ssquare [ass-ss arm-support-side-ssquare]
         #:arm-support-side-ssquare-width [ass-ssw arm-support-side-ssquare-width]
         #:arm-support-side-ssquare-depth [ass-ssd arm-support-side-ssquare-depth]
         
         ;armrest
         #:armrest [ar armrest]
         #:armrest-depth [ar-d armrest-depth]
         #:armrest-Δdepth-rear [ar-Δdr armrest-Δdepth-rear]
         
         #:armrest-sround [ar-sr armrest-sround]
         #:armrest-sround-diameter [ar-srd armrest-sround-diameter]
         #:armrest-ssquare [ar-ss armrest-ssquare]
         #:armrest-ssquare-width [ar-ssw armrest-ssquare-width]
         #:armrest-ssquare-height [ar-ssh armrest-ssquare-height]     
         
         )
 

  ;---------------GUIDES---------------
  ;------------------------------------

  ;CONVERSIONS (DEGREE > RADIANS)
  (define sa-αr-rad (deg>rad sa-αr))
  (define ba-αr-rad (deg>rad ba-αr))
  
  ;GUIDES
  
  ;POINTS
  ;absolute origin
  (define PO (xyz 0 0 0)) ;midpoint of the base front
  ;initial bounding box
  (define P1 (+z PO sa-h)) ;midpoint of the seat front
  (define P2 (+sph P1 sa-d pi/2 (+ pi/2 sa-αr-rad))) ;midpoint of the seat rear
  (define P3 (+sph P2 ba-h pi/2 (- (+ sa-αr-rad ba-αr-rad) pi/2))) ;midpoint of the back top
  (define P4 (xyz (cx P3) (cy P3) (cz PO))) ;midpoint of the base rear   
  
  ;GEOMETRY
  #;(define guides
    (when g
        (with-current-layer 
         "guides"  
         (line PO (+x PO (- (/ sa-w 2))) (+x P1 (- (/ sa-w 2))) P1) ;front panel
         (line P1 (+x P1 (- (/ sa-w 2))) (+x P2 (- (/ sa-w 2))) P2) ;seat panel
         (line P2 (+x P2 (- (/ sa-w 2))) (+x P3 (- (/ sa-w 2))) P3) ;back panel
         (line P3 (+x P3 (- (/ sa-w 2))) (+x P4 (- (/ sa-w 2))) P4) ;rear panel
         (line (+x PO (- (/ sa-w 2))) (+x P4 (- (/ sa-w 2)))))))
    
  
  ;---------SEAT-AREA----------
  

  ;CONVERSIONS (PERCENTAGE > MILLIMETERS)
  ;seat-area-Δwidth-taper (max = 100% = sa-w/2)
  (define sa-Δwt-mm
    (/ (* sa-Δwt (/ sa-w 2)) 100))
  ;seat-area-radius-front (max = 100% = sa-w/2 or sa-d/2)
  (define sa-rf-mm
    (cond ((<= (/ sa-d 2) (/ sa-w 2)) (/ (* sa-rf (/ sa-d 2)) 100)) ;currently, always sa-d <= sa-w
          ((< (/ sa-w 2) (/ sa-d 2)) (/ (* sa-rf (/ sa-w 2)) 100))))
  ;seat-area-radius-rear (max = 100% = (sa-w/2 - sa-Δwt-mm) or sa-d)
  (define sa-rr-mm
    (cond ((<= (- (/ sa-w 2) sa-Δwt-mm) (/ sa-d 2)) (/ (* sa-rr (- (/ sa-w 2) sa-Δwt-mm)) 100))
          ((< (/ sa-d 2) (- (/ sa-w 2) sa-Δwt-mm)) (/ (* sa-rr (/ sa-d 2)) 100))))
  ;POINTS
  ;new ucs (on point P1)
  (define origin-seat (xyz-on P1 (x 1) (norm-c (-c P2 P1))))  ;(xyz-on origin x-axis y-axis) ;norm-c: vector length = 1
  ;mid-points
  (define SFM origin-seat)
  (define SRM (+y origin-seat sa-d))
  ;quadrant-points
  (define SFF (+x SFM (+ (- (/ sa-w 2)) sa-rf-mm)))
  (define SRR (+x SRM (+ (- (/ sa-w 2)) sa-Δwt-mm sa-rr-mm))) 
  ;center-points
  (define SFC (+y SFF sa-rf-mm))
  (define SRC (+y SRR (- sa-rr-mm)))
  ;tangent-points
  (define SFR (+pol SFC sa-rf-mm (+ (θ-slope SRC SFC) (δ-tan-2circles sa-rf-mm sa-rr-mm SFC SRC))))
  (define SRF (+pol SRC sa-rr-mm (+ (θ-slope SRC SFC) (δ-tan-2circles sa-rf-mm sa-rr-mm SFC SRC))))
  ;reference-angles (half arc)
  (define sa-βf (/ (- (/ 3pi 2) (+ (θ-slope SRC SFC)(δ-tan-2circles sa-rf-mm sa-rr-mm SFC SRC))) 2))
  (define sa-βr (/ (δ-tan-2circles sa-rf-mm sa-rr-mm SFC SRC) 2))
  ;reference-points
  (define SF (+pol SFC sa-rf-mm (+ (θ-slope SRC SFC) (δ-tan-2circles sa-rf-mm sa-rr-mm SFC SRC) sa-βf)))  ;associated with LF
  (define SR (+pol SRC sa-rr-mm (+ (θ-slope SRC SFC) sa-βr)))  ;associated with LB
  ;GEOMETRY
  (define seat-area
    (when sa
      (join-curves-new
       (append ;joins lists
        (list
         (line-new SFM SFF)
         (arc-new SFC sa-rf-mm (/ (* 3 pi) 2) (- (* 2 sa-βf)))
         (line-new SFR SRF)
         (arc-new SRC sa-rr-mm pi/2 (+ (- (θ-slope SRC SFC) pi/2) (* 2 sa-βr)))
         (line-new SRR SRM))
         (if sp (list (line-new SRM SFM)) '())) ;if panel is #t, the curve has to be closed
         )))
  
  
  ;---------BACK-AREA----------
  
  ;---------TEMPLATE-CHAIR-RANDOM----------
  
  ;CONDITIONS (TO BE A CHAIR):
  ;(or (and lf lb) lf lb lc)
  ;(when (or lf lb) lbs)
  ;(when lc (or lbr lbp))
  ;sp
  ;(or bp (and bu (or bc bt)))
  
 
  
  ;---------------LEGS---------------
  ;----------------------------------

  ;---------LEG-FRONT----------
  ;CONVERSIONS (DEGREE > RADIANS)
  ;leg-front
  (define lf-αs-rad (deg>rad lf-αs))
  (define lf-αr-rad (deg>rad lf-αr))
  ;arm-area
  (define aa-αs-rad (deg>rad aa-αs))
  ;arm-support-front
  (define asf-αr-rad (deg>rad asf-αr))
  
  ;CONVERSIONS (PERCENTAGE > MILLIMETERS)
  ;leg-front-Δwidth(max = 100% = distance SF & axis)
  (define lf-Δw-mm
    (/ (* lf-Δw (distance SF (xyz 0 (cy SF) (cz SF)))) 100))
  ;leg-front-Δdepth(max = 100% = distance SF & axis)
  (define lf-Δd-mm
    (/ (* lf-Δd (distance SF (xyz (cx SF) (/ sa-d 2) (cz SF)))) 100))
  
  ;PONITS
  ;point in seat
  (define LFS
    (when lf
      (as-world ;the point is no longer affected by origin-seat 
      (+xy SF lf-Δw-mm lf-Δd-mm)))) ;construction affected by origin-seat
  ;point in base
  (define LFB
    (when lf
      (+xyz LFS
       (- (* (tan lf-αs-rad) (cz LFS)))
       (- (* (tan lf-αr-rad) (cz LFS)))
       (- (cz LFS)))))
  ;point in arm (arm-support-front)
  (define LFA 
    (when (and lf asf)
      (let ((ad (/ aa-h (cos (+ (- (+ ba-αr-rad sa-αr-rad) pi/2) asf-αr-rad)))))
        (+xyz LFS 
              (- (* ad (cos asf-αr-rad) (tan aa-αs-rad)))
              (- (* ad (sin asf-αr-rad)))
              (* ad (cos asf-αr-rad))))))
  
  ;GEOMETRY
  (define leg-front
    (cond 
      ((and lf (not asf)) (line LFB LFS))
      ((and lf asf) (line LFB LFS LFA))))
      
  
  ;---------LEG-REAR----------
  ;CONVERSIONS (DEGREE > RADIANS)
  ;leg-back
  (define lb-αs-rad (deg>rad lb-αs))
  (define lb-αr-rad (deg>rad lb-αr))
  ;arm-support-back
  (define asb-αr-rad (deg>rad asb-αr))
  ;back-upright
  (define bu-αs-rad (deg>rad bu-αs))
  
  ;CONVERSIONS (PERCENTAGE > MILLIMETERS)
  ;leg-back-Δwidth(max = 100% = distance SR & axis)
  (define lb-Δw-mm
    (/ (* lb-Δw (distance SR (xyz 0 (cy SR) (cz SR)))) 100))
  ;leg-front-Δdepth(max = 100% = distance SR & axis)
  (define lb-Δd-mm
    (/ (* lb-Δd (distance SR (xyz (cx SR) (/ sa-d 2) (cz SR)))) 100))
  ;back-upright-height (max = 100% = ba-h)
  (define bu-h-mm
    (/ (* bu-h ba-h) 100))
  
  ;PONITS
  ;point in seat
  (define LBS (when lb
      (as-world (+xy SR lb-Δw-mm lb-Δd-mm))))
  ;point in base
  (define LBB
    (when lb
      (+xyz LBS
       (- (* (tan lb-αs-rad) (cz LBS)))
       (* (tan lb-αr-rad) (cz LBS))
       (- (cz LBS)))))
  ;point in arm (arm-support-back)
    (define LBA 
    (when (and lb asb)
      (let ((ad (/ aa-h (cos (+ (- (+ ba-αr-rad sa-αr-rad) pi/2) asb-αr-rad)))))
        (+xyz LBS 
              (- (* ad (cos asb-αr-rad) (tan aa-αs-rad)))
              (* ad (sin asb-αr-rad))
              (* ad (cos asb-αr-rad))))))
  ;point in top back (back-upright)
  (define LBT
    (when (or (and lb bu) (and (not lb) bu ss))
      (+xyz LBS
            (- (* bu-h-mm (cos (- (+ ba-αr-rad sa-αr-rad) pi/2)) (tan bu-αs-rad)))
            (* bu-h-mm (sin (- (+ ba-αr-rad sa-αr-rad) pi/2)))
            (* bu-h-mm (cos (- (+ ba-αr-rad sa-αr-rad) pi/2))))))
                 
  ;GEOMETRY
  (define leg-back
    (cond 
      ((and lb (not asb) (not bu)) (line LBB LBS))
      ((and lb asb (not bu)) (line LBB LBS LBA))
      ((and lb (not asb) bu) (line LBB LBS LBT))))

  
  ;---------LEG-CENTRE----------
  ;POINTS
  ;point in seat
  (define LCS (when lc (as-world (+y origin-seat (/ sa-d 2)))))
  ;point in base
  (define LCB (when lc (as-world (+xyz LCS 0 0 (- (cz LCS))))))
  
  ;GEOMETRY
  (define leg-centre (when lc (line LCS LCB)))
  
 
  ;---------------LEG-CONNECTIONS (GENERAL FUNCTIONS)---------------
  ;-----------------------------------------------------------------
  
  ;CIRCUMCENTER
  (define (circumcenter PLF PLB) ;point in front leg & point in back leg
    (let*
        ((M1 (point-in-line PLF PLB 50)) ;mid-point
         (M2 (as-world                   ;mid-to-center-point
              (+pol
               (xyz-on M1 (x 1) (norm-c (-c (xyz (cx PLF) (cy PLB) (cz PLB)) PLF)))
               1
               (- (θ-slope PLB PLF) pi/2))))
         (AF (xyz 0 (cy PLF) (cz PLF))) ;axis-front-point
         (AB (xyz 0 (cy PLB) (cz PLB))) ;axis-back-point
         (CP ;curcumcenter-projection (on plane xy)
          (intersection-lines AF AB M1 M2))
         (AFP (xyz 0 (cy PLF) 0)) ;af-projection
         (ABP (xyz 0 (cy PLB) 0)) ;ab-projection
         (t (* (/ (distance AFP CP) (distance AFP ABP)) 100))) ;linear interpolation (in percentage)
      (point-in-line AF AB t)))
  
  ;----------AREAS----------
  ;general-function
  (define (area 
           rf rr ;front and rear radius
           PLF PLB ; front and rear points in legs
           a-w a-d) ;widtha and depth
    (when (and lf lb)
      ;CONVERSIONS (PERCENTAGE > MILLIMETERS)
      ;radius-front (max = 100% = distance to circumcenter)
      (define rf-mm
          (/ (* rf (distance (circumcenter PLF PLB) PLF)) 100))
      ;radius-rear (max = 100% = distance to circumcenter)
      (define rr-mm
          (/ (* rr (distance (circumcenter PLF PLB) PLB)) 100))
      ;POINTS
      ;new ucs (on point PF and PB?)
      ;center-points
      (define CF (+pol PLF rf-mm (/ (θ-slope PLB PLF) 2)))
      (define CB (+pol PLB rr-mm (- (/ (θ-slope PLB PLF) 2))))
      ;quadrant-points
      (define QF (+pol CF rf-mm (/ 3pi 2)))
      (define QB (+pol CB rr-mm pi/2))
      ;mid-points
      (define MF (xyz 0 (cy QF) (cz QF)))
      (define MB (xyz 0 (cy QB) (cz QB)))
      ;tangent-points
      (define TF (+pol CF rf-mm (+ (θ-slope CB CF) (δ-tan-2circles rf-mm rr-mm CF CB))))
      (define TB (+pol CB rr-mm (+ (θ-slope CB CF) (δ-tan-2circles rf-mm rr-mm CF CB))))
      ;reference-angles (half arc)
      (define βf (/ (- (/ 3pi 2) (+ (θ-slope CB CF)(δ-tan-2circles rf-mm rr-mm CF CB))) 2))
      (define βr (/ (δ-tan-2circles rf-mm rr-mm CF CB) 2))
      ;GEOMERY
        (join-curves-new
         (append (list
           (line-new MF QF)
           (arc-new CF rf-mm (/ (* 3 pi) 2) (- (* 2 βf)))
           (line-new TF TB)
           (arc-new CB rr-mm pi/2 (+ (- (θ-slope CB CF) pi/2) (* 2 βr)))
           (line-new QB MB)))))
    (when lc
      ;CONVERSIONS (PERCENTAGE > MILLIMETERS)
      ;radius-front (max = 100% = a-w/2 or a-d/2)
      (define a-rf-mm
        (cond ((<= (/ a-d 2) (/ a-w 2)) (/ (* rf (/ a-d 2)) 100))
              ((< (/ a-w 2) (/ a-d 2)) (/ (* rf (/ a-w 2)) 100))))
      ;radius-rear (max = 100% = a-w/2 or a-d)
      (define a-rr-mm
        (cond ((<= (/ a-d 2) (/ a-w 2)) (/ (* rr (/ a-d 2)) 100))
              ((< (/ a-w 2) (/ a-d 2)) (/ (* rr (/ a-w 2)) 100))))
      ;POINTS
      ;origin
      (define origin PO)
      ;mid-points
      (define FM origin)
      (define RM (+y origin a-d))
      ;quadrant-points
      (define FF (+x FM (+ (- (/ a-w 2)) a-rf-mm)))
      (define RR (+x RM (+ (- (/ a-w 2)) a-rr-mm)))
      ;center-points
      (define FC (+y FF a-rf-mm))
      (define RC (+y RR (- a-rr-mm)))
      ;tangent-points
      (define FR (+pol FC a-rf-mm (+ (θ-slope RC FC) (δ-tan-2circles a-rf-mm a-rr-mm FC RC))))
      (define RF (+pol RC a-rr-mm (+ (θ-slope RC FC) (δ-tan-2circles a-rf-mm a-rr-mm FC RC))))
      ;reference-angles (half arc)
      (define a-βf (/ (- (/ 3pi 2) (+ (θ-slope RC FC)(δ-tan-2circles a-rf-mm a-rr-mm FC RC))) 2))
      (define a-βr (/ (δ-tan-2circles a-rf-mm a-rr-mm FC RC) 2))
      ;GEOMETRY
      (join-curves-new
       (append ;joins lists
        (list
         (line-new FM FF)
         (arc-new FC a-rf-mm (/ (* 3 pi) 2) (- (* 2 a-βf)))
         (line-new FR RF)
         (arc-new RC a-rr-mm pi/2 (+ (- (θ-slope RC FC) pi/2) (* 2 a-βr)))
         (line-new RR RM))
        (if lbp (list (line-new RM FM)) '())) ;if panel is #t, the curve has to be closed
       ))
      )
  
  ;leg-base-area
  (define leg-base-area (when lba (area lba-rf lba-rr LFB LBB lba-w lba-d)))
  
  ;leg-stretchers-area
  (define leg-stretchers-area (when (and lsa lf lb)
                                (area lsa-rf lsa-rr (point-in-line LFB LFS lsa-h) (point-in-line LBB LBS  lsa-h) #f #f)))
  ;+rake angle
  
  
  ;----------OUTER FRAME----------
  ;general-function
    (define (outer-frame ;options: lf+lf, lr+lr, lf+lr
             name
             l1 ;leg 1
             l2 ;leg 2
             h) ;connection height
    (if (and name l1)
        (let* ((L1B (curve-start-point l1)) ;point in base of l1
               (L1S (curve-end-point l1)) ;point in seat of l1
               (L2B (if (equal? l1 l2)
                        (xyz (/ sa-w 2) (cy L1B) (cz L1B)) ;point in base of axis
                        (curve-start-point l2))) ;point in base of l2
               (L2S (if (equal? l1 l2)
                        (xyz (/ sa-w 2) (cy L1S) (cz L1S)) ;point in seat of axis
                        (curve-end-point l2))) ;point in seat of l2 
               (LC1 (point-in-line L1B L1S h)) ;connection pt1
               (LC2 (point-in-line L2B L2S h))) ;connection pt2
          (line LC1 LC2))
        #f))
  ;+if coincident with area
  
  ;seat-front
  (define seat-front (outer-frame sf leg-front leg-front 100))
  
  ;seat-back
  (define seat-back (outer-frame sb leg-back leg-back 100))
  
  ;seat-side
  (define seat-side (outer-frame ss leg-front leg-back 100))
  
  ;QUESTION: THE RAILS HAVE RADIUS WHEN ARE NOT COINCIDENT WITH SEAT?
  ;seat rails? no (or you would need to have 2 independent radius)
  ;stretchers? yes...
  

  ;----------RADIAL RAILS----------
  ;general function
  (define (radial-rail 
           name ;x-rail name
           PLF ;point in front leg
           PLB ;point in back leg
           PLC ;point in centre leg
           n) ;number of star-vertices
    ;DIAGONAL-RAIL (4 legs)
   ; (when (and name lf lb (not (or (= lf-Δw 100) (= lb-Δw 100))))
   ;     (let ((PX ;crossing point
   ;            (+xy (xyz-on (xyz 0 (cy PLF) (cz PLF)) (x 1) (norm-c (-c PLB PLF)))) ;new ucs
   ;                 0 ;x-coordinate
   ;                 (+ (/ (* (- (cy PLB) (cy PLF)) (- (/ s-l 2) (cx PLF))) 
   ;                       (- s-l (cx PLB) (cx PLF)))
   ;                    (cy PLF))))) ;y-coordinate
   ;        (line PLF PX PLB))
    ;RADIAL-RAIL (3 legs)
    (when (and lf lb name)
        (line PLF (circumcenter PLF PLB))
        (line PLB (circumcenter PLF PLB)))
    ;STAR-RAIL (1 leg)
    (when (and lc name)
      (define radius 
        (cond 
          ((equal? name sr)
           (if (>= (/ sa-w 2) (/ sa-d 2)) (/ sa-w 2) (/ sa-d 2)))
          ((equal? name lbr)
           (if (>= (/ lba-w 2) (/ lba-d 2)) (/ lba-w 2) (/ lba-d 2)))))
      (define dphi (/ 2pi n))
      (define (star-rail C radius n)
        (if (= n 0)
            #t
            (begin
              (line C (+pol C radius (+ pi/2 (* dphi (- n 1))))) ;starts in pi/2
              (star-rail C radius (- n 1)))))
      (star-rail PLC radius n))
    )
  
  ;seat-radial
  (define seat-radial (radial-rail sr LFS LBS LCS sr-n))
  
  ;leg-base-radial
  (define leg-base-radial (radial-rail lbr LFB LBB LCB lbr-n))
  
  ;leg-stretchers-radial
  (define leg-stretcher-radial 
    (when (and lf lb)
      (let ((LSF (point-in-line LFB LFS lsr-h))
            (LSB (point-in-line LBB LBS 
                                (if (>= lsa-αr 0)
                                (+ lsr-h (* (- 100 lsr-h) (/ lsa-αr 100)))
                                (+ lsr-h (* lsr-h (/ lsa-αr 100)))))))
        (radial-rail lsr LSF LSB #f #f))))
    
  
  
  #|
  ;----------SEAT-RAILS----------
  
  ;SEAT-FRONT
  (define sf 
    (if (and sf? lf?)
    (line LF1 (xyz (/ s-l 2) (cy LF1) (cz LF1)))
    #f))
  
  ;SEAT-REAR
  (define sr 
    (if (and sr? lr?)
    (line LR1 (xyz (/ s-l 2) (cy LR1) (cz LR1)))
    #f))
  
  ;SEAT-SIDE
  
  ;reverse-cantilever case (cantilever SSR is LR1)
  (define SSF
    (if (and ss? (not lf?) lr?)
        (+xy origin-seat (cx LR1) (- (cy LR1) ss-w))
        #f))
  
  (define ss 
    (cond 
      ((and ss? lf? lr?)
       (line LF1 LR1))
      ((and ss? lf? (not lr?))
       (line LF1 LR1))
      ((and ss? (not lf?) lr?)
       (line SSF LR1))
      (else #f)))
  
  ;SEAT-X
  (define sx (x-rail sx? origin-seat LF1 LR1))          
  
  ;SEAT-X-BOX
  (define sbx
    (if (and sxb? sb? lf? lr?)
        (list
         (line SF LF1)
         (line SR LR1)) ;;como avaliar os 2?!
        #f))
  
  
  ;SEAT-CROSS 
  (define sc
    (if (and sc? ss?)
        (let* ((SC (point-in-line LF1 LR1 sc-Δy))
               (SCm (xyz (/ s-l 2) (cy SC) (cz SC))))
        (line SC SCm))
        #f))
  
  ;SEAT-LENGTHWISE
  (define SLF 
    (if lf?
    (point-in-line LF1 (xyz (/ s-l 2) (cy LF1) (cz LF1)) sl-Δx-f)
    #f))
  
  (define SLR 
    (if lr?
    (point-in-line LR1 (xyz (/ s-l 2) (cy LR1) (cz LR1)) sl-Δx-r)
    #f))
  
  (define sl
    (cond
      ((and sl? lr? sf? (not sr?)) ;seat-LR-SF
       (line SLF LR1))
      ((and sl? lf? sr? (not sf?)) ;seat-LF-SR
      (line SLR LF1))
      ((and sl? sf? sr?) ;seat-lenhthwise
       (line SLF SLR))
      (else #f)))
  ;aqui juntaram-se 3 regras
  
  

  
 
  ;----------LEG-STRETCHERS----------
  
  ;LEG-STRETCHER-FRONT
  (define lsf
    (if (and lsf? lf?)
        (let* ((LSF (point-in-line LF LF1 lsf-h))
               (LSFm (xyz (/ s-l 2) (cy LSF) (cz LSF))))
          (line LSF LSFm))
        #f))
      
  ;LEG-STRETCHER-REAR
  (define lsr
    (if (and lsr? lr?)
        (let* ((LSR (point-in-line LR LR1 lsr-h))
               (LSRm (xyz (/ s-l 2) (cy LSR) (cz LSR))))
          (line LSR LSRm))
        #f))
  
  ;LEG-STRETCHER-SIDE
  (define LSSf 
    (if lf?
    (point-in-line LF LF1 lss-h)
    #f))
  
  (define LSSr 
    (if lr?
    (point-in-line LR LR1 lss-h)
    #f))
  
  (define lss
    (if (and lss? lf? lr?)      
        (line LSSf LSSr)
        #f))
  
  ;LEG-STRETCHER-X

#;  (define lsx 
    (if (and lsx? lf? lr?)
    (let* ((LSXf (point-in-line LF LF1 lsx-h))
           (LSXr (point-in-line LR LR1 lsx-h))
           (origin-stretchers (xyz-on (xyz 0 0 lsx-h) (x 1) (y 1))))
    (x-rail lsx? origin-stretchers LSXf LSXr))
    #f))
  ;ERRO
  
 (define lsx  
    (if (and lsx? lf? lr?)
        (let* ((LSXf (point-in-line LF LF1 lsx-h))
               (LSXr (point-in-line LR LR1 lsx-h))
               (LSXm
                (xyz
                 (/ s-l 2) 
                 (+ (/ (* (- (cy LSXr) (cy LSXf)) (- (/ s-l 2) (cx LSXf))) 
                       (- s-l (cx LSXr) (cx LSXf)))
                    (cy LSXf))
                 (cz LSXf))))
          (join-curves
           (line LSXr LSXm) 
           (line LSXf LSXm)))
        #f))
  
  ;LEG-STRETCHER-CROSS
  (define lsc
    (if (and lsc? lss?)
        (let* ((LSC (point-in-line LSSf LSSr lsc-Δy))
               (LSCm (xyz (/ s-l 2) (cy LSC) (cz LSC))))
          (line LSC LSCm))
        #f))
  
  
  ;----------LEG-BASE----------
  
  ;LEG-BASE-FRONT
  (define lbf 
    (if (and lbf? lf?)
    (line LF (xyz (/ s-l 2) (cy LF) (cz LF)))
    #f))
  
  ;LEG-BASE-REAR
  (define lbr 
    (if (and lbr? lr?)
    (line LR (xyz (/ s-l 2) (cy LR) (cz LR)))
    #f))
  
  ;LEG-BASE-SIDE
  (define lbs 
    (cond 
      ((and lbs? lf? lr?)
       (line LF LR))
      ((and lbs? lf? (not lr?))
       (line LF (+y LF lbs-w)))
    ((and lbs? (not lf?) lr?)
     (line LR (+y LR (- lbs-w))))
    (else #f)))
  
  ;LEG-BASE-X
  (define origin-base (xyz-on (xyz 0 0 0) (x 1) (y 1)))
  
  (define lbx (x-rail lbx? origin-base LF LR))
   
  
  ;LEG-BASE-CROSS 
  (define lbc
    (if (and lbc? lbs?)
        (let* ((LBC (point-in-line LF LR lbc-Δy))
               (LBCm (xyz (/ s-l 2) (cy LBC) (cz LBC))))
          (line LBC LBCm))
        #f))
  
  
  
  
  ;----------ARMS----------
  
  ;ARMCHAIR?
  (define armchair
    (if (and armchair? i?)
        (with-current-layer 
         "initial-shape"
        (polygon
         P1
         (+z P1 a-h)
         (intersection-lines (+z P1 a-h) (+z P2 a-h) P2 P3) ;this won't work!!!!!!!
         P2))
        #f))
  
  ;ARM-SUPPORT-SS
  (define AS1 (if as-ss? (point-in-line LR1 LF1 as-ss-Δy) #f))
  (define AS2 (if as-ss? (+z AS1 a-h) #f))
  
  (define as-ss 
    (if (and as-ss? ss?)
    (line AS1 AS2)
    #f))
  
  ;ARM-SUPPORT-LF
  ;(see below LF)
  ;this component doesn't exist, it's a redefinition of LF
  
  
  ;ARM-SUPPORT-LR
  ;(see below LR)
  ;this component doesn't exist, it's a redefinition of LR
     
  
  
  ;ARMREST
  (define ar
    (cond
      ((and ar? as-lf? as-lr? (not as-ss?))
       (line AS2F AS2R))
      ((and ar? as-lf? (not as-lr?) (not as-ss?))
       (line AS2F (+y AS2F ar-Δy-r)))
      ((and ar? (not as-lf?) as-lr? (not as-ss?))
       (line AS2R (+y AS2R (- ar-Δy-f))))
      ((and ar? (not as-lf?) (not as-lr?) as-ss?)
       (line (+y AS2 (- ar-Δy-f)) (+y AS2 ar-Δy-r)))
      (else #f)))
  
  
  

  
    ;----------BACK-RAILS----------
  
  ;BACK-UPRIGHT
  ;can be one of the next (2 rules):
  
  ;back-upright-lr
  ;(see below LR)
  ;this component doesn't exist, it's a redefinition of LR
  
  ;back-upright-ss
  (define bu-ss
    (if (and bu? ss? (not lr?))
    (line LR1 LR3)
    #f))
  
  ;BACK-UPRIGHT-AR
  (define bu-ar
    (if (and bu-ar? ar?)
        (line (curve-end-point ar) LR3)
        #f))

  ;BACK-TOP
  (define bt 
    (if (and bt? (or bu? bu-ar?))
        (line LR3 (xyz (/ s-l 2) (cy LR3) (cz LR3)))
        #f))
  
  ;BACK-CROSS
  (define bc
    (cond ((and bc? bu? (not bu-ar?))
           (let ((BC (point-in-line LR1 LR3 bc-Δh)))
             (line BC (xyz (/ s-l 2) (cy BC) (cz BC)))))
          ((and bc? (not bu?) bu-ar?) ;depends on bu-ar     
           (let ((BC (point-in-line (curve-start-point bu-ar) (curve-end-point bu-ar) bc-Δh)))
             (line BC (xyz (/ s-l 2) (cy BC) (cz BC)))))
          (else 
            #f)))
  
  ;BACK-CROSS-AR
  (define bc-ar
    (if (and bc-ar? ar?)
        (let ((BCAR (curve-end-point ar))) 
        (line BCAR (xyz (/ s-l 2) (cy BCAR) (cz BCAR))))
        #f))
  
  ;BACK-SPLAT
  (define bs
    (cond ((and bs? bt? (not bc?) sr?)
           (let ((BST (point-in-line (curve-end-point bt) (curve-start-point bt) bs-Δx))
                 (BS (point-in-line (curve-end-point sr) (curve-start-point sr) bs-Δx)))
             (line BST BS)))
          ((and bs? bt? bc? (not bc-ar?))
           (let ((BST (point-in-line (curve-end-point bt) (curve-start-point bt) bs-Δx))
                 (BS (point-in-line (curve-end-point bc) (curve-start-point bc) bs-Δx)))
             (line BST BS)))
          ((and bs? (not bt?) (not bc?) sr? bc-ar?)
           (let ((BST (point-in-line (curve-end-point bc-ar) (curve-start-point bc-ar) bs-Δx))
                 (BS (point-in-line (curve-end-point sr) (curve-start-point sr) bs-Δx)))
             (line BST BS)))
          (else #f)))
  
  ;BACK-X
#; (define bx
    (if (and bx? bu?)
        (let ((LR1r (xyz (+ (cx LR1) s-l) (cy LR1) (cz LR1))) ;can't mirror points
              (LR3r (xyz (+ (cx LR3) s-l) (cy LR3) (cz LR3))))
          (join-curves
           (line LR3 LR1r))
          (line LR1 LR3r))
        #f))
  ;this draws also the right-side = ERROR
  
  
  
 #; (define bx  
    (if (and bx? bu?)
        (let* ((origin-back (xyz-on LR1 (x 1) (norm-c (-c LR3 LR1))))     
               (BXm (PXm origin-back LR1 LR3)))
          (join-curves
           (line LR1 BXm) 
           (line LR3 BXm)))
        #f))
  ;ERROR
  
              
  ;-----------SEAT SURFACES----------  
  
  ;SEAT
  (define s 
    (if s?
    (surface-polygon
                P1
                (+x P1 (/ s-l 2))
                (+x P2 (/ s-l 2))
                P2)
    #f))
  
  ;SEAT-SB
  (define s-sb 
    (if (and sb? s-sb?)
          (surface sb)
        #f))
  
              
  
  ;SEAT-SS
  (define s-ss
    (if (and s-ss? ss?)
        (let ((SSF (point-in-line (curve-start-point ss) (curve-end-point ss) s-ss-Δy-f))
              (SSR (point-in-line (curve-start-point ss) (curve-end-point ss) s-ss-Δy-r)))
          (surface-polygon
           SSF
           (xyz (/ s-l 2) (cy SSF) (cz SSF))
           (xyz (/ s-l 2) (cy SSR) (cz SSR))
           SSR))
        #f))
         

  ;-----------BACK SURFACES----------  
  
  ;BACK
  (define b
    (if b?
        (surface-polygon
         P2
         (+x P2 (/ s-l 2))
         (+x P3 (/ s-l 2))
         P3)
        #f))
         
  
  ;BACK-BU
  (define b-bu
    (cond ((and b-bu? bu? (not bu-ar?))
           (let ((LB (point-in-line LR1 LR3 b-Δh)))
             (surface-polygon 
              LR3 
              (xyz (/ s-l 2) (cy LR3) (cz LR3)) 
              (xyz (/ s-l 2) (cy LB) (cz LB)) 
              LB)))
          ((and b-bu? (not bu?) bu-ar?)
           (let ((LB (point-in-line (curve-start-point bu-ar) (curve-end-point bu-ar) b-Δh)))
             (surface-polygon 
              LR3 
              (xyz (/ s-l 2) (cy LR3) (cz LR3)) 
              (xyz (/ s-l 2) (cy LB) (cz LB)) 
              LB)))
          (else #f)))
  
  ;BACK-BC
  
  ;SHELL
  ;malha 
  ;solid or mesh
  
  ;back-curve-profile-lumbar
  ;back-curve-profile-thoracic
  ;back-curve-profile-border
  ;back-curve-cross-lumbar
  
  ;seat-curve-profile-buttocks
  ;seat-curve-profile-hips
  ;seat-curve-profile-border
  ;seat-curve-cross-buttocks
  

  ;----------DEPENDENT VARIABLES-----------
  
  ;(define l (bbox-length-x (bounding-box (all-shapes))))
  ;(define w (bbox-length-y (bounding-box (all-shapes))))
  ;(define h (bbox-length-z (bounding-box (all-shapes)))) 
  
  ;----------SOLID----------
  
    (set! sb
          (if (and (or so-fr? so-fs?) s-sb?)
            (join-curves-new
             (list
              (line-new SFM SFF)
              (arc-new SFC sb-rf-mm (/ (* 3 pi) 2) (- (* 2 (- pi/2 (/ (- pi/2 sb-αt-rad) 2)))))
              (line-new SFR SRF)
              (arc-new SRC sb-rr-mm pi/2 (* 2 (- pi/2 (/ (+ pi/2 sb-αt-rad) 2))))
              (line-new SRR SRM)))
            sb))
  ;;;superficie apaga linha para o sweep
  ;;;sweep apaga linha para superficie
  
  ;FRAME
    (define frame 
    (append  
     (list 
      lf lr 
      sb sf sr ss sx sc sl
      lsf lsr lss lsx lsc
      lbf lbr lbs lbx lbc
      bu-ss bu-ar bt bc bc-ar bs ;bx
      as-ss ar)
     (if sbx sbx (list))) ;pq o sbx é uma lista
    )
  
  
  ;FRAME-ROUND 
  (define so-fr
    (if so-fr?
    (for ((el frame))
      (when el
        (sweep  el (surface-circle (u0) so-fr-r))))
    #f))
      
   
  ;FRAME-SQUARE
  (define so-sf
    (if so-fs?
    ;(displayln frame) mostra o que acontece aos elementos da lista
    (for ((el frame))
      (when el
        (sweep el (surface-rectangle (xy 0 0) so-fs-l so-fs-w))))
    #f))
  
  
  ;SUPPORT
 (define support (list
                   s s-sb s-ss
                   b b-bu))
  
 (define so-s
   (if so-s?
   (for ((el support))
     (when el
    (extrusion el (z so-s-t))))
   #f))
  |#
  
  ;----------MIRROR----------
  
  (mirror (all-shapes) (x 0) (x sa-w)) ;draws the right side
  
  
  ;-----------close chair----------

  )



;----------------------------------------------------------------------------------------------
;------GRAPHICAL INTERFACE------
;----------------------------------------------------------------------------------------------

;------DEFAULT FUNCTIONS------

;To not make copies & for image not jump whem moving sliders
(define (new-chair)
  (disable-update)
  (delete-all-shapes)
  (chair)
  (enable-update)
  )

;;To link variables with sliders
(define sliders-table (make-hash))

(define (name-slider! name slider)
  (hash-set! sliders-table name slider))

(define (get-slider name)
  (hash-ref sliders-table name))

(define-syntax-rule
  (change-default name value)
  (let ((v value))
    (set! name v)
    (send (get-slider 'name) set-value v)))

;---SLIDER---
(define-syntax-rule (default-slider 
                      tab 
                      name 
                      lab 
                      min 
                      max 
                      enabled? 
                      extra-code ...)
  (name-slider! 
   'name
     (letrec
         ((tab-sliders
           (new horizontal-panel% 
                [parent tab]
                [alignment '(right center)]
                [stretchable-width #f]))
          (message
           (new message% 
                [parent tab-sliders] 
                [label lab]
                [min-width 95]))
          (slider
           (new slider%
                [parent tab-sliders]
                [label ""]
                [min-width 200]
                [min-value min]
                [max-value max]
                [init-value name] ;initial value = global variables
                [enabled enabled?]
                [horiz-margin 15]
                [style '(plain horizontal)] ;does not display numbers
                [callback 
                 (lambda (slider event)
                   (set! name (send slider get-value))
                   (begin extra-code ...)
                   (new-chair)
                   (send text-field set-value (number->string (send slider get-value))))])) ;slider writes number in text-field
       (text-field
       (new text-field%
            [parent tab-sliders]
            [label ""]
            [init-value (number->string name)]
            [min-width 40]
            [callback 
             (lambda (text-field event)
               (cond 
                 ((and (string->number (send text-field  get-value)) ;if the sring can be converted to a number, and
                       (exact-integer? (string->number (send text-field  get-value))) ;is an exact integer number (not 1.), and
                       (<= min (string->number (send text-field  get-value)) max)) ;is between (max slider) and (min slider):
                  (send slider set-value (string->number (send text-field get-value)))) ;changes slider value
                 ((equal? (send text-field  get-value) "-") ;if string is "-"
                  #f) ;do nothing
                 (else
                  (send text-field set-value "")))  ;nothing is written
               (new-chair))])))
       (begin tab-sliders message slider text-field))))


;---CHECK-BOX---
(define-syntax-rule (default-check-box
                      tab-parent
                      tab 
                      tab-sliders ;#f when there is no children
                      name 
                      lab
                      enabled?
                      extra-code ...)
  (name-slider!
   'name
   (new check-box%
        [value name]  ;initial value = global variables
        [parent tab-parent]
        [label lab]
        [callback
         (lambda (check-box event)
           (set! name (send check-box get-value))
           (cond ((and name tab-sliders)
                  (send tab add-child tab-sliders)) ;sliding menu - hide/show variables of each part 
                 ((and (not name) tab-sliders)
                  (send tab delete-child tab-sliders))
                 (else #t))
           (begin extra-code ...)
           (new-chair))]
        [enabled enabled?]
        )))

;SUBSEQUENT (CHECK-BOX)
(define (subsequent name check-box-subsequent tab-check-box-subsequent tab-slider-subsequent)
  (send (get-slider check-box-subsequent) enable name)     ;if check-box is on: enables subsequent
  (when (and (not name) (send (get-slider check-box-subsequent) get-value)) ;if check-box is off:
    (send tab-check-box-subsequent delete-child tab-slider-subsequent) ;removes its sliders
    (send (get-slider check-box-subsequent) set-value #f)  ;and disables subsequent     
  ))

;SUBSEQUENT (SLIDER)
(define (subsequent-slider name subsequent-slider)
  (send (get-slider subsequent-slider) enable name))

;PRECEDENT (CHECK-BOX AND SLIDER)
(define (precedent precedent-check-box)
  (send (get-slider precedent-check-box) get-value)) ;picks the value of the precedent to know if starts enabled

;---PANELS---
(define (default-tab-panel parent)
    (new vertical-panel% 
       [parent parent]
       [alignment '(left top)]
       [vert-margin 5]
       [horiz-margin 0]
       [stretchable-height #t] ;is stretched to fill all available space in a container
       [style '(vscroll)]
       [spacing 10] ;spacing (in pixels) used between subareas in the container
       ))

(define (default-panel parent style)
    (new vertical-panel% 
       [parent parent]
       [alignment '(left top)]
       [vert-margin 5]
       [horiz-margin 0]
       [stretchable-height #f] ;it always shrinks to its minimum size
       [style style]
       [spacing 5]
       ))


;---------------FRAME---------------
;-----------------------------------

(define frame (new frame% 
                   [label "ChairDNA"]
                   [width 400]
                   [height 800]))

;panel-master
(define panel-master (new horizontal-panel% 
                          [parent frame]))

;------TABS------
(define tab-panel 
  (new tab-panel%
       [parent panel-master]
       [choices (list "Guides" "Legs" "Seat" "Back" "Stretchers" "Base" "Arms")]
       [callback
        (lambda (tp event)
          (case (send tp get-selection)
            ((0) (send tp change-children (lambda (children) (list tab-guides))))
            ((1) (send tp change-children (lambda (children) (list tab-legs))))
            ((2) (send tp change-children (lambda (children) (list tab-seat))))
            ((3) (send tp change-children (lambda (children) (list tab-back))))
            ((4) (send tp change-children (lambda (children) (list tab-stretchers))))
            ((5) (send tp change-children (lambda (children) (list tab-base))))
            ((6) (send tp change-children (lambda (children) (list tab-arms))))
            ))]
       ))

(define tab-guides (default-tab-panel tab-panel))
(define tab-legs (default-tab-panel tab-panel))
(define tab-seat (default-tab-panel tab-panel))  
(define tab-back (default-tab-panel tab-panel))
(define tab-stretchers (default-tab-panel tab-panel))
(define tab-base (default-tab-panel tab-panel))
(define tab-arms (default-tab-panel tab-panel))

(send tab-panel change-children (lambda (children) (list tab-guides)))


;---------------TAB GUIDES---------------
;----------------------------------------	

;----------GUIDES----------
(define guides-seat-area #t)
(define guides-back-area #t)
(define guides-arm-area #f)

(define tab-g (default-panel tab-guides '()))
(define tab-g-sliders (default-panel tab-g '())) ;sliders are not hidden by default

(default-check-box
  tab-g tab-g tab-g-sliders
  guides
  "Guides"
  #t
  (when guides
    (send (get-slider 'guides-seat-area) set-value #t)
    (send (get-slider 'guides-back-area) set-value #t)
    (send (get-slider 'guides-arm-area) set-value #f)
    (send (get-slider 'seat-area) set-value #t)
    (send (get-slider 'back-area) set-value #t)
    (send tab-sa add-child tab-sa-sliders)
    (send tab-ba add-child tab-ba-sliders))
  (when (not guides)
    (send (get-slider 'seat-area) set-value #f)
    (send (get-slider 'back-area) set-value #f)
    (send (get-slider 'arm-area) set-value #f)
    (when tab-sa (send tab-sa change-children (lambda (children) (list (get-slider 'seat-area)))))
    (when tab-ba (send tab-ba change-children (lambda (children) (list (get-slider 'back-area)))))
    (when tab-aa (send tab-aa change-children (lambda (children) (list (get-slider 'arm-area))))))  
  )

(default-check-box
  tab-g-sliders #f #f
  guides-seat-area
  "Seat Area"
  #t
  (when guides-seat-area
    (send (get-slider 'seat-area) set-value #t)
    (send tab-sa add-child tab-sa-sliders))
  (when (not guides-seat-area)
    (send (get-slider 'seat-area) set-value #f)
    (when tab-sa (send tab-sa change-children (lambda (children) (list (get-slider 'seat-area))))))
  (when (and 
         (not (send (get-slider 'guides-seat-area) get-value)) 
         (not (send (get-slider 'guides-back-area) get-value))
         (not (send (get-slider 'guides-arm-area) get-value)))
    (send (get-slider 'guides) set-value #f)
    (send tab-g delete-child tab-g-sliders)))

(default-check-box
  tab-g-sliders #f #f
  guides-back-area
  "Back Area"
  #t
  (when guides-back-area
    (send (get-slider 'back-area) set-value #t)
    (send tab-ba add-child tab-ba-sliders))
  (when (not guides-back-area)
    (send (get-slider 'back-area) set-value #f)
    (when tab-ba (send tab-ba change-children (lambda (children) (list (get-slider 'back-area))))))
  (when (and 
         (not (send (get-slider 'guides-seat-area) get-value)) 
         (not (send (get-slider 'guides-back-area) get-value))
         (not (send (get-slider 'guides-arm-area) get-value)))
    (send (get-slider 'guides) set-value #f)
    (send tab-g delete-child tab-g-sliders)))

(default-check-box
  tab-g-sliders #f #f
  guides-arm-area
  "Arms Area"
  #t
  (when guides-arm-area
    (send (get-slider 'arm-area) set-value #t)
    (send tab-aa add-child tab-aa-sliders))
  (when (not guides-arm-area)
    (send (get-slider 'arm-area) set-value #f)
    (when tab-aa (send tab-aa change-children (lambda (children) (list (get-slider 'arm-area)))))
    (when (and 
           (not (send (get-slider 'guides-seat-area) get-value)) 
           (not (send (get-slider 'guides-back-area) get-value))
           (not (send (get-slider 'guides-arm-area) get-value)))
      (send (get-slider 'guides) set-value #f)
      (send tab-g delete-child tab-g-sliders))))

;----------SOLID MODE----------
(define tab-solid (default-panel tab-guides '()))

(new message% [parent tab-solid] [label "Solid Mode"])

(new radio-box%
     [parent tab-solid]
     [label ""]
     [choices (list "Wireframe" "Solid")])

;----------TEMPLATES----------
(define tab-templates (default-panel tab-guides '()))

(new message% [parent tab-templates] [label "Chair Templates"])

(new choice%
     [parent tab-templates]
     [label #f]
     [choices (list "None" "Thonet N14" "Thonet N15" "Thonet N18" "Random")]
     [callback 
      (lambda (choice event) 
        (case (send choice get-selection)
          ((0) #f)))])



;---------------TAB LEGS---------------
;--------------------------------------

;----------LEG FRONT----------
(define tab-lf (default-panel tab-legs '()))
(define tab-lf-sliders (default-panel tab-lf '(deleted)))

(default-check-box
  tab-lf tab-lf tab-lf-sliders
  leg-front
  "Front Leg"
  #t
  (send (get-slider 'leg-centre) enable (not leg-front))
  (subsequent leg-front 'seat-front #f #f)
  (subsequent (or leg-front leg-back) 'seat-side tab-ss tab-ss-sliders)
  (subsequent-slider (or (not leg-front) (not leg-back)) 'seat-side-depth)
  (subsequent (or (and leg-back seat-front) (and leg-front seat-back) (and seat-front seat-back)) 'seat-long tab-sl tab-sl-sliders)
  (subsequent (or (and leg-front leg-back) leg-centre) 'seat-radial tab-sr tab-sr-sliders)
  (subsequent leg-front 'leg-stretcher-front tab-lsf tab-lsf-sliders)
  (subsequent (and leg-front leg-back) 'leg-stretcher-side tab-lss tab-lss-sliders)
  (subsequent (and leg-front leg-back) 'leg-stretcher-radial tab-lsr tab-lsr-sliders)
  (subsequent leg-front 'leg-base-front #f #f)
  (subsequent (or leg-front leg-back) 'leg-base-side tab-lbs tab-lbs-sliders)
  (subsequent-slider (or (not leg-front) (not leg-back)) 'leg-base-side-depth)
  (subsequent (or (and leg-front leg-back) leg-centre) 'leg-base-radial tab-lbr tab-lbr-sliders)
  (subsequent leg-front 'arm-support-front tab-asf tab-asf-sliders))

(default-slider
  tab-lf-sliders
  leg-front-Δwidth
  "Width Spacing (%)"
  -100 100
  #t)

(default-slider
  tab-lf-sliders
  leg-front-Δdepth
  "Depth Spacing (%)"
  -100 100
  #t)

(default-slider
  tab-lf-sliders
  leg-front-angle-splay
  "Splay Angle (º)"
  -45 45
  #t)

(default-slider
  tab-lf-sliders
  leg-front-angle-rake
  "Rake Angle (º)"
  -45 45
  #t)

;SECTION
;tabs
(define tab-lf-section (default-panel tab-lf-sliders '()))
(define tab-lf-labels (new horizontal-panel% [parent tab-lf-section]))
(define tab-lf-sround-sliders (default-panel tab-lf-section '(deleted)))
(define tab-lf-ssquare-sliders (default-panel tab-lf-section '(deleted)))
  
;message
(new message% [parent tab-lf-labels] [label "Section"])
  
;round
(default-check-box
  tab-lf-labels tab-lf-section tab-lf-sround-sliders
   leg-front-sround
  "Round"
  #t
  (if (and (send (get-slider 'leg-front-sround) get-value) (send (get-slider 'leg-front-ssquare) get-value))
      (begin
        (send (get-slider 'leg-front-ssquare) set-value #f)
        (send tab-lf-section delete-child tab-lf-ssquare-sliders)) #f))
  
(default-slider
  tab-lf-sround-sliders
  leg-front-sround-diameter
  "Diameter (mm)"
  1 260
  #t)

;square
(default-check-box
  tab-lf-labels tab-lf-section tab-lf-ssquare-sliders
  leg-front-ssquare
  "Square"
  #t
  (if (and (send (get-slider 'leg-front-sround) get-value) (send (get-slider 'leg-front-ssquare) get-value))
      (begin
        (send (get-slider 'leg-front-sround) set-value #f)
        (send tab-lf-section delete-child tab-lf-sround-sliders)) #f))

(default-slider
  tab-lf-ssquare-sliders
  leg-front-ssquare-width
  "Width (mm)"
  1 260
  #t)

(default-slider
  tab-lf-ssquare-sliders
  leg-front-ssquare-depth
  "Depth (mm)"
  1 260
  #t)

;angle
(default-slider
  tab-lf-section
  leg-front-angle-taper
  "Taper Angle (%)"
  0 100
  #t)

;----------LEG BACK----------
(define tab-lb 
  (default-panel tab-legs '()))

(define tab-lb-sliders
  (default-panel tab-lb '(deleted)))

;check-box
(default-check-box
  tab-lb tab-lb tab-lb-sliders
  leg-back
  "Back Leg"
  #t
  (send (get-slider 'leg-centre) enable (not leg-back))
  (subsequent leg-back 'seat-back #f #f)
  (subsequent (or leg-back leg-front) 'seat-side tab-ss tab-ss-sliders)
  (subsequent-slider (or (not leg-front) (not leg-back)) 'seat-side-depth)
  (subsequent (or (and leg-back seat-front) (and leg-front seat-back) (and seat-front seat-back)) 'seat-long tab-sl tab-sl-sliders)
  (subsequent (or (and leg-back leg-front) leg-centre) 'seat-radial tab-sr tab-sr-sliders)
  (subsequent (or leg-back seat-side) 'back-upright tab-bu tab-bu-sliders)
  (subsequent leg-back 'leg-stretcher-back tab-lsb tab-lsb-sliders)
  (subsequent (and leg-front leg-back) 'leg-stretcher-side tab-lss tab-lss-sliders)
  (subsequent (and leg-front leg-back) 'leg-stretcher-radial tab-lsr tab-lsr-sliders)
  (subsequent leg-back 'leg-base-back #f #f)
  (subsequent (or leg-front leg-back) 'leg-base-side tab-lbs tab-lbs-sliders)
  (subsequent-slider (or (not leg-front) (not leg-back)) 'leg-base-side-depth)
  (subsequent (or (and leg-front leg-back) leg-centre) 'leg-base-radial tab-lbr tab-lbr-sliders)
  (subsequent leg-back 'arm-support-back tab-asb tab-asb-sliders))

(default-slider
  tab-lb-sliders
  leg-back-Δwidth
  "Width Spacing (%)"
  -100 100
  #t)

(default-slider
  tab-lb-sliders
  leg-back-Δdepth
  "Depth Spacing (%)"
  -100 100
  #t)

(default-slider
  tab-lb-sliders
  leg-back-angle-splay
  "Splay Angle (º)"
  -45 45
  #t)

(default-slider
  tab-lb-sliders
  leg-back-angle-rake
  "Rake Angle (º)"
  -45 45
  #t)

;SECTION
;tabs
(define tab-lb-section (default-panel tab-lb-sliders '()))
(define tab-lb-labels (new horizontal-panel% [parent tab-lb-section]))
(define tab-lb-sround-sliders (default-panel tab-lb-section '(deleted)))
(define tab-lb-ssquare-sliders (default-panel tab-lb-section '(deleted)))
  
;message
(new message% [parent tab-lb-labels] [label "Section"])
  
;round
(default-check-box
  tab-lb-labels tab-lb-section tab-lb-sround-sliders
   leg-back-sround
  "Round"
  #t
    (if (and (send (get-slider 'leg-back-sround) get-value) (send (get-slider 'leg-back-ssquare) get-value))
      (begin
        (send (get-slider 'leg-back-ssquare) set-value #f)
        (send tab-lb-section delete-child tab-lb-ssquare-sliders)) #f))
  
(default-slider
  tab-lb-sround-sliders
  leg-back-sround-diameter
  "Diameter (mm)"
  1 260
  #t)

;square
(default-check-box
  tab-lb-labels tab-lb-section tab-lb-ssquare-sliders
  leg-back-ssquare
  "Square"
  #t
  (if (and (send (get-slider 'leg-back-sround) get-value) (send (get-slider 'leg-back-ssquare) get-value))
      (begin
        (send (get-slider 'leg-back-sround) set-value #f)
        (send tab-lb-section delete-child tab-lb-sround-sliders)) #f))

(default-slider
  tab-lb-ssquare-sliders
  leg-back-ssquare-width
  "Width (mm)"
  1 260
  #t)

(default-slider
  tab-lb-ssquare-sliders
  leg-back-ssquare-depth
  "Depth (mm)"
  1 260
  #t)

;angle
(default-slider
  tab-lb-section
  leg-back-angle-taper
  "Taper Angle (%)"
  0 100
  #t)

;----------LEG CENTRE----------
(define tab-lc (default-panel tab-legs '()))
(define tab-lc-sliders (default-panel tab-lc '(deleted)))

(default-check-box
  tab-lc tab-lc tab-lc-sliders
  leg-centre
  "Centre Leg"
  #t
  (send (get-slider 'leg-front) enable (not leg-centre))
  (send (get-slider 'leg-back) enable (not leg-centre))
  (subsequent (or (and leg-back leg-front) leg-centre) 'seat-radial tab-sr tab-sr-sliders)
  (subsequent (or (and leg-front leg-back) leg-centre) 'leg-base-radial tab-lbr tab-lbr-sliders)
  (subsequent-slider leg-centre 'seat-radial-number)
  (subsequent-slider leg-centre 'leg-base-radial-number))

;SECTION
;tabs
(define tab-lc-section (default-panel tab-lc-sliders '()))
(define tab-lc-labels (new horizontal-panel% [parent tab-lc-section]))
(define tab-lc-sround-sliders (default-panel tab-lc-section '(deleted)))
(define tab-lc-ssquare-sliders (default-panel tab-lc-section '(deleted)))
  
;message
(new message% [parent tab-lc-labels] [label "Section"])
  
;round
(default-check-box
  tab-lc-labels tab-lc-section tab-lc-sround-sliders
   leg-centre-sround
  "Round"
  #t
      (if (and (send (get-slider 'leg-centre-sround) get-value) (send (get-slider 'leg-centre-ssquare) get-value))
      (begin
        (send (get-slider 'leg-centre-ssquare) set-value #f)
        (send tab-lc-section delete-child tab-lc-ssquare-sliders)) #f))
  
(default-slider
  tab-lc-sround-sliders
  leg-centre-sround-diameter
  "Diameter (mm)"
  1 260
  #t)

;square
(default-check-box
  tab-lc-labels tab-lc-section tab-lc-ssquare-sliders
  leg-centre-ssquare
  "Square"
  #t
    (if (and (send (get-slider 'leg-centre-sround) get-value) (send (get-slider 'leg-centre-ssquare) get-value))
      (begin
        (send (get-slider 'leg-centre-sround) set-value #f)
        (send tab-lc-section delete-child tab-lc-sround-sliders)) #f))

(default-slider
  tab-lc-ssquare-sliders
  leg-centre-ssquare-width
  "Width (mm)"
  1 260
  #t)

(default-slider
  tab-lc-ssquare-sliders
  leg-centre-ssquare-depth
  "Depth (mm)"
  1 260
  #t)

;angle
(default-slider
  tab-lc-section
  leg-centre-angle-taper
  "Taper Angle (%)"
  0 100
  #t)

;---------------TAB SEAT---------------
;--------------------------------------

;----------SEAT AERA----------
(define tab-sa (default-panel tab-seat '()))
(define tab-sa-sliders (default-panel tab-sa '(deleted)))

(default-check-box
  tab-sa tab-sa tab-sa-sliders
  seat-area
  "Seat Area"
  #t
  (when seat-area
    (send (get-slider 'guides-seat-area) set-value #t)
    (when (and 
           (not (send (get-slider 'guides-back-area) get-value))
           (not (send (get-slider 'guides-arm-area) get-value)))
      (send (get-slider 'guides) set-value #t)
      (send tab-g add-child tab-g-sliders)))
  (when (not seat-area)
    (send (get-slider 'guides-seat-area) set-value #f)
    (when (and 
           (not (send (get-slider 'guides-back-area) get-value))
           (not (send (get-slider 'guides-arm-area) get-value)))
      (send (get-slider 'guides) set-value #f)
      (send tab-g delete-child tab-g-sliders)))
  )
 
(default-slider
  tab-sa-sliders
  seat-area-width
  "Width (mm)"
  457 508
  #t)

(default-slider
  tab-sa-sliders
  seat-area-depth
  "Depth (mm)"
  397 457 
  #t)

(default-slider
  tab-sa-sliders
  seat-area-height
  "Height (mm)"
  406 432
  #t)

(default-slider
  tab-sa-sliders
  seat-area-angle-rake
  "Rake Angle (º)"
  0 5
  #t)

(default-slider
  tab-sa-sliders
  seat-area-radius-front
  "Front Radius (%)"
  0 100
  #t)

(default-slider
  tab-sa-sliders
  seat-area-radius-rear
  "Rear Radius (%)"
  0 100
  #t)

(default-slider
  tab-sa-sliders
  seat-area-Δwidth-taper
  "Taper Width (%)"
  0 100
  #t)

;----------SEAT PANEL----------
(define tab-sp (default-panel tab-seat '()))
(define tab-sp-sliders (default-panel tab-sp '(deleted)))

(default-check-box
  tab-sp tab-sp tab-sp-sliders
  seat-panel
  "Seat Panel"
  #t)

(default-slider
  tab-sp-sliders
  seat-panel-thickness
  "Thickness (mm)"
  1 100
  #t)

;----------SEAT OUTER FRAME----------
(define tab-so (default-panel tab-seat '()))

(new message% [parent tab-so] [label "Seat Outer Frame"])

;----------SEAT FRONT----------


(default-check-box
  tab-so #f #f
  seat-front
  "Seat Front Rail"
  (precedent 'leg-front)
  (subsequent (or (and leg-back seat-front) (and leg-front seat-back) (and seat-front seat-back)) 'seat-long tab-sl tab-sl-sliders)
  (subsequent-slider seat-front 'seat-long-Δwidth-front)
  (subsequent (or seat-front seat-back seat-side) 'seat-outer-sround tab-so-section tab-so-sround-sliders)
  (subsequent (or seat-front seat-back seat-side) 'seat-outer-ssquare tab-so-section tab-so-ssquare-sliders))
    
;----------SEAT BACK----------
(default-check-box
  tab-so #f #f
  seat-back
  "Seat Back Rail"
  (precedent 'leg-back)
  (subsequent (or (and leg-back seat-front) (and leg-front seat-back) (and seat-front seat-back)) 'seat-long tab-sl tab-sl-sliders)
  (subsequent (or (and back-top seat-back) (and back-top back-cross-top) (and back-top back-cross-bottom) (and back-cross-arm seat-back)) 'back-splat tab-bs tab-bs-sliders)
  (subsequent-slider seat-back 'seat-long-Δwidth-rear)
  (subsequent (or seat-front seat-back seat-side) 'seat-outer-sround tab-so-section tab-so-sround-sliders)
  (subsequent (or seat-front seat-back seat-side) 'seat-outer-ssquare tab-so-section tab-so-ssquare-sliders))

;----------SEAT SIDE----------
(define tab-ss (default-panel tab-so '()))
(define tab-ss-sliders (default-panel tab-ss '(deleted)))
  
(default-check-box
  tab-ss tab-ss tab-ss-sliders
  seat-side
  "Seat Side Rail"
  (precedent (or 'leg-back 'leg-front)) 
  (subsequent seat-side 'seat-cross tab-sc tab-sc-sliders)
  (subsequent (or leg-back seat-side) 'back-upright #f #f)
  (subsequent seat-side 'arm-support-side tab-ass tab-ass-sliders)
  (subsequent (or seat-front seat-back seat-side) 'seat-outer-sround tab-so-section tab-so-sround-sliders)
  (subsequent (or seat-front seat-back seat-side) 'seat-outer-ssquare tab-so-section tab-so-ssquare-sliders))

(default-slider
  tab-ss-sliders
  seat-side-depth
  "Depth (mm)"
  397 457
  (or (not (send (get-slider 'leg-back) get-value))
      (not (send (get-slider 'leg-front) get-value))))

;----------SECTION----------
;tabs
(define tab-so-section (default-panel tab-so '()))
(define tab-so-labels (new horizontal-panel% [parent tab-so-section]))
(define tab-so-sround-sliders (default-panel tab-so-section '(deleted)))
(define tab-so-ssquare-sliders (default-panel tab-so-section '(deleted)))
  
;message
(new message% [parent tab-so-labels] [label "Section"])
  
;round
(default-check-box
  tab-so-labels tab-so-section tab-so-sround-sliders
  seat-outer-sround
  "Round"
  (precedent (or 'seat-front 'seat-back 'seat-side))
  (if (and (send (get-slider 'seat-outer-sround) get-value) (send (get-slider 'seat-outer-ssquare) get-value))
      (begin
        (send (get-slider 'seat-outer-ssquare) set-value #f)
        (send tab-so-section delete-child tab-so-ssquare-sliders)) #f))

(default-slider
  tab-so-sround-sliders
  seat-outer-sround-diameter
  "Diameter (mm)"
  1 260
  #t)

;square
(default-check-box
  tab-so-labels tab-so-section tab-so-ssquare-sliders
  seat-outer-ssquare
  "Square"
  (precedent (or 'seat-front 'seat-back 'seat-side))
    (if (and (send (get-slider 'seat-outer-sround) get-value) (send (get-slider 'seat-outer-ssquare) get-value))
      (begin
        (send (get-slider 'seat-outer-sround) set-value #f)
        (send tab-so-section delete-child tab-so-sround-sliders)) #f))

(default-slider
  tab-so-ssquare-sliders
  seat-outer-ssquare-height
  "Height (mm)"
  1 260
  #t)

(default-slider
  tab-so-ssquare-sliders
  seat-outer-ssquare-width
  "Width/Depth (mm)"
  1 260
  #t)

;----------SEAT INNER FRAME----------
(define tab-si (default-panel tab-seat '()))

(new message% [parent tab-si] [label "Seat Inner Frame"])

;----------SEAT CROSS----------
(define tab-sc (default-panel tab-si '()))
(define tab-sc-sliders (default-panel tab-sc '(deleted)))

(default-check-box
  tab-sc tab-sc tab-sc-sliders
  seat-cross
  "Seat Cross Rail"
  (precedent 'seat-side)
  (subsequent (or seat-cross seat-long seat-radial) 'seat-inner-sround tab-si-section tab-si-sround-sliders)
  (subsequent (or seat-cross seat-long seat-radial) 'seat-inner-ssquare tab-si-section tab-si-ssquare-sliders))

(default-slider
  tab-sc-sliders
  seat-cross-Δdepth
  "Depth Spacing (%)"
  1 99
  #t)

;----------SEAT LONG----------
(define tab-sl (default-panel tab-si '()))
(define tab-sl-sliders (default-panel tab-sl '(deleted)))

(default-check-box
  tab-sl tab-sl tab-sl-sliders
  seat-long
  "Seat Long Rail"
  (precedent (or (and 'leg-back 'seat-front) (and 'leg-front 'seat-back) (and 'seat-front 'seat-back)))
  (subsequent (or seat-cross seat-long seat-radial) 'seat-inner-sround tab-si-section tab-si-sround-sliders)
  (subsequent (or seat-cross seat-long seat-radial) 'seat-inner-ssquare tab-si-section tab-si-ssquare-sliders)) 

(default-slider
  tab-sl-sliders
  seat-long-Δwidth-front
  "Width Front Spacing (%)"
  0 100
  (precedent 'seat-front))

(default-slider
  tab-sl-sliders
  seat-long-Δwidth-rear
  "Width Rear Spacing (%)"
  0 100
  (precedent 'seat-back))

;----------SEAT RADIAL----------
(define tab-sr (default-panel tab-si '()))
(define tab-sr-sliders (default-panel tab-sr '(deleted)))

(default-check-box
  tab-sr tab-sr tab-sr-sliders
  seat-radial
  "Seat Radial Rail"
  (precedent (or (and 'leg-front 'leg-back) 'leg-centre))
    (subsequent (or seat-cross seat-long seat-radial) 'seat-inner-sround tab-si-section tab-si-sround-sliders)
    (subsequent (or seat-cross seat-long seat-radial) 'seat-inner-ssquare tab-si-section tab-si-ssquare-sliders))

(default-slider
  tab-sr-sliders
  seat-radial-number
  "Number"
  3 5
  (precedent 'leg-centre))

;----------SECTION----------
;tabs
(define tab-si-section (default-panel tab-si '()))
(define tab-si-labels (new horizontal-panel% [parent tab-si-section]))
(define tab-si-sround-sliders (default-panel tab-si-section '(deleted)))
(define tab-si-ssquare-sliders (default-panel tab-si-section '(deleted)))
  
;message
(new message% [parent tab-si-labels] [label "Section"])

;round
(default-check-box
  tab-si-labels tab-si-section tab-si-sround-sliders
  seat-inner-sround
  "Round"
  (precedent (or 'seat-cross 'seat-long 'seat-radial))
  (if (and (send (get-slider 'seat-inner-sround) get-value) (send (get-slider 'seat-inner-ssquare) get-value))
      (begin
        (send (get-slider 'seat-inner-ssquare) set-value #f)
        (send tab-si-section delete-child tab-si-ssquare-sliders)) #f))

(default-slider
  tab-si-sround-sliders
  seat-inner-sround-diameter
  "Diameter (mm)"
  1 260
  #t)

;square
(default-check-box
  tab-si-labels tab-si-section tab-si-ssquare-sliders
  seat-inner-ssquare
  "Square"
    (precedent (or 'seat-cross 'seat-long 'seat-radial))
    (if (and (send (get-slider 'seat-inner-sround) get-value) (send (get-slider 'seat-inner-ssquare) get-value))
      (begin
        (send (get-slider 'seat-inner-sround) set-value #f)
        (send tab-si-section delete-child tab-si-sround-sliders)) #f))

(default-slider
  tab-si-ssquare-sliders
  seat-inner-ssquare-height
  "Height (mm)"
  1 260
  #t)

(default-slider
  tab-si-ssquare-sliders
  seat-inner-ssquare-width
  "Width/Depth (mm)"
  1 260
  #t)


;---------------TAB BACK---------------
;--------------------------------------

;----------BACK AERA----------
(define tab-ba (default-panel tab-back '()))
(define tab-ba-sliders (default-panel tab-ba '(deleted)))

(default-check-box
  tab-ba tab-ba tab-ba-sliders
  back-area
  "Back Area"
  #t
  (when back-area
    (send (get-slider 'guides-back-area) set-value #t)
    (when (and 
           (not (send (get-slider 'guides-seat-area) get-value))
           (not (send (get-slider 'guides-arm-area) get-value)))
      (send (get-slider 'guides) set-value #t)
      (send tab-g add-child tab-g-sliders)))
  (when (not back-area)
    (send (get-slider 'guides-back-area) set-value #f)
    (when (and 
           (not (send (get-slider 'guides-seat-area) get-value))
           (not (send (get-slider 'guides-arm-area) get-value)))
      (send (get-slider 'guides) set-value #f)
      (send tab-g delete-child tab-g-sliders)))
  )

(default-slider
  tab-ba-sliders
  back-area-height
  "Height (mm)"
  432 610
  #t)

(default-slider
  tab-ba-sliders
  back-area-Δheight
  "Height Spacing (mm)"
  0 152
  #t)

(default-slider
  tab-ba-sliders
  back-area-angle-rake
  "Rake Angle (º)"
  90 105
  #t)

(default-slider
  tab-ba-sliders
  back-area-radius-top
  "Top Radius (%)"
  0 100
  #t)

(default-slider
  tab-ba-sliders
  back-area-radius-bottom
  "Bottom Radius (%)"
  0 100
  #t)

;----------BACK PANEL----------
(define tab-bp (default-panel tab-back '()))
(define tab-bp-sliders (default-panel tab-bp '(deleted)))

(default-check-box
  tab-bp tab-bp tab-bp-sliders
  back-panel
  "Back Panel"
  #t)

(default-slider
  tab-bp-sliders
  back-panel-thickness
  "Thickness (mm)"
  1 100
  #t)

;----------BACK OUTER FRAME----------
(define tab-bo (default-panel tab-back '()))

(new message% [parent tab-bo] [label "Back Outer Frame"])

;----------BACK UPRIGHT----------
(define tab-bu (default-panel tab-bo '()))
(define tab-bu-sliders (default-panel tab-bu '(deleted)))

(default-check-box
  tab-bu tab-bu tab-bu-sliders
  back-upright
  "Back Upright"
  (precedent (or 'leg-back 'seat-side))
  (subsequent back-upright 'back-top #f #f)
  (subsequent back-upright 'back-cross-top tab-bct tab-bct-sliders)
  (subsequent back-upright 'back-cross-bottom tab-bcb tab-bcb-sliders)
  (subsequent back-upright 'back-radial #f #f)
  (subsequent back-upright 'back-outer-sround tab-bo-section tab-bo-sround-sliders)
  (subsequent back-upright 'back-outer-ssquare tab-bo-section tab-bo-ssquare-sliders)
  (send (get-slider 'arm-support-back) enable (not back-upright))) ;when not bu, enable asb

(default-slider
  tab-bu-sliders
  back-upright-height
  "Height (%)"
  0 100
  #t)

(default-slider
  tab-bu-sliders
  back-upright-angle-splay
  "Splay Angle (º)"
  -45 45
  #t)

;----------BACK TOP----------
(default-check-box
  tab-bo #f #f
  back-top
  "Back Top Rail"
  (precedent 'back-upright)
  (subsequent (or (and back-top seat-back) (and back-top back-cross-top) (and back-top back-cross-bottom) (and back-cross-arm seat-back)) 'back-splat tab-bs tab-bs-sliders))

;----------SECTION----------
;tabs
(define tab-bo-section (default-panel tab-bo '()))
(define tab-bo-labels (new horizontal-panel% [parent tab-bo-section]))
(define tab-bo-sround-sliders (default-panel tab-bo-section '(deleted)))
(define tab-bo-ssquare-sliders (default-panel tab-bo-section '(deleted)))
  
;message
(new message% [parent tab-bo-labels] [label "Section"])

;round
(default-check-box
  tab-bo-labels tab-bo-section tab-bo-sround-sliders
  back-outer-sround
  "Round"
  (precedent 'back-upright)
  (if (and (send (get-slider 'back-outer-sround) get-value) (send (get-slider 'back-outer-ssquare) get-value))
      (begin
        (send (get-slider 'back-outer-ssquare) set-value #f)
        (send tab-bo-section delete-child tab-bo-ssquare-sliders)) #f))

(default-slider
  tab-bo-sround-sliders
  back-outer-sround-diameter
  "Diameter (mm)"
  1 260
  #t)

;square
(default-check-box
  tab-bo-labels tab-bo-section tab-bo-ssquare-sliders
  back-outer-ssquare
  "Square"
  (precedent 'back-upright)
    (if (and (send (get-slider 'back-outer-sround) get-value) (send (get-slider 'back-outer-ssquare) get-value))
      (begin
        (send (get-slider 'back-outer-sround) set-value #f)
        (send tab-bo-section delete-child tab-bo-sround-sliders)) #f))

(default-slider
  tab-bo-ssquare-sliders
  back-outer-ssquare-depth
  "Depth (mm)"
  1 260
  #t)

(default-slider
  tab-bo-ssquare-sliders
  back-outer-ssquare-width
  "Width/Height (mm)"
  1 260
  #t)

;----------BACK INNER FRAME----------
(define tab-bi (default-panel tab-back '()))

(new message% [parent tab-bi] [label "Back Inner Frame"])

;----------BACK CROSS TOP----------
(define tab-bct (default-panel tab-bi '()))
(define tab-bct-sliders (default-panel tab-bct '(deleted)))

(default-check-box
  tab-bct tab-bct tab-bct-sliders
  back-cross-top
  "Back Cross Top Rail"
  (precedent 'back-upright)
  (subsequent (or (and back-top seat-back) (and back-top back-cross-top) (and back-top back-cross-bottom) (and back-cross-arm seat-back)) 'back-splat tab-bs tab-bs-sliders)
  (subsequent (or back-cross-top back-cross-bottom back-splat back-radial) 'back-inner-sround tab-bi-section tab-bi-sround-sliders)
  (subsequent (or back-cross-top back-cross-bottom back-splat back-radial) 'back-inner-ssquare tab-bi-section tab-bi-ssquare-sliders))

(default-slider
  tab-bct-sliders
  back-cross-top-height
  "Height (%)"
  50 99
  #t)

;----------BACK CROSS BOTTOM----------
(define tab-bcb (default-panel tab-bi '()))
(define tab-bcb-sliders (default-panel tab-bcb '(deleted)))

(default-check-box
  tab-bcb tab-bcb tab-bcb-sliders
  back-cross-bottom
  "Back Cross Bottom Rail"
  (precedent 'back-upright)
  (subsequent (or (and back-top seat-back) (and back-top back-cross-top) (and back-top back-cross-bottom) (and back-cross-arm seat-back)) 'back-splat tab-bs tab-bs-sliders)
  (subsequent (or back-cross-top back-cross-bottom back-splat back-radial) 'back-inner-sround tab-bi-section tab-bi-sround-sliders)
  (subsequent (or back-cross-top back-cross-bottom back-splat back-radial) 'back-inner-ssquare tab-bi-section tab-bi-ssquare-sliders))

(default-slider
  tab-bcb-sliders
  back-cross-bottom-height
  "Height (%)"
  1 49
  #t)

;----------BACK CROSS ARM----------
(define tab-bca (default-panel tab-bi '()))

;back-cross-arm
;see below armrest

;----------BACK SPLAT----------
(define tab-bs (default-panel tab-bi '()))
(define tab-bs-sliders (default-panel tab-bs '(deleted)))

;back-splat
;see below back-cross-arm

;----------BACK RADIAL----------
(default-check-box
  tab-bi #f #f
  back-radial
  "Back Radial"
  (precedent 'back-upright)
  (subsequent (or back-cross-top back-cross-bottom back-splat back-radial) 'back-inner-sround tab-bi-section tab-bi-sround-sliders)
  (subsequent (or back-cross-top back-cross-bottom back-splat back-radial) 'back-inner-ssquare tab-bi-section tab-bi-ssquare-sliders))

;----------SECTION----------
;tabs
(define tab-bi-section (default-panel tab-bi '()))
(define tab-bi-labels (new horizontal-panel% [parent tab-bi-section]))
(define tab-bi-sround-sliders (default-panel tab-bi-section '(deleted)))
(define tab-bi-ssquare-sliders (default-panel tab-bi-section '(deleted)))
  
;message
(new message% [parent tab-bi-labels] [label "Section"])

;round
(default-check-box
  tab-bi-labels tab-bi-section tab-bi-sround-sliders
  back-inner-sround
  "Round"
  (precedent (or 'back-cross-top 'back-cross-bottom 'back-splat 'back-radial))
  (if (and (send (get-slider 'back-inner-sround) get-value) (send (get-slider 'back-inner-ssquare) get-value))
      (begin
        (send (get-slider 'back-inner-ssquare) set-value #f)
        (send tab-bi-section delete-child tab-bi-ssquare-sliders)) #f))

(default-slider
  tab-bi-sround-sliders
  back-inner-sround-diameter
  "Diameter (mm)"
  1 260
  #t)

;square
(default-check-box
  tab-bi-labels tab-bi-section tab-bi-ssquare-sliders
  back-inner-ssquare
  "Square"
  (precedent (or 'back-cross-top 'back-cross-bottom 'back-splat 'back-radial))
    (if (and (send (get-slider 'back-inner-sround) get-value) (send (get-slider 'back-inner-ssquare) get-value))
      (begin
        (send (get-slider 'back-inner-sround) set-value #f)
        (send tab-bi-section delete-child tab-bi-sround-sliders)) #f))

(default-slider
  tab-bi-ssquare-sliders
  back-inner-ssquare-depth
  "Depth (mm)"
  1 260
  #t)

(default-slider
  tab-bi-ssquare-sliders
  back-inner-ssquare-width
  "Width/Height (mm)"
  1 260
  #t)


;---------------TAB STRETCHERS---------------
;--------------------------------------------

;----------LEG STRETCHER AERA----------
(define tab-lsa (default-panel tab-stretchers '()))
(define tab-lsa-sliders (default-panel tab-lsa '(deleted)))

(default-check-box
  tab-lsa tab-lsa tab-lsa-sliders
  leg-stretcher-area
  "Stretchers Area"
  #t)

(default-slider
  tab-lsa-sliders
  leg-stretcher-area-height
  "Height (%)"
  1 99
  #t)

(default-slider
  tab-lsa-sliders
  leg-stretcher-area-angle-rake
  "Rake Angle (%)"
  -100 100
  #t)

(default-slider
  tab-lsa-sliders
  leg-stretcher-area-radius-front
  "Front Radius (%)"
  0 100
  #t)

(default-slider
  tab-lsa-sliders
  leg-stretcher-area-radius-rear
  "Rear Radius (%)"
  0 100
  #t)

;----------LEG STRETCHER OUTER FRAME----------
(define tab-lso (default-panel tab-stretchers '()))

(new message% [parent tab-lso] [label "Stretchers Outer Frame"])

;----------LEG STRETCHER FRONT----------
(define tab-lsf (default-panel tab-lso '()))
(define tab-lsf-sliders (default-panel tab-lsf '(deleted)))

(default-check-box
  tab-lsf tab-lsf tab-lsf-sliders
  leg-stretcher-front
  "Front Stretcher"
  (precedent 'leg-front)
  (subsequent (or leg-stretcher-front leg-stretcher-back leg-stretcher-side) 'leg-stretcher-outer-sround tab-lso-section tab-lso-sround-sliders)
  (subsequent (or leg-stretcher-front leg-stretcher-back leg-stretcher-side) 'leg-stretcher-outer-ssquare tab-lso-section tab-lso-ssquare-sliders))

(default-slider
  tab-lsf-sliders
  leg-stretcher-front-height
  "Height (%)"
  1 99
  #t)

;----------LEG STRETCHER BACK----------
(define tab-lsb (default-panel tab-lso '()))
(define tab-lsb-sliders (default-panel tab-lsb '(deleted)))

(default-check-box
  tab-lsb tab-lsb tab-lsb-sliders
  leg-stretcher-back
  "Back Stretcher"
  (precedent 'leg-back)
  (subsequent (or leg-stretcher-front leg-stretcher-back leg-stretcher-side) 'leg-stretcher-outer-sround tab-lso-section tab-lso-sround-sliders)
  (subsequent (or leg-stretcher-front leg-stretcher-back leg-stretcher-side) 'leg-stretcher-outer-ssquare tab-lso-section tab-lso-ssquare-sliders))

(default-slider
  tab-lsb-sliders
  leg-stretcher-back-height
  "Height (%)"
  1 99
  #t)

;----------LEG STRETCHER SIDE----------
(define tab-lss (default-panel tab-lso '()))
(define tab-lss-sliders (default-panel tab-lss '(deleted)))

(default-check-box
  tab-lss tab-lss tab-lss-sliders
  leg-stretcher-side
  "Side Stretcher"
  (precedent (and 'leg-back 'leg-front))
  (subsequent leg-stretcher-side 'leg-stretcher-cross tab-lsc tab-lsc-sliders)
  (subsequent (or leg-stretcher-front leg-stretcher-back leg-stretcher-side) 'leg-stretcher-outer-sround tab-lso-section tab-lso-sround-sliders)
  (subsequent (or leg-stretcher-front leg-stretcher-back leg-stretcher-side) 'leg-stretcher-outer-ssquare tab-lso-section tab-lso-ssquare-sliders))

(default-slider
  tab-lss-sliders
  leg-stretcher-side-height
  "Height (%)"
  1 99
  #t)

;----------SECTION----------
;tabs
(define tab-lso-section (default-panel tab-lso '()))
(define tab-lso-labels (new horizontal-panel% [parent tab-lso-section]))
(define tab-lso-sround-sliders (default-panel tab-lso-section '(deleted)))
(define tab-lso-ssquare-sliders (default-panel tab-lso-section '(deleted)))
  
;message
(new message% [parent tab-lso-labels] [label "Section"])

;round
(default-check-box
  tab-lso-labels tab-lso-section tab-lso-sround-sliders
  leg-stretcher-outer-sround
  "Round"
  (precedent (or 'leg-stretcher-front 'leg-stretcher-back 'leg-stretcher-side))
  (if (and (send (get-slider 'leg-stretcher-outer-sround) get-value) (send (get-slider 'leg-stretcher-outer-ssquare) get-value))
      (begin
        (send (get-slider 'leg-stretcher-outer-ssquare) set-value #f)
        (send tab-lso-section delete-child tab-lso-ssquare-sliders)) #f))

(default-slider
  tab-lso-sround-sliders
  leg-stretcher-outer-sround-diameter
  "Diameter (mm)"
  1 260
  #t)

;square
(default-check-box
  tab-lso-labels tab-lso-section tab-lso-ssquare-sliders
  leg-stretcher-outer-ssquare
  "Square"
  (precedent (or 'leg-stretcher-front 'leg-stretcher-back 'leg-stretcher-side))
    (if (and (send (get-slider 'leg-stretcher-outer-sround) get-value) (send (get-slider 'leg-stretcher-outer-ssquare) get-value))
      (begin
        (send (get-slider 'leg-stretcher-outer-sround) set-value #f)
        (send tab-lso-section delete-child tab-lso-sround-sliders)) #f))

(default-slider
  tab-lso-ssquare-sliders
  leg-stretcher-outer-ssquare-height
  "Height (mm)"
  1 260
  #t)

(default-slider
  tab-lso-ssquare-sliders
  leg-stretcher-outer-ssquare-width
  "Width/Depth (mm)"
  1 260
  #t)

;----------LEG STRETCHER INNER FRAME----------
(define tab-lsi (default-panel tab-stretchers '()))

(new message% [parent tab-lsi] [label "Stretchers Inner Frame"])

;----------LEG STRETCHER CROSS----------
(define tab-lsc (default-panel tab-lsi '()))
(define tab-lsc-sliders (default-panel tab-lsc '(deleted)))

(default-check-box
  tab-lsc tab-lsc tab-lsc-sliders
  leg-stretcher-cross
  "Cross Stretcher"
  (precedent 'leg-stretcher-side)
  (subsequent (or leg-stretcher-cross leg-stretcher-radial) 'leg-stretcher-inner-sround tab-lsi-section tab-lsi-sround-sliders)
  (subsequent (or leg-stretcher-cross leg-stretcher-radial) 'leg-stretcher-inner-ssquare tab-lsi-section tab-lsi-ssquare-sliders))

(default-slider
  tab-lsc-sliders
  leg-stretcher-cross-Δdepth
  "Depth Spacing (%)"
  1 99
  #t)

;----------LEG STRETCHER RADIAL----------
(define tab-lsr (default-panel tab-lsi '()))
(define tab-lsr-sliders (default-panel tab-lsr '(deleted)))

(default-check-box
  tab-lsr tab-lsr tab-lsr-sliders
  leg-stretcher-radial
  "Radial Stretcher"
  (precedent (and 'leg-front 'leg-back))
  (subsequent (or leg-stretcher-cross leg-stretcher-radial) 'leg-stretcher-inner-sround tab-lsi-section tab-lsi-sround-sliders)
  (subsequent (or leg-stretcher-cross leg-stretcher-radial) 'leg-stretcher-inner-ssquare tab-lsi-section tab-lsi-ssquare-sliders))

(default-slider
  tab-lsr-sliders
  leg-stretcher-radial-height
  "Height (%)"
  1 99
  #t)

;----------SECTION----------
;tabs
(define tab-lsi-section (default-panel tab-lsi '()))
(define tab-lsi-labels (new horizontal-panel% [parent tab-lsi-section]))
(define tab-lsi-sround-sliders (default-panel tab-lsi-section '(deleted)))
(define tab-lsi-ssquare-sliders (default-panel tab-lsi-section '(deleted)))
  
;message
(new message% [parent tab-lsi-labels] [label "Section"])

;round
(default-check-box
  tab-lsi-labels tab-lsi-section tab-lsi-sround-sliders
  leg-stretcher-inner-sround
  "Round"
  (precedent (or 'leg-stretcher-cross 'leg-stretcher-radial))
  (if (and (send (get-slider 'leg-stretcher-inner-sround) get-value) (send (get-slider 'leg-stretcher-inner-ssquare) get-value))
      (begin
        (send (get-slider 'leg-stretcher-inner-ssquare) set-value #f)
        (send tab-lsi-section delete-child tab-lsi-ssquare-sliders)) #f))

(default-slider
  tab-lsi-sround-sliders
  leg-stretcher-inner-sround-diameter
  "Diameter (mm)"
  1 260
  #t)

;square
(default-check-box
  tab-lsi-labels tab-lsi-section tab-lsi-ssquare-sliders
  leg-stretcher-inner-ssquare
  "Square"
   (precedent (or 'leg-stretcher-cross 'leg-stretcher-radial))
    (if (and (send (get-slider 'leg-stretcher-inner-sround) get-value) (send (get-slider 'leg-stretcher-inner-ssquare) get-value))
      (begin
        (send (get-slider 'leg-stretcher-inner-sround) set-value #f)
        (send tab-lsi-section delete-child tab-lsi-sround-sliders)) #f))

(default-slider
  tab-lsi-ssquare-sliders
  leg-stretcher-inner-ssquare-height
  "Height (mm)"
  1 260
  #t)

(default-slider
  tab-lsi-ssquare-sliders
  leg-stretcher-inner-ssquare-width
  "Width/Depth (mm)"
  1 260
  #t)


;---------------TAB BASE---------------
;--------------------------------------

;----------LEG BASE AERA----------
(define tab-lba (default-panel tab-base '()))
(define tab-lba-sliders (default-panel tab-lba '(deleted)))

(default-check-box
  tab-lba tab-lba tab-lba-sliders
  leg-base-area
  "Base Area"
  #t)
  
  (default-slider
    tab-lba-sliders
    leg-base-area-width
    "Width (mm)"
    457 508
    #t)
  
  (default-slider
    tab-lba-sliders
    leg-base-area-depth
    "Depth (mm)"
    397 457 
    #t)

(default-slider
  tab-lba-sliders
  leg-base-area-radius-front
  "Front Radius (%)"
  0 100
  #t)

(default-slider
  tab-lba-sliders
  leg-base-area-radius-rear
  "Rear Radius (%)"
  0 100
  #t)

;----------LEG BASE PANEL----------
(define tab-lbp (default-panel tab-base '()))
(define tab-lbp-sliders (default-panel tab-lbp '(deleted)))

(default-check-box
  tab-lbp tab-lbp tab-lbp-sliders
  leg-base-panel
  "Base Panel"
  #t)

(default-slider
  tab-lbp-sliders
  leg-base-panel-thickness
  "Thickness (mm)"
  1 100
  #t)

;----------LEG BASE OUTER FRAME----------
(define tab-lbo (default-panel tab-base '()))

(new message% [parent tab-lbo] [label "Base Outer Frame"])

;----------LEG BASE FRONT----------
(default-check-box
  tab-lbo #f #f
  leg-base-front
  "Base Front Rail"
  (precedent 'leg-front)
  (subsequent (or leg-base-front leg-base-back leg-base-side) 'leg-base-outer-sround tab-lbo-section tab-lbo-sround-sliders)
  (subsequent (or leg-base-front leg-base-back leg-base-side) 'leg-base-outer-ssquare tab-lbo-section tab-lbo-ssquare-sliders))

;----------LEG BASE BACK----------
(default-check-box
  tab-lbo #f #f
  leg-base-back
  "Base Back Rail"
  (precedent 'leg-back)
  (subsequent (or leg-base-front leg-base-back leg-base-side) 'leg-base-outer-sround tab-lbo-section tab-lbo-sround-sliders)
  (subsequent (or leg-base-front leg-base-back leg-base-side) 'leg-base-outer-ssquare tab-lbo-section tab-lbo-ssquare-sliders))

;----------LEG BASE SIDE----------
(define tab-lbs (default-panel tab-lbo '()))
(define tab-lbs-sliders (default-panel tab-lbs '(deleted)))
  
(default-check-box
  tab-lbs tab-lbs tab-lbs-sliders
  leg-base-side
  "Base Side Rail"
  (precedent (or 'leg-front 'leg-back))
  (subsequent leg-base-side 'leg-base-cross tab-lbc tab-lbc-sliders)
  (subsequent (or leg-base-front leg-base-back leg-base-side) 'leg-base-outer-sround tab-lbo-section tab-lbo-sround-sliders)
  (subsequent (or leg-base-front leg-base-back leg-base-side) 'leg-base-outer-ssquare tab-lbo-section  tab-lbo-ssquare-sliders))

(default-slider
  tab-lbs-sliders
  leg-base-side-depth
  "Depth (mm)"
  397 500
  (or (not (send (get-slider 'leg-back) get-value))
      (not (send (get-slider 'leg-front) get-value))))

;----------SECTION----------
;tabs
(define tab-lbo-section (default-panel tab-lbo '()))
(define tab-lbo-labels (new horizontal-panel% [parent tab-lbo-section]))
(define tab-lbo-sround-sliders (default-panel tab-lbo-section '(deleted)))
(define tab-lbo-ssquare-sliders (default-panel tab-lbo-section '(deleted)))
  
;message
(new message% [parent tab-lbo-labels] [label "Section"])

;round
(default-check-box
  tab-lbo-labels tab-lbo-section tab-lbo-sround-sliders
  leg-base-outer-sround
  "Round"
  (precedent (or 'leg-base-front 'leg-base-back 'leg-base-side))
  (if (and (send (get-slider 'leg-base-outer-sround) get-value) (send (get-slider 'leg-base-outer-ssquare) get-value))
      (begin
        (send (get-slider 'leg-base-outer-ssquare) set-value #f)
        (send tab-lbo-section delete-child tab-lbo-ssquare-sliders)) #f))

(default-slider
  tab-lbo-sround-sliders
  leg-base-outer-sround-diameter
  "Diameter (mm)"
  1 260
  #t)

;square
(default-check-box
  tab-lbo-labels tab-lbo-section tab-lbo-ssquare-sliders
  leg-base-outer-ssquare
  "Square"
  (precedent (or 'leg-base-front 'leg-base-back 'leg-base-side))
    (if (and (send (get-slider 'leg-base-outer-sround) get-value) (send (get-slider 'leg-base-outer-ssquare) get-value))
      (begin
        (send (get-slider 'leg-base-outer-sround) set-value #f)
        (send tab-lbo-section delete-child tab-lbo-sround-sliders)) #f))

(default-slider
  tab-lbo-ssquare-sliders
  leg-base-outer-ssquare-height
  "Height (mm)"
  1 260
  #t)

(default-slider
  tab-lbo-ssquare-sliders
  leg-base-outer-ssquare-width
  "Width/Depth (mm)"
  1 260
  #t)

;----------LEG BASE INNER FRAME----------
(define tab-lbi (default-panel tab-base '()))

(new message% [parent tab-lbi] [label "Base Inner Frame"])

;----------LEG BASE CROSS----------
(define tab-lbc (default-panel tab-lbi '()))
(define tab-lbc-sliders (default-panel tab-lbc '(deleted)))

(default-check-box
  tab-lbc tab-lbc tab-lbc-sliders
  leg-base-cross
  "Base Cross Rail"
  (precedent 'leg-base-side)
  (subsequent (or leg-base-cross leg-base-radial) 'leg-base-inner-sround tab-lbi-section tab-lbi-sround-sliders)
  (subsequent (or leg-base-cross leg-base-radial) 'leg-base-inner-ssquare tab-lbi-section tab-lbi-sround-sliders))

(default-slider
  tab-lbc-sliders
  leg-base-cross-Δdepth
  "Depth Spacing (%)"
  1 99
  #t)

;----------LEG BASE RADIAL----------
(define tab-lbr (default-panel tab-lbi '()))
(define tab-lbr-sliders (default-panel tab-lbr '(deleted)))

(default-check-box
  tab-lbr tab-lbr tab-lbr-sliders
  leg-base-radial
  "Base Radial Rail"
  (precedent (or (and 'leg-front 'leg-back) leg-centre))
  (subsequent (or leg-base-cross leg-base-radial) 'leg-base-inner-sround tab-lbi-section tab-lbi-sround-sliders)
  (subsequent (or leg-base-cross leg-base-radial) 'leg-base-inner-ssquare tab-lbi-section tab-lbi-ssquare-sliders))

(default-slider
  tab-lbr-sliders
  leg-base-radial-number
  "Number"
  3 5
  (precedent 'leg-centre))

;----------SECTION----------
;tabs
(define tab-lbi-section (default-panel tab-lbi '()))
(define tab-lbi-labels (new horizontal-panel% [parent tab-lbi-section]))
(define tab-lbi-sround-sliders (default-panel tab-lbi-section '(deleted)))
(define tab-lbi-ssquare-sliders (default-panel tab-lbi-section '(deleted)))
  
;message
(new message% [parent tab-lbi-labels] [label "Section"])

;round
(default-check-box
  tab-lbi-labels tab-lbi-section tab-lbi-sround-sliders
  leg-base-inner-sround
  "Round"
  (precedent (or 'leg-base-cross 'leg-base-radial))
  (if (and (send (get-slider 'leg-base-inner-sround) get-value) (send (get-slider 'leg-base-inner-ssquare) get-value))
      (begin
        (send (get-slider 'leg-base-inner-ssquare) set-value #f)
        (send tab-lbi-section delete-child tab-lbi-ssquare-sliders)) #f))

(default-slider
  tab-lbi-sround-sliders
  leg-base-inner-sround-diameter
  "Diameter (mm)"
  1 260
  #t)

;square
(default-check-box
  tab-lbi-labels tab-lbi-section tab-lbi-ssquare-sliders
  leg-base-inner-ssquare
  "Square"
  (precedent (or 'leg-base-cross 'leg-base-radial))
    (if (and (send (get-slider 'leg-base-inner-sround) get-value) (send (get-slider 'leg-base-inner-ssquare) get-value))
      (begin
        (send (get-slider 'leg-base-inner-sround) set-value #f)
        (send tab-lbi-section delete-child tab-lbi-sround-sliders)) #f))

(default-slider
  tab-lbi-ssquare-sliders
  leg-base-inner-ssquare-height
  "Height (mm)"
  1 260
  #t)

(default-slider
  tab-lbi-ssquare-sliders
  leg-base-inner-ssquare-width
  "Width/Depth (mm)"
  1 260
  #t)


;---------------TAB ARMS---------------
;--------------------------------------

;----------ARM AERA----------
(define tab-aa (default-panel tab-arms '()))
(define tab-aa-sliders (default-panel tab-aa '(deleted)))

(default-check-box
  tab-aa tab-aa tab-aa-sliders
  arm-area
  "Arms Area"
  #t
  (when arm-area
    (send (get-slider 'guides-arm-area) set-value #t)
    (when (and 
           (not (send (get-slider 'guides-seat-area) get-value))
           (not (send (get-slider 'guides-back-area) get-value)))
      (send (get-slider 'guides) set-value #t)
      (send tab-g add-child tab-g-sliders)))
  (when (not arm-area)
    (send (get-slider 'guides-arm-area) set-value #f)
    (when (and 
           (not (send (get-slider 'guides-seat-area) get-value))
           (not (send (get-slider 'guides-back-area) get-value)))
      (send (get-slider 'guides) set-value #f)
      (send tab-g delete-child tab-g-sliders)))
  )

(default-slider
  tab-aa-sliders
  arm-area-height
  "Height (mm)"
  203 254
  #t)

(default-slider
  tab-aa-sliders
  arm-area-angle-splay
  "Splay Angle (º)"
  -45 45
  #t)

(default-slider
  tab-aa-sliders
  arm-area-radius-front
  "Front Radius (%)"
  0 100
  #t)

(default-slider
  tab-aa-sliders
  arm-area-radius-rear
  "Rear Radius (%)"
  0 100
  #t)

;----------ARM SUPPORT FRONT----------
(define tab-asf (default-panel tab-arms '()))
(define tab-asf-sliders (default-panel tab-asf '(deleted)))

(default-check-box
  tab-asf tab-asf tab-asf-sliders
  arm-support-front
  "Arm Front Support"
  (precedent 'leg-front)
  (subsequent (or arm-support-front arm-support-back arm-support-side) 'armrest tab-ar tab-ar-sliders))

(default-slider
  tab-asf-sliders
  arm-support-front-angle-rake
  "Rake Angle (º)"
  -45 45
  #t)

;----------ARM SUPPORT BACK----------
(define tab-asb (default-panel tab-arms '()))
(define tab-asb-sliders (default-panel tab-asb '(deleted)))

(default-check-box
  tab-asb tab-asb tab-asb-sliders
  arm-support-back
  "Arm Back Support"
  (precedent 'leg-back)
  (subsequent (or arm-support-front arm-support-back arm-support-side) 'armrest tab-ar tab-ar-sliders)
  (send (get-slider 'back-upright) enable (not arm-support-back))) ;when not bu, enable asb

(default-slider
  tab-asb-sliders
  arm-support-back-angle-rake
  "Rake Angle (º)"
  -45 45
  #t)

;----------ARM SUPPORT SIDE----------
(define tab-ass (default-panel tab-arms '()))
(define tab-ass-sliders (default-panel tab-ass '(deleted)))

(default-check-box
  tab-ass tab-ass tab-ass-sliders
  arm-support-side
  "Arm Side Support"
  (precedent 'seat-side)
  (subsequent (or arm-support-front arm-support-back arm-support-side) 'armrest tab-ar tab-ar-sliders))

(default-slider
  tab-ass-sliders
  arm-support-side-Δdepth
  "Depth Spacing (%)"
  1 99
  #t)
;if spacing=0 > arm-support-back, spacing=100 > arm-support-front

;SECTION
;tabs
(define tab-ass-section (default-panel tab-ass-sliders '()))
(define tab-ass-labels (new horizontal-panel% [parent tab-ass-section]))
(define tab-ass-sround-sliders (default-panel tab-ass-section '(deleted)))
(define tab-ass-ssquare-sliders (default-panel tab-ass-section '(deleted)))
  
;message
(new message% [parent tab-ass-labels] [label "Section"])

;round
(default-check-box
  tab-ass-labels tab-ass-section tab-ass-sround-sliders
  arm-support-side-sround
  "Round"
  #t
  (if (and (send (get-slider 'arm-support-side-sround) get-value) (send (get-slider 'arm-support-side-ssquare) get-value))
      (begin
        (send (get-slider 'arm-support-side-ssquare) set-value #f)
        (send tab-ass-section delete-child tab-ass-ssquare-sliders)) #f))

(default-slider
  tab-ass-sround-sliders
  arm-support-side-sround-diameter
  "Diameter (mm)"
  1 260
  #t)

;square
(default-check-box
  tab-ass-labels tab-ass-section tab-ass-ssquare-sliders
  arm-support-side-ssquare
  "Square"
  #t
    (if (and (send (get-slider 'arm-support-side-sround) get-value) (send (get-slider 'arm-support-side-ssquare) get-value))
      (begin
        (send (get-slider 'arm-support-side-sround) set-value #f)
        (send tab-ass-section delete-child tab-ass-sround-sliders)) #f))

(default-slider
  tab-ass-ssquare-sliders
  arm-support-side-ssquare-width
  "Width (mm)"
  1 260
  #t)

(default-slider
  tab-ass-ssquare-sliders
  arm-support-side-ssquare-depth
  "Depth (mm)"
  1 260
  #t)

;----------ARMREST----------
(define tab-ar (default-panel tab-arms '()))
(define tab-ar-sliders (default-panel tab-ar '(deleted)))

(default-check-box
  tab-ar tab-ar tab-ar-sliders
  armrest
  "Armrest"
  (precedent (or 'arm-support-front 'arm-support-back 'arm-support-side))
  (subsequent armrest 'back-cross-arm #f #f))

(default-slider
  tab-ar-sliders
  armrest-depth
  "Depth (mm)"
  254 305
  #t)

(default-slider
  tab-ar-sliders
  armrest-Δdepth-rear
  "Depth Rear Spacing (mm)"
  0 100
  #t)

;SECTION
;tabs
(define tab-ar-section (default-panel tab-ar-sliders '()))
(define tab-ar-labels (new horizontal-panel% [parent tab-ar-section]))
(define tab-ar-sround-sliders (default-panel tab-ar-section '(deleted)))
(define tab-ar-ssquare-sliders (default-panel tab-ar-section '(deleted)))
  
;message
(new message% [parent tab-ar-labels] [label "Section"])

;round
(default-check-box
  tab-ar-labels tab-ar-section tab-ar-sround-sliders
  armrest-sround
  "Round"
  #t
  (if (and (send (get-slider 'armrest-sround) get-value) (send (get-slider 'armrest-ssquare) get-value))
      (begin
        (send (get-slider 'armrest-ssquare) set-value #f)
        (send tab-ar-section delete-child tab-ar-ssquare-sliders)) #f))

(default-slider
  tab-ar-sround-sliders
  armrest-sround-diameter
  "Diameter (mm)"
  1 260
  #t)

;square
(default-check-box
  tab-ar-labels tab-ar-section tab-ar-ssquare-sliders
  armrest-ssquare
  "Square"
  #t
    (if (and (send (get-slider 'armrest-sround) get-value) (send (get-slider 'armrest-ssquare) get-value))
      (begin
        (send (get-slider 'armrest-sround) set-value #f)
        (send tab-ar-section delete-child tab-ar-sround-sliders)) #f))

(default-slider
  tab-ar-ssquare-sliders
  armrest-ssquare-width
  "Width (mm)"
  1 260
  #t)

(default-slider
  tab-ar-ssquare-sliders
  armrest-ssquare-height
  "Height (mm)"
  1 260
  #t)


;---------------OUT OF ORDER-----------
;--------------------------------------

;----------BACK CROSS ARM----------
(default-check-box
  tab-bca #f #f
  back-cross-arm
  "Back Cross Arm Rail"
  (precedent 'armrest)
  (subsequent (or (and back-top seat-back) (and back-top back-cross-top) (and back-top back-cross-bottom) (and back-cross-arm seat-back)) 'back-splat tab-bs tab-bs-sliders))

;----------BACK SPLAT----------
(default-check-box
  tab-bs tab-bs tab-bs-sliders
  back-splat
  "Back Splat"
  (precedent (or (and 'back-top 'seat-back) (and 'back-top 'back-cross-top) (and 'back-top 'back-cross-bottom) (and 'back-cross-arm 'seat-back)))
  (subsequent (or back-cross-top back-cross-bottom back-splat back-radial) 'back-inner-sround tab-bi-section tab-bi-sround-sliders)
  (subsequent (or back-cross-top back-cross-bottom back-splat back-radial) 'back-inner-ssquare tab-bi-section tab-bi-sround-sliders))

(default-slider
  tab-bs-sliders
  back-splat-Δwidth-top
  "Width Top Spacing (%)"
  0 100
  #t)

(default-slider
  tab-bs-sliders
  back-splat-Δwidth-bottom
  "Width Bottom Spacing (%)"
  0 100
  #t)


;---------------SHOW FRAME---------------
;----------------------------------------
(send frame show #t)






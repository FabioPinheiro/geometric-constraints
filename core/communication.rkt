#lang racket
;(require racket/tcp racket/gui/base)
;(require parser-tools/lex)
(provide maxima-mathematica-expression-counter ;maxima-mathematica-expression-counter
         maxima-eval-mathematica ;(maxima-eval-mathematica str #:display [display #f]) ;#:display 'fromLisp
         maxima-eval-lisp ;(maxima-eval-lisp lst #:exactValues [exactValues #t])
         maxima-read-lisp ;(maxima-read-lisp #:parameter [p "%"])
         maxima-lisp->mathematica ;(maxima-lisp->mathematica lst)
         ;maxima-parse-lispSolutions ;(maxima-parse-lispSolutions lst)
         )
(provide start-maxima
         current-maxima-port
         current-maxima-path
         #;maxima-configuration
         maxima-listOfConfigurations
         socket-out ;FIXME DELETE
         socket-in ;FIXME DELETE
         socket-err ;FIXME DELETE
         )
(define current-maxima-port (make-parameter 'none))
(define current-maxima-path (make-parameter 'none))
(define socket-out (make-parameter #f)) ; output socke for sending 
(define socket-in  (make-parameter #f)) ; input socke for receiving
(define socket-err  (make-parameter #f)) ; error socke for receiving
(define maxima-listOfConfigurations
  (make-parameter
   (list "display2d:false"
         ;https://www.ma.utexas.edu/maxima/maxima_20.html
         ;Variable: LINSOLVEWARN default: [TRUE] - if FALSE will cause the message "Dependent equations eliminated" to be suppressed.
         "ratprint:false"; to not print rat: replaced -2.25 by -9/4 = -2.25
         "linsolvewarn:false"
         "linel:1000"
         "realonly:true"
         "radexpand:all" ; With the use of "radcan" (see warpToMaxima) converte sqrt(15) -> sqrt(3)*sqrt(5)
         )))

(define (start-maxima #:path [path (find-executable-path
                                    (case (system-type 'os)
                                      [(unix) "maxima"]
                                      [(windows) "maxima.bat"]
                                      [else (begin
                                              (displayln "start-maxima: no Operating System found")
                                              "maxima")]))]
                      #:port [port 8080 #;35868]) ;32768-65535
  (current-maxima-path path)
  (current-maxima-port port)
  (start-maxima-execute)
  (send/recive-maxima null 'starting-maxima)
  (maxima-configuration))

(define (start-maxima-execute)
  (let (#;[listener (tcp-listen (current-maxima-port) 3 #t)])
    (match-let
        ([(list pin pout pid perr status)
          (process* (current-maxima-path) "--very-quiet")
          #;(process* (current-maxima-path) "--very-quiet" "-s" (format "~a" (current-maxima-port)))])
      ;(displayln (format "start-maxima:: status:~a; pin:~a; pout:~a; pid:~a; perr:~a;" (status 'status) pin pout pid perr))
      ;(status 'kill);close-input-port;close-output-port
      #;(displayln  (tcp-accept-ready? listener))#;(sleep 3)#;(displayln  (tcp-accept-ready? listener))
      (socket-out pout)(socket-err perr)(socket-in pin)
      #;(let-values ([(lin lout) (tcp-accept listener)])(socket-in lin))
      ;(receive-welcome-message)
      'maxima-started)))

(define (maxima-configuration)
  (for/list ([i (maxima-listOfConfigurations)])
    #;displayln (format "maxima-configuration: ~a -> ~a;" i (send/recive-maxima-lispMathematica i)))
  (void))

#;(define (receive-welcome-message); Due to the flag --very-quiet the welcome is a single line containg the pid.
       (displayln (string-join (list "Maxima inited:" (recive-line-maxima)))))


;;; Communication
(provide send/recive-maxima
         send/recive-maxima-mathematica
         send/norecive-maxima-mathematica
         send/recive-maxima-lisp
         send/recive-maxima-lispMathematica
         send/recive-maxima-display);FIXME DELETE

;maxima-mathematica-expression-counter
(define maxima-mathematica-expression-counter 0)
(define (maxima-mathematica-expression-counter-add1)
  (set! maxima-mathematica-expression-counter
        (add1 maxima-mathematica-expression-counter)))

(define (displayDebug srt) (void)) ;FIXME REVOME 
(define (send/recive-maxima str [mode '\;])
  (sync (socket-out))
  (displayDebug (format ">>>> send-maxima (mode ~a)    ~a <<<<<~n" mode str))
  (cond
    [(equal? mode '\;) (begin (maxima-mathematica-expression-counter-add1)
                              (display (format "~a;~n:lisp $linenum~n" str)  (socket-out)))]
    [(equal? mode '$) (begin (maxima-mathematica-expression-counter-add1)
                             (display (format "~a$~n:lisp $%~n:lisp $linenum~n" str)  (socket-out)))]
    [(equal? mode 'lisp) (display (format ":lisp ~a~n:lisp $linenum~n" str)  (socket-out))]
    [(equal? mode 'lisp#$$) (display (format ":lisp #$~a$~n:lisp $linenum~n" str)  (socket-out))]
    [(equal? mode 'display) (display (format ":lisp (maxima-display '~a)~n:lisp $linenum~n" str)  (socket-out))] ;HOTFIX (cons '(MLIST SIMP) str)
    [(equal? mode 'starting-maxima) (display (format ":lisp $linenum~n")  (socket-out))]
    [else (error 'send-maxima "Unknow mode")])
  (flush-output (socket-out))
  (let ([tmp
         (cond
           [(equal? mode '\;) (begin (displayDebug (format ">>>> recive-lixo1 .~a. <<<<~n" (read (socket-in))))
                                     (read-line (socket-in)))]
           [(equal? mode '$) (begin (displayDebug (format ">>>> recive-lixo2 .~a. <<<<~n" (read (socket-in))))
                                    (read (socket-in)))]
           [(equal? mode 'lisp) (begin (displayDebug (format ">>>> recive-lixo3 .~a. <<<<~n" (read (socket-in))))
                                       (read (socket-in)))]
           [(equal? mode 'display)  (begin (displayDebug (format ">>>> recive-lixo4 .~a. <<<<~n" (read (socket-in))))
                                           (let loop ([tmp-aux '()])
                                             (let ([aux (read-line (socket-in) 'any)])
                                               ;(displayln aux)
                                               (if (or (equal? "NIL" aux)
                                                        (and (eq? 4 (string-length aux))(equal? "NIL" (substring aux 0 3))))
                                                   (apply string-append (reverse tmp-aux))
                                                   ;(reverse tmp-aux)
                                                 (loop (cons aux tmp-aux))))))]
           [(equal? mode 'lisp#$$) (begin (displayDebug (format ">>>> recive-lixo5 .~a. <<<<~n" (read (socket-in))))
                                          (read (socket-in)))]
           [(equal? mode 'starting-maxima) 'void])])
    (displayDebug (format ">>>> recive ~a <<<<~n" tmp))
    tmp))

(define (send/recive-maxima-mathematica str)
  (send/recive-maxima str '\;))
(define (send/norecive-maxima-mathematica str)
  (send/recive-maxima str '$))
(define (send/recive-maxima-lisp str)
  (send/recive-maxima str 'lisp))
(define (send/recive-maxima-lispMathematica str)
  (send/recive-maxima str 'lisp#$$))
(define (send/recive-maxima-display str)
  (send/recive-maxima str 'display))

;############################################################################################
#|;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
:lisp
#$....$
(strmeval '..)
(mfuncall
(displa 
(maxima-display
(i-$grind
(symbolp '$Y)
((MLIST SIMP)((MLIST SIMP) ((MEQUAL SIMP) $X ((RAT SIMP) 1 2)) ((MEQUAL SIMP) $Y ((RAT SIMP) 1 2))((MEQUAL SIMP) $Z 0) ((MEQUAL SIMP) $ID_LINE_12305 ((RAT SIMP) 3 4))((MEQUAL SIMP) $ID_LINE_12308 ((RAT SIMP) 1 2))))
:lisp (strmeval '(($EV) (($SOLVE)((MLIST) ((MEQUAL) $X ((MPLUS) ((MTIMES) 2 $ID_LINE_12305) ((MMINUS) 1)))((MEQUAL) $Y ((MPLUS) ((MTIMES) 2 $ID_LINE_12305) ((MMINUS) 1)))((MEQUAL) $Z ((MPLUS) ((MTIMES) 0 $ID_LINE_12305) 0))((MEQUAL) $X ((MPLUS) ((MTIMES) ((MMINUS) 1) $ID_LINE_12308) 1))((MEQUAL) $Y ((MPLUS) ((MTIMES) 1 $ID_LINE_12308) 0))((MEQUAL) $Z ((MPLUS) ((MTIMES) 0 $ID_LINE_12308) 0)))((MLIST) $X $Y $Z $ID_LINE_12305 $ID_LINE_12308)) $NUMER $%ENUMER))


|#;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;###########################################      $EV            ###########################################
;maxima-eval-mathematica #:display fromLisp
(define (maxima-eval-mathematica str); #:display [display #f])
  (send/recive-maxima str '$))

;maxima-eval-lisp
(define (maxima-eval-lisp lst #:exactValues [exactValues #t])
  (send/recive-maxima-lisp (format "(strmeval '(($EV) ~a ~a))~n" lst (if exactValues "" "$NUMER $%ENUMER"))));(string->number tmp2)

;maxima-read-lisp
(define (maxima-read-lisp #:parameter [p "$%"])
  (send/recive-maxima-lisp p))

;maxima-lisp->mathematica
(define (maxima-lisp->mathematica lst)
  (send/recive-maxima-display lst))


;############################################## TESTS ####################################################
(module+ test
  (displayln "++++++++++++++++++++++++++ communication module test start")
  (start-maxima)
  ;(start-maxima #:path "C:\\maxima-5.37.2\\bin\\maxima.bat")
  ;(start-maxima-path-port "/usr/local/bin/maxima" 8087)
  ;(start-maxima-path-port "/usr/bin/maxima" 8087)
  ;(start-maxima-path-port "C:\\maxima-5.37.2\\bin\\maxima.bat" 8087)
  ;(define p2 (string->path "C:\\Program Files (x86)\\Maxima-5.31.2\\bin\\maxima.bat"))
  ;(select-maxima-path-port "C:\\Program Files (x86)\\Maxima-5.31.2\\bin\\maxima.bat" 8088);8088
  ;C:\Users\Fabio>start "ola" "C:\Program Files (x86)\Maxima-5.31.2\bin\maxima.bat" --very-quiet
  
  (define equacoes (list
                    "algsys ([ (x-(-1))^2 + (y-(1))^2 + (z-(2))^2 = r^2, (x-(2))^2 + (y-(1))^2 + (z-(3))^2 = r^2, (x-(1))^2 + (y-(-2))^2 + (z-(-3))^2 = r^2, (x-(3))^2 + (y-(2))^2 + (z-(1))^2 = r^2], [x,y,z,r]);"
                    "algsys ([ (x-(1))^2 + (y-(0))^2 + (z-(0))^2 = (2)^2 , (x-(3))- (2) = t , (y - (0)) - (2) = t , (z-(0))- (2) = t], [x,y,z,t]);"
                    ))
  (define test1 "solve([(x-(1))^2 + (y-(0))^2 + (z-(0))^2 = (2)^2, (x-(3))- (2) = t, (y - (0)) - (2) = t, (z-(0))- (2) = t],[x, y, z, t])")
  
  (define result (maxima-eval-mathematica test1))
  (displayln "result:")
  result
  (displayln "result-r:")
  (define result-r (maxima-eval-lisp result #:exactValues #f))
  result-r
  (maxima-lisp->mathematica result)
  (maxima-lisp->mathematica result-r)
  (send/recive-maxima-display
   '(($SOLVE SIMP)
     ((MLIST SIMP) ((MEQUAL SIMP) ((MPLUS SIMP) ((MEXPT SIMP) ((MPLUS SIMP) -1 $X) 2) ((MEXPT SIMP) ((MPLUS SIMP) -1 $Y) 2) ((MEXPT SIMP) $Z 2)) 1)
                   ((MEQUAL SIMP) ((MPLUS SIMP) ((MEXPT SIMP) ((MPLUS SIMP) -1 $X) 2) ((MEXPT SIMP) ((MPLUS SIMP) -1 $Y) 2) ((MEXPT SIMP) $Z 2)) 1))
     ((MLIST SIMP) $X $Y $Z)))
  (displayln "++++++++++++++++++++++++++ communication module test end"))
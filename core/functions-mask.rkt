#lang racket

(require 
  "../core/entities-declaration.rkt"
  (prefix-in function: "../function-module/geometric-functions-lib.rkt")
  ;racket/contract
  (for-syntax racket
              racket/syntax
              "../core/entities-declaration.rkt"))

(begin-for-syntax
    (define (build-name id . parts)
    (datum->syntax id
                   (string->symbol
                    (apply string-append
                           (map (lambda (p) (if (syntax? p)  (symbol->string (syntax-e p)) p))
                                parts))) id)))

(define-syntax (mask stx)
  (syntax-case stx ()
    [(_ (method-mew-name method-name (args ...) args-plan [return-match-pat return-match-body] ...)) ;TODO return-plan
     ((listof (cons/c symbol? (cons/c symbol? null?))) (syntax->datum #'(args ...)))
     (let* ([agrs-stxes (syntax->list #'(args ...))]
            [agrs-aux (map (lambda (e) (syntax->list e)) agrs-stxes)]
            [args-name-stxes (map (lambda (e) (car e)) agrs-aux)]
            [args-type-stxes (map (lambda (e) (cadr e)) agrs-aux)]
            [args-type?-stxes (map (lambda (e) (build-name e e "?")) args-type-stxes)]
            [args-plan-aux1 (map (lambda (e) (string-split (symbol->string e) "-")) (syntax->datum #'args-plan))]
            [args-plan-aux2 (map (lambda (e) (let* ([stxx (findf (lambda (arg) (equal? (string->symbol (first e)) (syntax->datum (car arg)))) agrs-aux)]
                                                    [getter-name-parts (list* (symbol->string (syntax->datum (cadr stxx))) (cdr e))]
                                                    [getter-name  (string->symbol (string-join getter-name-parts "-" ))])
                                               (cons (car stxx) getter-name)))
                                 args-plan-aux1)])
            ;[return-aux (map (lambda (e) (syntax->list e)) (syntax->list #'(return)))]
            ;[return-names (map (lambda (e) (syntax->datum (car e))) return-aux)]
            ;[return-type-stxes (car (map (lambda (e) (cadr e)) return-aux))]
            ;[return-return-names-and-type-stxes (map (lambda (n e) (cons n (syntax->datum e))) return-names (list return-type-stxes))]
            ;[return-matchs (syntax->datum #'(return-match ...))]
            ;[return-match-pat (map (lambda (e) (car e)) return-matchs)]
            ;[return-match-body (map (lambda (e) (cadr e)) return-matchs)])
       ;(displayln return-matchs)
       ;(define (binding b) (findf (lambda (e) (equal? (symbol->string (car e)) (car (string-split b "-")))) return-return-names-and-type-stxes))
       ;(sort (filter binding (map symbol->string (flatten return-match-pat))) string<?))
       (with-syntax* ([funtion-warp
                       `(define/contract
                          (,#'method-mew-name ,@args-name-stxes)
                          ;(flat-contract (and/c ,@args-type?-stxes))
                          (,@args-type?-stxes . -> . any/c);,(if #t 'any/c return-type-stxes)
                          ;(match/derived val-expr original-datum clause ...)
                           (match/derived (,#'method-name ,@(map (lambda (e) `(,(cdr e) ,(car e))) args-plan-aux2)) #'stx ;TODO CHECK the original-datum
                             ,@(map (lambda (pat body) `[,pat ,body])
                                    (syntax->datum #'(return-match-pat ...))
                                    (syntax->datum #'(return-match-body ...)))
                             [any (raise-mismatch-error ',#'method-mew-name "contract violation in mask(macro), unexpected return...\nexpected one of:"
                                                        ',(syntax->datum #'(return-match-pat ...)) "\nreceived:" any)]))])
         ;(displayln (syntax->datum #`funtion-warp))
         ;(displayln `(,#`(provide method-mew-name) ,#`funtion-warp))

         #`(begin (provide #,#'method-mew-name) funtion-warp)))
     ]))



(mask (2d-location-norm function:2d-location-norm ;(location-2d-norm x y)
       ((A point)) (A-x-n A-y-n)
       [R R]))


(mask (2d-line-midLocation function:2d-line-midLocation ;(new-line -2d-midLocation x1 y1 x2 y2)
       ((A line)) (A-start-x-n A-start-y-n A-end-x-n A-end-y-n)
       [(list X Y) (new-point X Y)]))

(mask (2d-location-location-distance function:2d-location-location-distance ;(location-location-2d-distance Ax Ay Bx By)
       ((A line)) (A-start-x-n A-start-y-n A-end-x-n A-end-y-n)
       [R R]))

#;(mask (3d-location-location-distance function:3d-location-location-distance ;(location-location-3d-distance Ax Ay Az Bx By Bz)
       ((A line) (B line)) (A-start-x-n A-start-y-n A-start-z-n A-end-x-n A-end-y-n A-end-z-n B-start-x-n B-start-y-n B-start-z-n B-end-x-n B-end-y-n B-end-z-n) (R number) ()))

(mask (2d-line-horizontal? function:2d-line-horizontal? ;(new-line -2d-horizontal? Ax Ay Bx By)
       ((A line)) (A-start-x-n A-start-y-n A-end-x-n A-end-y-n)  [R R]))

(mask (2d-line-vertical? function:2d-line-vertical? ;(new-line -2d-vertical? Ax Ay Bx By)
       ((A line)) (A-start-x-n A-start-y-n A-end-x-n A-end-y-n) [R R]))

(mask (2d-line-slope function:2d-line-slope ;(new-line -2d-slope Ax Ay Bx By)
       ((A line)) (A-start-x-n A-start-y-n A-end-x-n A-end-y-n) [R R]))


;;line-line
(mask (2d-intersection-line-line* function:2d-intersection-line-line* 
       ((A line)(B line)) (A-start-x-n A-start-y-n A-end-x-n A-end-y-n B-start-x-n B-start-y-n B-end-x-n B-end-y-n)
       [(list X Y) (list (new-point X Y))]
       ['parallel '() #;'parallel]))
(mask (2d-intersects-line-line? function:2d-intersects-line-line? 
       ((A line)(B line)) (A-start-x-n A-start-y-n A-end-x-n A-end-y-n B-start-x-n B-start-y-n B-end-x-n B-end-y-n)
       [R R]))
(mask (2d-intersection-line-line function:2d-intersection-line-line 
       ((A line)(B line)) (A-start-x-n A-start-y-n A-end-x-n A-end-y-n B-start-x-n B-start-y-n B-end-x-n B-end-y-n)
       [(list x y) (new-point x y)]))

;;circle-line
(mask (2d-intersection-circle-line* function:2d-intersection-circle-line*
      ((A circle)(B line)) (A-center-x-n A-center-y-n A-radius-n B-start-x-n B-start-y-n B-end-x-n B-end-y-n)
      [(cons 'no_intersection null) '() #;'no_intersection]
      [(cons 'tangent (cons X Y)) (list (new-point X Y))]
      [(cons 'intersections (cons (cons X1 Y1) (cons X2 Y2))) (list (new-point X1 Y1) (new-point X2 Y2))]))
(mask (2d-intersects-circle-line? function:2d-intersects-circle-line? 
      ((A circle)(B line)) (A-center-x-n A-center-y-n A-radius-n B-start-x-n B-start-y-n B-end-x-n B-end-y-n)
      [R R]))
(mask (2d-tangent-circle-line? function:2d-tangent-circle-line?
      ((A circle)(B line)) (A-center-x-n A-center-y-n A-radius-n B-start-x-n B-start-y-n B-end-x-n B-end-y-n)
      [R R]))
(mask (2d-intersections-circle-line function:2d-intersections-circle-line 
      ((A circle)(B line)) (A-center-x-n A-center-y-n A-radius-n B-start-x-n B-start-y-n B-end-x-n B-end-y-n)
      [(cons (cons X1 Y1) (cons X2 Y2)) (cons (new-point X1 Y1) (new-point X2 Y2))]))
(mask (2d-tangent-circle-line function:2d-tangent-circle-line
      ((A circle)(B line)) (A-center-x-n A-center-y-n A-radius-n B-start-x-n B-start-y-n B-end-x-n B-end-y-n)
      [(cons X Y) (new-point X Y)]))

;;circle-circle
(mask (2d-intersection-circle-circle* function:2d-intersection-circle-circle*
       ((A circle)(B circle)) (A-center-x-n A-center-y-n A-radius-n B-center-x-n B-center-y-n B-radius-n)
       [(cons 'no_intersection 'coincide) '() #;'coincide]
       [(cons 'no_intersection 'contains) '() #;'contains]
       [(cons 'no_intersection 'too_far_apart) '() #;'too_far_apart]
       [(cons 'tangent (cons X Y)) (list (new-point X Y))]
       [(cons 'intersections (cons (cons X1 Y1) (cons X2 Y2))) (list (new-point X1 Y1) (new-point X2 Y2))]))


(mask (2d-intersects-circle-circle? function:2d-intersects-circle-circle?
       ((A circle)(B circle)) (A-center-x-n A-center-y-n A-radius-n B-center-x-n B-center-y-n B-radius-n)
       [R R]))
(mask (2d-tangent-circle-circle? function:2d-tangent-circle-circle?
       ((A circle)(B circle)) (A-center-x-n A-center-y-n A-radius-n B-center-x-n B-center-y-n B-radius-n)
       [R R]))
(mask (2d-intersects-circle-circle function:2d-intersects-circle-circle
       ((A circle)(B circle)) (A-center-x-n A-center-y-n A-radius-n B-center-x-n B-center-y-n B-radius-n)
       [(cons (cons X1 Y1) (cons X2 Y2)) (cons (new-point X1 Y1) (new-point X2 Y2))]))
(mask (2d-tangent-circle-circle function:2d-tangent-circle-circle
       ((A circle)(B circle)) (A-center-x-n A-center-y-n A-radius-n B-center-x-n B-center-y-n B-radius-n)
       [(cons X Y) (new-point X Y)]))



#|
(provide hash-hash get-hash add-function)
(define hash-hash (make-hash))

(define (get-hash hash-name)
  (hash-ref hash-hash hash-name))
(define (add-function hash-name function-name function)
  (hash-set! (get-hash hash-name) function-name function))
;entities-tags

(define-syntax (define-in! stx)
  (syntax-case stx ()
    [(_ hash name args body0 body ...)
     #'(let ((f (function$-special-constructor name args (return) body0 body ...)))
         (hash-set! hash (function$-name f) f))]))

#;(void
   (for/list ([i entities])  ;(hash-for-each hash proc) -> void?
     (let ((name (string->symbol (format "hash-f-~a" i)))
           (hash-f-aux (make-hash)))
       (hash-set! hash-hash name hash-f-aux)
       (eval-syntax #`(define-in! #,hash-f-aux test-inspection (obj agr1 agr2)
                        (format "Funtion test-inspection of obj:~a; type:~a; return:~a."
                                (str$-id obj) '#,i (+ agr1 (* agr2 100)))))
       (eval-syntax #`(define-in! #,hash-f-aux type? ((obj : #,i)) '#,i))
       (eval-syntax #`(define-in! #,hash-f-aux id? ((obj : #,i)) (str$-id obj)))
       (eval-syntax #`(define-in! #,hash-f-aux print ((obj : #,i))
                        (format "~a$:(id:~a ; property:~a)" '#,i (str$-id obj) (str$-property obj))))
       (let ((funtion-name 'unwarp))
         (if (equal? 'var i)
             (eval-syntax #`(define-in! #,hash-f-aux #,funtion-name ((obj : #,i))
                              (var$-value obj))) ;FIXME
             (eval-syntax #`(define-in! #,hash-f-aux #,funtion-name ((obj : #,i)) 
                              (map (lambda (sub-entitie)
                                     ;(display sub-entitie)
                                     (if (equal? 'value (entitie-tag-type '#,i sub-entitie))
                                         (str$-property-get-key obj sub-entitie)
                                         (call (str$-property-get-key obj sub-entitie) 'unwarp)))
                                   '#,(entitie-default-unwarp i))))))
       (let* ((funtion-name 'warp))
         (if (equal? 'var i)
             (eval-syntax #`(define-in! #,hash-f-aux #,funtion-name ((obj : #,i))
                              (var$-value obj))) ;FIXME
             (let* ((type-default-warp (entitie-default-warp i))
                    (type-default-unwarp (entitie-default-unwarp i))
                    (list-of-types (map (lambda (e) (display i) (display "->") (displayln e) (entitie-tag-type i e))
                                        type-default-unwarp))
                    (list-of-types-names (map (lambda (n t d) `(,(string->symbol (format "$~a" n)) : ,t : ,d))
                                              (stream->list  (in-range (length list-of-types)))
                                              list-of-types
                                              type-default-unwarp))
                    (lst-agrs (cons `(obj : ,i) list-of-types-names)))
               
               (displayln ">>>>>")
               (displayln type-default-unwarp)
               (displayln type-default-warp)
               (displayln list-of-types)
               (displayln list-of-types-names)
               (displayln lst-agrs)
               (displayln "<<<<<<")
               (displayln #`(define-in! #,hash-f-aux #,funtion-name #,lst-agrs 111 ))
               
               (eval-syntax #`(define-in! #,hash-f-aux #,funtion-name ((obj : #,i))
                                (let ((entitie '(build '#,i)))
                                  'TODO)
                                (if (equal? 'value type-to-warp)
                                    type-to-warp
                                    (displayln "TODO232")))))))
       )))

#;(define-syntax (call stx)
    (syntax-case stx ()
      [(_ obj method args ...)
       #'(let ((funtion (hash-ref (str$-methods obj) method)))
           ;TODO check args
           (funtion obj args ...))]))

#;(define-syntax (call-apply stx)
    (syntax-case stx ()
      [(_ method obj ... (args ...))
       #'(map (lambda (obj2)
                (let ((funtion (hash-ref (str$-methods obj2) method)))
                  ;TODO check args
                  (funtion obj2 args ...)))
              (list obj ...))]))
|#



;####################################################################################

;



#;(define (call-dispatch-unwarp entities ...)
    (let* ((types (call-apply 'type? entities ... ()))
           (agrs (flatten (list (call Line1 'unwarp) (call Line2 'unwarp)))))
      (displayln "call-dispatch-unwarp")
      (displayln types)
      (apply line-line-2d-intersection agrs))
    )

(module+ test
  (displayln "++++++++++++++++++++++++++ functions module test start")
  (2d-location-norm (new-point 0 2))
  (2d-line-midLocation (new-line  (new-point 0 0) (new-point 0 1)))
  (2d-location-location-distance (new-line  (new-point 0 0) (new-point 0 1)))
  (2d-line-horizontal? (new-line  (new-point 0 0) (new-point 0 1)))
  (2d-line-vertical? (new-line  (new-point 0 0) (new-point 1 0)))
  (2d-line-slope (new-line  (new-point 0 0) (new-point 0 1)))
  (displayln ">>2d-line-line")
  (displayln 2d-intersection-line-line*)
  (2d-intersection-line-line* (new-line  (new-point 7 10) (new-point 7 -10)) (new-line  (new-point 0 5) (new-point 15 5))) ;'(7 5)
  (2d-intersection-line-line* (new-line  (new-point 1 0) (new-point 3 0)) (new-line  (new-point 1 1) (new-point 3 1))) ;'parallel
  (displayln 2d-intersects-line-line?)
  (2d-intersects-line-line? (new-line  (new-point 7 10) (new-point 7 -10)) (new-line  (new-point 0 5) (new-point 15 5))) ;#t
  (2d-intersects-line-line? (new-line  (new-point 1 0) (new-point 3 0)) (new-line  (new-point 1 1) (new-point 3 1))) ;#f
  (displayln 2d-intersection-line-line)
  (2d-intersection-line-line (new-line  (new-point 7 10) (new-point 7 -10)) (new-line  (new-point 0 5) (new-point 15 5))) ;'(7 5)
  ;error (2d-intersection-line-line (new-line  (new-point 1 0) (new-point 3 0)) (new-line  (new-point 1 1) (new-point 3 1)))
  (displayln ">>2d-circle-line")
  (displayln 2d-intersection-circle-line*)
  (2d-intersection-circle-line* (new-circle  (new-point 0 0) 1) (new-line  (new-point -5 0) (new-point 5 0))) ;'((1 0) (-1 0))
  (2d-intersection-circle-line* (new-circle  (new-point 0 0) 1) (new-line  (new-point -5 1) (new-point 5 1))) ;'((0 1))
  (2d-intersection-circle-line* (new-circle  (new-point 0 0) 0.5) (new-line  (new-point -5 1) (new-point 5 1)))
  (displayln 2d-intersects-circle-line?)
  (2d-intersects-circle-line? (new-circle  (new-point 0 0) 1) (new-line  (new-point -5 0) (new-point 5 0))) ;#t
  (2d-intersects-circle-line? (new-circle  (new-point 0 0) 1) (new-line  (new-point -5 1) (new-point 5 1))) ;#t
  (2d-intersects-circle-line? (new-circle  (new-point 0 0) 0.5) (new-line  (new-point -5 1) (new-point 5 1))) ;#f
  (displayln 2d-tangent-circle-line?)
  (2d-tangent-circle-line? (new-circle  (new-point 0 0) 1) (new-line  (new-point -5 0) (new-point 5 0))) ;#f
  (2d-tangent-circle-line? (new-circle  (new-point 0 0) 1) (new-line  (new-point -5 1) (new-point 5 1))) ;#t
  (2d-tangent-circle-line? (new-circle  (new-point 0 0) 0.5) (new-line  (new-point -5 1) (new-point 5 1))) ;#f
  (displayln 2d-intersections-circle-line)
  (2d-intersections-circle-line (new-circle  (new-point 0 0) 1) (new-line  (new-point -5 0) (new-point 5 0))) ;'((1 . 0) -1 . 0)
  ;error (2d-intersections-circle-line (new-circle  (new-point 0 0) 1) (new-line  (new-point -5 1) (new-point 5 1))) ;error
  ;error (2d-intersections-circle-line (new-circle  (new-point 0 0) 0.5) (new-line  (new-point -5 1) (new-point 5 1))) ;error
  (displayln 2d-tangent-circle-line)
  ;error (2d-tangent-circle-line (new-circle  (new-point 0 0) 1) (new-line  (new-point -5 0) (new-point 5 0))) ;error
  (2d-tangent-circle-line (new-circle  (new-point 0 0) 1) (new-line  (new-point -5 1) (new-point 5 1))) ;'(0 . 1)
  ;error (2d-tangent-circle-line (new-circle  (new-point 0 0) 0.5) (new-line  (new-point -5 1) (new-point 5 1))) ;error
  (displayln ">>circle-circle")
  (displayln 2d-intersection-circle-circle*)
  (2d-intersection-circle-circle*  (new-circle  (new-point 0 0) 1) (new-circle  (new-point 0 0) 2))
  (2d-intersection-circle-circle*  (new-circle  (new-point 0 0) 10) (new-circle  (new-point 1 0) 1.5))
  (2d-intersection-circle-circle*  (new-circle  (new-point 100 0) 1) (new-circle  (new-point 10 0) 2))
  (2d-intersection-circle-circle*  (new-circle  (new-point 1 -2) 3) (new-circle  (new-point 1 -2) 3))
  (2d-intersection-circle-circle*  (new-circle  (new-point 0 0) 1) (new-circle  (new-point 0 2) 1)) ;'(tangent 0 . 1)) ;tagente
  (2d-intersection-circle-circle*  (new-circle  (new-point 0 0) 1.5) (new-circle  (new-point 1 0) 1.5)) ;'(intersections (0.5 . 1.414213562373095) 0.5 . -1.414213562373095))
  (2d-intersection-circle-circle*  (new-circle  (new-point 0 0) 1.5) (new-circle  (new-point 1 0) 1)) ;'(intersections (1.125 . 0.9921567416492215) 1.125 . -0.9921567416492215))
  (2d-intersection-circle-circle*  (new-circle  (new-point 0 0) 1.5) (new-circle  (new-point 1 1) 1)) ;'(intersections (0.13070549283526772 . 1.4942945071647322) 1.4942945071647322 . 0.13070549283526772))
  (displayln 2d-intersects-circle-circle?)
  (2d-intersects-circle-circle? (new-circle  (new-point 100 0) 1) (new-circle  (new-point 1 0) 1)) ;#f ;no_intersection
  (2d-intersects-circle-circle? (new-circle  (new-point 0 0) 1) (new-circle  (new-point 0 2) 1)) ;#f ;tagente
  (2d-intersects-circle-circle? (new-circle  (new-point 0 0) 1.5) (new-circle  (new-point 1 0) 1.5)) ;#t ;intersections
  (displayln 2d-tangent-circle-circle?)
  (2d-tangent-circle-circle? (new-circle  (new-point 100 0) 1) (new-circle  (new-point 1 0) 1)) ;#f ;no_intersection
  (2d-tangent-circle-circle? (new-circle  (new-point 0 0) 1) (new-circle  (new-point 0 2) 1)) ;#t ;tagente
  (2d-tangent-circle-circle? (new-circle  (new-point 0 0) 1.5) (new-circle  (new-point 1 0) 1.5)) ;#f ;intersections
  (displayln 2d-intersects-circle-circle)
  ;error (2d-intersects-circle-circle (new-circle  (new-point 100 0) 1) (new-circle  (new-point 1 0) 1)) error ;no_intersection
  ;error (2d-intersects-circle-circle (new-circle  (new-point 0 0) 1) (new-circle  (new-point 0 2) 1)) error ;tagente
  (2d-intersects-circle-circle (new-circle  (new-point 0 0) 1.5) (new-circle  (new-point 1 0) 1.5)) ;'((0.5 . 1.414213562373095) 0.5 . -1.414213562373095) ;intersections
  (displayln 2d-tangent-circle-circle)
  ;error (2d-tangent-circle-circle (new-circle  (new-point 100 0) 1) (new-circle  (new-point 1 0) 1)) error ;no_intersection
  (2d-tangent-circle-circle (new-circle  (new-point 0 0) 1) (new-circle  (new-point 0 2) 1)) ;'(0 . 1) ;tagente
  ;error (2d-tangent-circle-circle (new-circle  (new-point 0 0) 1.5) (new-circle  (new-point 1 0) 1.5)) error ;intersections
  
  
  #;(let ((L1 (build 'line))
          (L2 (build 'line)))
      (let ((A (build 'location))
            (B (build 'location))
            (C (build 'location))
            (D (build 'location)))
        (str$-property-set-key A 'X 7)
        (str$-property-set-key A 'Y 10)
        (str$-property-set-key B 'X 7)
        (str$-property-set-key B 'Y -10)
        (str$-property-set-key C 'X 0)
        (str$-property-set-key C 'Y 5)
        (str$-property-set-key D 'X 15)
        (str$-property-set-key D 'Y 5)
        (str$-property-set-key L1 'start A)
        (str$-property-set-key L1 'end B)
        (str$-property-set-key L2 'start C)
        (str$-property-set-key L2 'end D)
        (set! Line1 L1)
        (set! Line2 L2)
        (let ((Ax (str$-property-get-key (str$-property-get-key L1 'start) 'X))
              (Ay (str$-property-get-key (str$-property-get-key L1 'start) 'Y))
              (Bx (str$-property-get-key (str$-property-get-key L1 'end) 'X))
              (By (str$-property-get-key (str$-property-get-key L1 'end) 'Y))
              (Cx (str$-property-get-key (str$-property-get-key L2 'start) 'X))
              (Cy (str$-property-get-key (str$-property-get-key L2 'start) 'Y))
              (Dx (str$-property-get-key (str$-property-get-key L2 'end) 'X))
              (Dy (str$-property-get-key (str$-property-get-key L2 'end) 'Y)))
          (equal? (new-line -line-2d-intersection Ax Ay Bx By Cx Cy Dx Dy) '(7 5)))))
  (displayln "++++++++++++++++++++++++++ functions module test end")
  )


#|(module main typed/racket
  (require "str.rkt")
  (displayln "-------------------------- functions module main start")
  (#%top-interaction . (:print-type point$))(newline)
  (displayln "-------------------------- functions module main end")
  )|#

#|
(hash-set! hash-f-sphere 'f-surface-point!
           (λ (obj x y z)
             (add-to-info! obj 'surface-point (quasiquote (,x ,y ,z)))))
(hash-set! hash-f-sphere 'f-center!
           (λ (obj x y z)
             (add-to-info! obj 'center (quasiquote (,x ,y ,z)))))
(hash-set! hash-f-sphere 'f-radius!
           (λ (obj radius)
             (add-to-info! obj 'radius (quasiquote (,radius)))))

(define (filter-obj obj tag)
  (filter (λ (d) (eq? tag (description$-tag d))) (str$-info obj)))

(hash-set! hash-f-sphere 'f-solve
           (λ (obj)
             (let ((mat-surface-point (λ (desc)
                                        (let* ((lst (description$-val desc))
                                               (x (first lst))
                                               (y (second lst))
                                               (z (third lst)))
                                          (format "(x-(~a))^2 + (y-(~a))^2 + (z-(~a))^2 = r^2" x y z))))
                   (compose-restrictions (λ (lst)
                                           (let ((str "algsys (["))
                                             (set! str (format "~a ~a" str (car lst)))
                                             (for ([i (cdr lst)])
                                               (set! str (format "~a, ~a" str i)))
                                             (format "~a], ~a" str "[x,y,z,r]);"))))
                   (lst (filter-obj obj 'surface-point)))
               (compose-restrictions (map mat-surface-point lst)))))            

(hash-set! hash-f-line 'f-form&a+bt!
           (λ (obj x xt y yt z zt)
             (add-to-info! obj 'form&a+bt (quasiquote ((,x ,xt) (,y ,yt) (,z ,zt))))))

(hash-set! hash-f-sphere 'f-intersection
           (λ (sphere-obj line-obj)
             (let* ((mat-line (λ (desc)
                                (let* ((lst (description$-val desc)))
                                  (format "(x-(~a))- ~a = t , (y - (~a)) - ~a = t , (z-(~a))- ~a = t"
                                          (car (first lst))  (cdr (first lst))
                                          (car (second lst)) (cdr (second lst))
                                          (car (third lst))  (cdr (third lst))))))
                    (mat-shpere (λ (point radius)
                                  (let ((x (first (description$-val point)))
                                        (y (second (description$-val point)))
                                        (z (third  (description$-val point)))
                                        (r (first (description$-val radius))))
                                    (format "(x-(~a))^2 + (y-(~a))^2 + (z-(~a))^2 = (~a)^2" x y z r))))
                    (compose-restrictions (λ (sphere line)
                                            (let ((str "algsys (["))
                                              (set! str (format "~a ~a , ~a" str sphere line ))
                                              (format "~a], ~a" str "[x,y,z,t]);"))))
                    
                   (center (car (filter-obj sphere-obj 'center)))
                   (radius (car (filter-obj sphere-obj 'radius)))
                   (new-line  (car (filter-obj line-obj 'form&a+bt))))
               (compose-restrictions (mat-shpere center radius)(mat-line line)))))
|#


#|(define f-sphere-surface-point
  (λ ([sphere : sphere$] [point : point$])
    (format "(x-(~a))^2 (y-(~a))^2 (z-(~a))^2 = r^2"
            (var$-value (point$-x-n point))
            (var$-value (point$-y-n point))
            (var$-value (point$-z-n point)))))|#


#lang racket
(require #;REQUIRE
         "../core/communication.rkt"
         "../core/entities-declaration.rkt"
         "../mathematical-module/property.rkt"
         "../mathematical-module/mathProblem.rkt"
         )
(provide #;PROVIDE
         ;mathProblem EXTRAS FUNCTIONS
         #;mathProblem-add-property
         #;mathProblem-status
         mathProblem-solve #;FIXME
         #;mathProblem-nSolutions
         mathProblem-solution #;FIXME
         mathProblem-resolve 
         #;mathProblem-warpToMaxima
         ;MAXIMA API
         #;warpToMaxima
         ;EQUATION FORM
         mathProblem-getEntityWithValues #;FIXME
         )


(define (mathProblem-add-property str ep)
  (unless (equal? 'NEW (mathProblem-state str))
    (error 'mathProblem-add-property
           (format "This mathProblem's state (~a) should be (~a) to call a function" (mathProblem-state str) 'NEW)))
  (unless (property? ep)
    (error 'mathProblem-add-property (format "A property is expected. Gived ~a" ep)))
  (set-mathProblem-entitiesProperty! str (cons ep (mathProblem-entitiesProperty str))))

(define (mathProblem-add-restrictions str rest)
  (unless (equal? 'NEW (mathProblem-state str))
    (error 'mathProblem-add-restrictions
           (format "This mathProblem's state (~a) should be (~a) to call a function" (mathProblem-state str) 'NEW)))
  (unless (string? rest)
    (displayln rest)
    (error 'mathProblem-add-restrictions
           (format "A string is expected (in Maxima restrictions). Gived ~a" rest)))
  (set-mathProblem-restrictions! str (cons rest (mathProblem-restrictions str))))

(define (mathProblem-status str)
  (let ([n (mathProblem-idMaxima str)]
        [state (mathProblem-state str)]
        [solution (mathProblem-solutionsMaxima str)])
    (displayln (format "# ~a~n~a~n~a"
                       (format "mathProblem-status: state=~a; opc=%i~a" state n)
                       (if (equal? n 'uninitialized) 'uninitialized
                           (maxima-lisp->mathematica (maxima-eval-mathematica (format "%i~a" n))))
                       (if (equal? solution 'uninitialized) 'uninitialized
                           (maxima-lisp->mathematica solution))
                       ))))

;#######################################   GET SOLUTIONS   #################################################
(define (mathProblem-solve str #:debug[debug false])
  (unless (equal? 'NEW (mathProblem-state str))
    (error 'mathProblem-solve
           (format "This mathProblem's state (~a) should be (~a) to call a function" (mathProblem-state str) 'NEW)))
  (set-mathProblem-state! str 'SOLVE)
  (let* ([maximaFrom (mathProblem-warpToMaxima str)]
         [maximaSolution (maxima-eval-mathematica maximaFrom)])
    (set-mathProblem-solutionsMaxima! str maximaSolution)
    (when debug (displayln maximaFrom))
    (set-mathProblem-idMaxima! str maxima-mathematica-expression-counter))
  #;(mathProblem-nSolutions str))

(define (mathProblem-nSolutions str)
  (+ -1  (length (mathProblem-solutionsMaxima str))))

(define (symbol-upcase symbol)
  (string->symbol (string-upcase (~a symbol))))

(define (mathProblem-solution str pos [var 'all] #:mode [mode 1] #:allowNotFound [allowNotFound #f]) ;#:returnPairIdValue [returnPairIdValue #f]
  ;#:allowNotFound #t   => return #<void> when not var(id) is not Found.
  (unless (and (exact-nonnegative-integer? pos)
               (< pos (mathProblem-nSolutions str)))
    (error 'solution-ref (format "The solution's number(~a) must be a exact-nonnegative-integer and for this solutions less than ~a"
                                 pos (mathProblem-nSolutions str))))
  (let* ([lst (mathProblem-solutionsMaxima str)]
         [selected (cdr (list-ref (cdr lst) pos))]
         [call (case mode
                 [(0 'none) (lambda (v) v)]
                 [(1 'string) send/recive-maxima-display]
                 [(2 'exactValues) (lambda (v) (maxima-eval-lisp v #:exactValues #t))]
                 [(3 'round) (lambda (v) (maxima-eval-lisp v #:exactValues #f))]
                 [else (error 'mathProblem-solution "Mode unknown, use (0 for none) (1 for string) (2 for exactValues) (3 for round)")])]) ;FIXME case?
    (match var
      ['all (call  (cons '(MLIST SIMP) selected))] ;FIXME HOTFIX '(MLIST SIMP) is "[" in MAXIMA
      [(? list? lstV) (map (lambda (v) (mathProblem-solution str pos v #:allowNotFound allowNotFound)) lstV)]
      [(? symbol? V) (let* ([var-aux (string->symbol (string-upcase (~a '$ var)))]
                            [toReturn (findf (lambda (e) (equal? (cadr e) var-aux))
                                             selected)])
                       (if toReturn
                           (call (third toReturn))
                           (unless allowNotFound
                             (error 'mathProblem-solution (format "No solution found; agrs: ~a ; s_selected: ~a" var-aux selected)))))])))

#|(define (mathProblem-solution-ref str pos var)
  (caddr (mathProblem-solution str pos var #:exactValues #f)))|#

;(define (mathProblem-resolve . lstProperty #:restrictions [extraRestrictions '()]) ;BUG ? Module Language: invalid module text
(define (mathProblem-resolve #:restrictions [extraRestrictions '()] #:debug[debug false] . lstProperty)
  (unless (list? extraRestrictions)
    (error 'mathProblem-resolve
           (format "A listof strings is expected (in Maxima restrictions). Gived ~a" rest)))
  (let ([str (new-mathProblem null)])
    (for ([i lstProperty])
      (mathProblem-add-property str i))
    (for ([v extraRestrictions])
      (displayln extraRestrictions)
      (mathProblem-add-restrictions str v))
    (mathProblem-solve str #:debug debug)
    #;(mathProblem-solution str 0 #:exactValues #f)
    str))


#|(define (mathProblem-newAndWarpToMaxima . lstProperty)
  (let ([str (mathProblem-new)])
    (for ([i lstProperty])
      (mathProblem-add-property str i))
    (mathProblem-warpToMaxima str)))|#

;#######################################   EQUATION FORM   #################################################
(define (mathProblem-warpToMaxima str)
  (let* ([mP (mathProblem-entitiesProperty str)]
         [eqs (properties-equations mP)]
         [vars (properties-vars mP)]
         [rests (append (mathProblem-restrictions str)
                        (properties-restrictions mP))]
         [ret (warpToMaxima eqs vars rests)])
    (set-mathProblem-tmpEquation! str ret)
    ;(displayln ret) ;FIXME REMOVE
    ret))

(define (warpToMaxima equations-list [vars '()] [filter '()])
  (define varsSorted (sort (map symbol->string vars) string<?)) ;FIXME REMOVE sort to optimization
  (define filterListTreated (let loop ([str (string-join filter " and ")]
                                       [vars varsSorted])
                              (if (null? vars)
                                  str
                                  (loop (string-replace str (~a (first vars)) (~a "subst(s," (first vars) ")"))
                                        (rest vars)))))
  (define solution (string-join
                    (list
                     "radcan(" ;FIXME HOTFIX sohould be in maxima-listOfConfigurations (communication.rkt);
                     "solve(["
                     (string-join equations-list ",")
                     (if (empty? vars) ""
                         (string-join (list "],["  (string-join varsSorted ",")) ""))
                     "]))")
                    ""))
  solution
  #;(if (empty? filter)
      solution
      (string-join (list "sublist(" solution ", lambda([s]," filterListTreated "))") ""))) ;FIXME is not working properly (review points2sphere)

;#######################################   ENTITIE  MATH   #################################################
(define (mathProblem-getEntityWithValues str entity)
  (unless (equal? (mathProblem-state str) 'SOLVE)
    (error 'mathProblem-to-entitie "Math is not solved"))
  (let loop ([i 0][return '()])
    (if (>= i (mathProblem-nSolutions str))
        (reverse return)
        (let* ([lstOfId (map (λ (e) (entity-id e))
                             (flat-values entity))]
               [lstOfValue (mathProblem-solution str i lstOfId #:mode 1 #:allowNotFound #t)]
               [lstOfPairIdValue (map (λ (id value) (cons id value))
                                      lstOfId lstOfValue)]
               [lstOfPairIdValueWithNotVoids (filter (λ (e) (not (void? (cdr e)))) lstOfPairIdValue)])
          (if (empty? lstOfPairIdValueWithNotVoids)
              (loop (add1 i) return)
              (loop (add1 i) (cons (update-values (entity-copy entity) lstOfPairIdValueWithNotVoids) return)))))))



;(define (wtf) (new-point-unset));BUG?
(module+ test
  (require "../mathematical-module/entities-properties.rkt")
  (displayln "++++++++++++++++++++++++++ mat-utilities module test start")
  ;(new-point-unset) ;#BUG!
  (define p1 (new-point-unset))
  (define l1 (new-line (new-point -1 -1 0) (new-point 1 1 0)))
  (define l2 (new-line (new-point 1 0 0) (new-point 0 1 0)))
  (define l3 (new-line (new-point 0 0 0) (new-point 10 0 0)))
  (define l4 (new-line (new-point 0 0 0) (new-point 1 0 0)))
  (define l5 (new-line (new-point 2 0 0) (new-point 0 2 0)))
  (define l6 (new-line (new-point 0 0 0) (new-point 2 0 0)))
  (define c1 (new-circle (new-point 0 0) "(1+1/2)"))
  (define c2 (new-circle (new-point 1 1) 1))
  (define c3 (new-circle (new-point 1 1) 1))
  (define c4 (new-circle (new-point 0 0) 2))
  (define c5 (new-circle (new-point 4 0) 2))
  (define point1 (new-point "(3/2)" 0))
  ;(define point2 (point 4 -3))
  (define point2 (new-point 10 1))
  
  (start-maxima)
  
  (displayln "> mathProblem-resolve")
  ;(mathProblem-resolve (line-form-equation l1) (line-form-equation l2))
  (displayln "> mathProblem1")
  (define str1 (new-mathProblem))

  (mathProblem-add-property str1 (line-form-equation l1 p1))
  (mathProblem-add-property str1 (line-form-equation l2 p1))
  (mathProblem-solve str1)
  (mathProblem-solution str1 0 (point-x-s p1) #:mode 3)
  ;(mathProblem-solution str1 0 'X)
  (mathProblem-solution str1 0)
  
  (displayln "> mathProblem2")
  (define str2 (new-mathProblem null))
  (mathProblem-add-property str2 (line-form-equation l2 p1))
  (mathProblem-add-property str2 (circle-form-equation c1 p1))
  ;(mathProblem-warpToMaxima str2)
  (mathProblem-solve str2)
  ;str2
  ;(mathProblem-solution str2 0 'X #:exactValues #f)
  
  (displayln "> cast str2<")
  ;(mathProblem-status str2)
  (mathProblem-getEntityWithValues str2 p1)
  
  (displayln "++++++++++++++++++++++++++ mat-utilities module test end")
  )